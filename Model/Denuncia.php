<?php
App::uses('AppModel', 'Model');
/**
 * Denuncia Model
 *
 * @property Denunciacomunidade $Denunciacomunidade
 * @property Denunciadocumento $Denunciadocumento
 * @property Denunciado $Denunciado
 * @property Denunciaorganismo $Denunciaorganismo
 * @property Ciudadano $Ciudadano
 * @property Denunciaconsulta $Denunciaconsulta
 */
class Denuncia extends AppModel {
	public $virtualFields = array(
	    'tiempo_atencion' => 'TIMESTAMPDIFF(SECOND,Denuncia.created,Denuncia.ended)'
	);

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nro_expediente';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'ciudadano_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'organismo_denunciado' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasOne associations
 *
 * @var array
 */
	public $hasOne = array(
		'Denunciacomunidade' => array(
			'className' => 'Denunciacomunidade',
			'foreignKey' => 'denuncia_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Denunciadocumento' => array(
			'className' => 'Denunciadocumento',
			'foreignKey' => 'denuncia_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Denunciaorganismo' => array(
			'className' => 'Denunciaorganismo',
			'foreignKey' => 'denuncia_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Ciudadano' => array(
			'className' => 'Ciudadano',
			'foreignKey' => 'ciudadano_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Denunciaconsulta' => array(
			'className' => 'Denunciaconsulta',
			'foreignKey' => 'denuncia_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Denunciado' => array(
			'className' => 'Denunciado',
			'joinTable' => 'denunciados_denuncias',
			'foreignKey' => 'denuncia_id',
			'associationForeignKey' => 'denunciado_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

	public function getValidateUpdated() {
		return Set::merge(
			$this->validate,
			array(
				'descripcion_denuncia' => array(
					'notempty' => array(
						'rule'       => 'notEmpty',
						'allowEmpty' => false,
						'required'   => true,
					)
				),
			)
		);
	}

}
