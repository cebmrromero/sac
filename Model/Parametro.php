<?php
App::uses('AppModel', 'Model');
/**
 * Parametro Model
 *
 */
class Parametro extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'nombre' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
        
    public function getUltimoAtencionControlByAtenciontipoId($atenciontipo_id) {
        $this->getEjercicioFiscal();
        $atencion_control = $this->findByNombre('control_' . $atenciontipo_id);
        $atencion_control['Parametro']['valor']++;
        $this->save($atencion_control);
        return sprintf("%03s", $atencion_control['Parametro']['valor']);
    }
    
    public function getUltimoDenunciaControl() {
        $this->getEjercicioFiscal();
        $denuncia_control = $this->findByNombre('denuncia_control');
        $denuncia_control['Parametro']['valor']++;
        $this->save($denuncia_control);
        return sprintf("%03s", $denuncia_control['Parametro']['valor']);
    }
    
    public function getEjercicioFiscal($short = false) {
        $ejercicio_fiscal = $this->findByNombre('ejercicio_fiscal');
        if ($ejercicio_fiscal['Parametro']['valor'] != date('Y')) {
            $ejercicio_fiscal['Parametro']['valor'] = date('Y');
            
            for($i = 1; $i < 6; $i++) {
                $atencion_control = $this->findByNombre('control_' . $i);
                $atencion_control['Parametro']['valor'] = 0;
                $this->save($atencion_control);
            }
            
            $denuncia_control = $this->findByNombre('denuncia_control');
            $denuncia_control['Parametro']['valor'] = 0;
            $this->save($denuncia_control);
        }
        $this->save($ejercicio_fiscal);
        return ($short) ? substr($ejercicio_fiscal['Parametro']['valor'], -2) : $ejercicio_fiscal['Parametro']['valor'];
    }
}
