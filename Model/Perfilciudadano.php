<?php
App::uses('AppModel', 'Model');
/**
 * Perfilciudadano Model
 *
 * @property Ciudadano $Ciudadano
 * @property Estadocivile $Estadocivile
 * @property Parroquia $Parroquia
 */
class Perfilciudadano extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'ciudadano_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fecha_nacimiento' => array(
			'date' => array(
				'rule' => array('date', 'dmy'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sexo' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'estadocivile_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'El campo es requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'nivelinstruccione_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'El campo es requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'parroquia_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'El campo es requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Ciudadano' => array(
			'className' => 'Ciudadano',
			'foreignKey' => 'ciudadano_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Estadocivile' => array(
			'className' => 'Estadocivile',
			'foreignKey' => 'estadocivile_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Parroquia' => array(
			'className' => 'Parroquia',
			'foreignKey' => 'parroquia_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Nivelinstruccione' => array(
			'className' => 'Nivelinstruccione',
			'foreignKey' => 'nivelinstruccione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function getPerfilciudadanoPath($perfilciudadano) {
        return (isset($perfilciudadano['parroquia_id']) && !empty($perfilciudadano['parroquia_id'])) ? array_merge($perfilciudadano, $this->Parroquia->getFullPath($perfilciudadano['parroquia_id'])) : $perfilciudadano;
	}

        public function beforeSave($options = array()) {
		    if (!empty($this->data['Perfilciudadano']['fecha_nacimiento'])) {
		        $this->data['Perfilciudadano']['fecha_nacimiento'] = $this->dateFormatBeforeSave(
		            $this->data['Perfilciudadano']['fecha_nacimiento']
		        );
		    }
		    return true;
		}
		public function afterFind($results, $primary = false) {
		    foreach ($results as $key => $val) {
		        if (isset($val['Perfilciudadano']['fecha_nacimiento'])) {
		            $results[$key]['Perfilciudadano']['fecha_nacimiento'] = $this->dateFormatAfterFind(
		                $val['Perfilciudadano']['fecha_nacimiento']
		            );
		        }
		        if ($key === 'fecha_nacimiento') {
		            $results['edad'] = $this->getEdad($val);
		        }
		    }
		    return $results;
		}

		public function dateFormatBeforeSave($dateString) {
		    return date('Y-m-d', strtotime($dateString));
		}

		public function dateFormatAfterFind($dateString) {
		    return date('d-m-Y', strtotime($dateString));
		}

		public function getEdad($fecha) {
		    list($Y,$m,$d) = explode("-",$fecha);
		    return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
		}
}
