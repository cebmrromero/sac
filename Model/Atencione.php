<?php
App::uses('AppModel', 'Model');
/**
 * Atencione Model
 *
 * @property Atenciontipo $Atenciontipo
 * @property Ciudadano $Ciudadano
 * @property User $User
 * @property Acompanante $Acompanante
 * @property Atencionconsulta $Atencionconsulta
 * @property Atencionprocedencia $Atencionprocedencia
 */
class Atencione extends AppModel {
	public $virtualFields = array(
	    'tiempo_atencion' => 'CEIL((TIME_TO_SEC(Atencione.ended) - TIME_TO_SEC(Atencione.created)) / 60)'
	);

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nro_atencion';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'atenciontipo_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'ciudadano_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tiene_acompanante' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'procedencia' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasOne associations
 *
 * @var array
 */
	public $hasOne = array(
		'Atencionprocedencia' => array(
			'className' => 'Atencionprocedencia',
			'foreignKey' => 'atencione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Atenciontipo' => array(
			'className' => 'Atenciontipo',
			'foreignKey' => 'atenciontipo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Ciudadano' => array(
			'className' => 'Ciudadano',
			'foreignKey' => 'ciudadano_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Leye' => array(
			'className' => 'Leye',
			'foreignKey' => 'leye_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Atencioncategoria' => array(
			'className' => 'Atencioncategoria',
			'foreignKey' => 'atencioncategoria_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Acompanante' => array(
			'className' => 'Acompanante',
			'foreignKey' => 'atencione_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Atencionconsulta' => array(
			'className' => 'Atencionconsulta',
			'foreignKey' => 'atencione_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => 'Atencionconsulta.id DESC',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);

	public function getValidateUpdated() {
		return Set::merge(
			$this->validate,
			array(
				'motivo_atencion' => array(
					'notempty' => array(
						'rule'       => 'notEmpty',
						'allowEmpty' => false,
						'required'   => true,
					)
				),
				'atencion_brindada' => array(
					'notempty' => array(
						'rule'       => 'notEmpty',
						'allowEmpty' => false,
						'required'   => true,
					)
				),
				'leye_id' => array(
					'notempty' => array(
						'rule'       => 'notEmpty',
						'allowEmpty' => false,
						'required'   => true,
					)
				),
				'atencioncategoria_id' => array(
					'notempty' => array(
						'rule'       => 'notEmpty',
						'allowEmpty' => false,
						'required'   => true,
					)
				),
			)
		);
	}

	public function beforeSave($options = array()) {
	    if (!empty($this->data['Atencione']['created'])) {
	        $this->data['Atencione']['created'] = $this->dateFormatBeforeSave(
	            $this->data['Atencione']['created']
	        );
	    }

	    return true;
	}

	public function dateFormatBeforeSave($dateString) {
	    return date('Y-m-d H:i:s', strtotime($dateString));
	}
}
