<?php
App::uses('AppModel', 'Model');
/**
 * Procedenciaorganismo Model
 *
 * @property Atencionprocedencia $Atencionprocedencia
 * @property Organismo $Organismo
 */
class Procedenciaorganismo extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'atencionprocedencia_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'organismo_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'notempty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Atencionprocedencia' => array(
			'className' => 'Atencionprocedencia',
			'foreignKey' => 'atencionprocedencia_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Organismo' => array(
			'className' => 'Organismo',
			'foreignKey' => 'organismo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
