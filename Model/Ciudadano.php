<?php
App::uses('AppModel', 'Model');
/**
 * Ciudadano Model
 *
 */
class Ciudadano extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'visitante';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'ciudadano';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'id_ciudadano';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'cedula';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id_ciudadano' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cedula' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'nombres' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'apellidos' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'direccion' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'ruta_foto' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasOne associations
 *
 * @var array
 */
	public $hasOne = array(
		'Perfilciudadano' => array(
			'className' => 'Perfilciudadano',
			'foreignKey' => 'ciudadano_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Telefono' => array(
			'className' => 'Telefono',
			'foreignKey' => 'ciudadano_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Atencionconsulta' => array(
			'className' => 'Atencionconsulta',
			'foreignKey' => 'ciudadano_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Atencione' => array(
			'className' => 'Atencione',
			'foreignKey' => 'ciudadano_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Atencionprocedencia' => array(
			'className' => 'Atencionprocedencia',
			'foreignKey' => 'ciudadano_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Acompanante' => array(
			'className' => 'Acompanante',
			'foreignKey' => 'ciudadano_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Denuncia' => array(
			'className' => 'Denuncia',
			'foreignKey' => 'ciudadano_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Denunciaconsulta' => array(
			'className' => 'Denunciaconsulta',
			'foreignKey' => 'ciudadano_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
        
        public function getCiudadanoForSession($cedula) {
            $this->recursive = 0;
            $ciudadano = $this->findByCedula($cedula);
            if (isset($ciudadano['Perfilciudadano']['parroquia_id'])) {
                $parroquia = $this->Perfilciudadano->Parroquia->getFullPath($ciudadano['Perfilciudadano']['parroquia_id']);
                $ciudadano = array_merge($ciudadano, $parroquia);
            }
            return $ciudadano;
        }
}
