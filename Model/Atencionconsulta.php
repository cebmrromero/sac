<?php
App::uses('AppModel', 'Model');
/**
 * Atencionconsulta Model
 *
 * @property Atencione $Atencione
 * @property Ciudadano $Ciudadano
 */
class Atencionconsulta extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'atencione_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'ciudadano_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'descripcion' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Atencione' => array(
			'className' => 'Atencione',
			'foreignKey' => 'atencione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Ciudadano' => array(
			'className' => 'Ciudadano',
			'foreignKey' => 'ciudadano_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
