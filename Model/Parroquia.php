<?php
App::uses('AppModel', 'Model');
/**
 * Parroquia Model
 *
 * @property Municipio $Municipio
 */
class Parroquia extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comun';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'nombre' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'municipio_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Municipio' => array(
			'className' => 'Municipio',
			'foreignKey' => 'municipio_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
        
        public function getFullPath($parroquia_id) {
            if (!$parroquia_id) {
                return array();
            }
            $this->recursive = -1;
            $parroquia = $this->findById($parroquia_id);
            
            $this->Municipio->recursive = -1;
            $municipio = $this->Municipio->findById($parroquia['Parroquia']['municipio_id']);
            
            $this->Municipio->Entidade->recursive = -1;
            $entidad = $this->Municipio->Entidade->findById($municipio['Municipio']['entidad_id']);
            
            $this->Municipio->Entidade->Paise->recursive = -1;
            $pais = $this->Municipio->Entidade->Paise->findById($entidad['Entidade']['pais_id']);
            
            return array_merge($parroquia, $municipio, $entidad, $pais);
        }
}
