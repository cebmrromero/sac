<?php
App::uses('AppModel', 'Model');
/**
 * Atencionprocedencia Model
 *
 * @property Procedenciacomunidade $Procedenciacomunidade
 * @property Procedenciaorganismo $Procedenciaorganismo
 * @property Ciudadano $Ciudadano
 * @property Atencione $Atencione
 * @property Parroquia $Parroquia
 */
class Atencionprocedencia extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'ciudadano_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'atencione_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'procedencia' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasOne associations
 *
 * @var array
 */
	public $hasOne = array(
		'Procedenciacomunidade' => array(
			'className' => 'Procedenciacomunidade',
			'foreignKey' => 'atencionprocedencia_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Procedenciaorganismo' => array(
			'className' => 'Procedenciaorganismo',
			'foreignKey' => 'atencionprocedencia_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Ciudadano' => array(
			'className' => 'Ciudadano',
			'foreignKey' => 'ciudadano_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Atencione' => array(
			'className' => 'Atencione',
			'foreignKey' => 'atencione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Parroquia' => array(
			'className' => 'Parroquia',
			'foreignKey' => 'parroquia_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
