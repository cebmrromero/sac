<?php
App::uses('AppModel', 'Model');
/**
 * Estadocivile Model
 *
 * @property Perfilciudadano $Perfilciudadano
 */
class Estadocivile extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comun';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'denominacion';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'denominacion' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Perfilciudadano' => array(
			'className' => 'Perfilciudadano',
			'foreignKey' => 'estadocivile_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
