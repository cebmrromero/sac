<?php
App::uses('AppModel', 'Model');
/**
 * Procedenciacomunidade Model
 *
 * @property Atencionprocedencia $Atencionprocedencia
 * @property Comunidade $Comunidade
 */
class Procedenciacomunidade extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'atencionprocedencia_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'comunidade_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'notempty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Atencionprocedencia' => array(
			'className' => 'Atencionprocedencia',
			'foreignKey' => 'atencionprocedencia_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Comunidade' => array(
			'className' => 'Comunidade',
			'foreignKey' => 'comunidade_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Responsabilidade' => array(
			'className' => 'Responsabilidade',
			'foreignKey' => 'responsabilidade_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
