-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: sacdb
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acompanantes`
--

DROP TABLE IF EXISTS `acompanantes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acompanantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `atencione_id` int(11) NOT NULL,
  `ciudadano_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `atencione_id` (`atencione_id`,`ciudadano_id`),
  KEY `fk_acompanantes_atenciones_idx` (`atencione_id`),
  CONSTRAINT `fk_acompanantes_atenciones` FOREIGN KEY (`atencione_id`) REFERENCES `atenciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acompanantes`
--

LOCK TABLES `acompanantes` WRITE;
/*!40000 ALTER TABLE `acompanantes` DISABLE KEYS */;
INSERT INTO `acompanantes` VALUES (1,3,6302),(3,10,3817),(5,10,3896),(7,10,5587),(8,12,6743),(9,17,6302),(10,19,6990),(11,24,3817),(34,28,142),(33,28,1209),(35,28,3274),(36,28,3730),(32,28,6743),(12,29,2246),(13,29,7327),(15,34,3817),(14,34,6302),(25,40,220),(22,40,3095),(27,40,3156),(24,40,3857),(21,40,4833),(19,40,5212),(20,40,5885),(26,40,7651),(18,42,2222),(16,42,2246),(30,42,3896),(17,42,5212),(31,42,6309),(29,42,8090);
/*!40000 ALTER TABLE `acompanantes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acos`
--

DROP TABLE IF EXISTS `acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acos`
--

LOCK TABLES `acos` WRITE;
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;
INSERT INTO `acos` VALUES (1,NULL,NULL,NULL,'controllers',1,242),(2,1,NULL,NULL,'Atenciones',2,25),(3,2,NULL,NULL,'main',3,4),(5,2,NULL,NULL,'view',5,6),(6,2,NULL,NULL,'add',7,8),(8,2,NULL,NULL,'delete',9,10),(9,1,NULL,NULL,'Groups',26,37),(10,9,NULL,NULL,'index',27,28),(11,9,NULL,NULL,'view',29,30),(12,9,NULL,NULL,'add',31,32),(13,9,NULL,NULL,'edit',33,34),(14,9,NULL,NULL,'delete',35,36),(15,1,NULL,NULL,'Pages',38,41),(16,15,NULL,NULL,'display',39,40),(17,1,NULL,NULL,'Sistemas',42,43),(29,1,NULL,NULL,'Users',44,67),(30,29,NULL,NULL,'index',45,46),(31,29,NULL,NULL,'view',47,48),(32,29,NULL,NULL,'add',49,50),(33,29,NULL,NULL,'edit',51,52),(34,29,NULL,NULL,'delete',53,54),(35,29,NULL,NULL,'login',55,56),(36,29,NULL,NULL,'logout',57,58),(37,29,NULL,NULL,'checkAro',59,60),(38,29,NULL,NULL,'initDB',61,62),(39,29,NULL,NULL,'rebuildARO',63,64),(40,1,NULL,NULL,'AclExtras',68,69),(41,1,NULL,NULL,'DebugKit',70,77),(42,41,NULL,NULL,'ToolbarAccess',71,76),(43,42,NULL,NULL,'history_state',72,73),(44,42,NULL,NULL,'sql_explain',74,75),(45,1,NULL,NULL,'Ciudadanos',78,87),(49,45,NULL,NULL,'edit',79,80),(51,45,NULL,NULL,'buscar',81,82),(52,45,NULL,NULL,'cambiar',83,84),(53,1,NULL,NULL,'Atencionconsultas',88,95),(54,53,NULL,NULL,'add',89,90),(55,53,NULL,NULL,'edit',91,92),(56,53,NULL,NULL,'delete',93,94),(57,1,NULL,NULL,'Entidades',96,99),(58,57,NULL,NULL,'getAllByPaisIdJson',97,98),(59,1,NULL,NULL,'Municipios',100,103),(60,59,NULL,NULL,'getAllByEntidadIdJson',101,102),(61,1,NULL,NULL,'Parroquias',104,107),(62,61,NULL,NULL,'getAllByMunicipioIdJson',105,106),(63,1,NULL,NULL,'Perfilciudadanos',108,109),(65,2,NULL,NULL,'continuar',11,12),(66,1,NULL,NULL,'Acompanantes',110,113),(67,66,NULL,NULL,'delete',111,112),(68,1,NULL,NULL,'Comunidades',114,119),(69,68,NULL,NULL,'getByIdJson',115,116),(70,1,NULL,NULL,'Organismos',120,125),(71,70,NULL,NULL,'getByIdJson',121,122),(74,45,NULL,NULL,'getCedulasJSONP',85,86),(76,1,NULL,NULL,'Atencionprocedencias',126,127),(79,2,NULL,NULL,'getAreasJSONP',13,14),(80,2,NULL,NULL,'getSubareasJSONP',15,16),(83,70,NULL,NULL,'getByNombreJSONP',123,124),(84,68,NULL,NULL,'getByNombreJSONP',117,118),(85,1,NULL,NULL,'Denuncias',128,143),(86,85,NULL,NULL,'index',129,130),(87,85,NULL,NULL,'view',131,132),(88,85,NULL,NULL,'add',133,134),(90,85,NULL,NULL,'delete',135,136),(91,85,NULL,NULL,'continuar',137,138),(92,1,NULL,NULL,'Denunciados',144,151),(98,1,NULL,NULL,'DenunciadosDenuncias',152,155),(99,98,NULL,NULL,'delete',153,154),(100,1,NULL,NULL,'Denunciaconsultas',156,163),(101,100,NULL,NULL,'add',157,158),(102,92,NULL,NULL,'getByCedula',145,146),(103,92,NULL,NULL,'getByCedulaJSON',147,148),(104,2,NULL,NULL,'index',17,18),(105,100,NULL,NULL,'edit',159,160),(106,100,NULL,NULL,'delete',161,162),(107,29,NULL,NULL,'profile',65,66),(108,1,NULL,NULL,'Atenciontipos',164,171),(109,108,NULL,NULL,'index',165,166),(110,108,NULL,NULL,'view',167,168),(112,108,NULL,NULL,'edit',169,170),(114,1,NULL,NULL,'Parametros',172,177),(115,114,NULL,NULL,'index',173,174),(118,114,NULL,NULL,'edit',175,176),(126,1,NULL,NULL,'Security',178,183),(127,126,NULL,NULL,'backup',179,180),(128,126,NULL,NULL,'restore',181,182),(130,92,NULL,NULL,'updateMany',149,150),(131,2,NULL,NULL,'planilla',19,20),(132,85,NULL,NULL,'planilla',139,140),(133,1,NULL,NULL,'Nivelinstrucciones',184,195),(140,85,NULL,NULL,'finalizar',141,142),(141,1,NULL,NULL,'Procedenciaorganismos',196,201),(142,141,NULL,NULL,'getDependenciasJSONP',197,198),(143,141,NULL,NULL,'getCargosJSONP',199,200),(144,1,NULL,NULL,'Procedenciacomunidades',202,205),(145,144,NULL,NULL,'getOcupacionesJSONP',203,204),(146,1,NULL,NULL,'Responsabilidades',206,217),(147,146,NULL,NULL,'index',207,208),(148,146,NULL,NULL,'view',209,210),(149,146,NULL,NULL,'add',211,212),(150,146,NULL,NULL,'edit',213,214),(151,146,NULL,NULL,'delete',215,216),(152,133,NULL,NULL,'index',185,186),(153,133,NULL,NULL,'view',187,188),(154,133,NULL,NULL,'add',189,190),(155,133,NULL,NULL,'edit',191,192),(156,133,NULL,NULL,'delete',193,194),(157,1,NULL,NULL,'Atencioncategorias',218,229),(158,157,NULL,NULL,'index',219,220),(159,157,NULL,NULL,'view',221,222),(160,157,NULL,NULL,'add',223,224),(161,157,NULL,NULL,'edit',225,226),(162,157,NULL,NULL,'delete',227,228),(163,1,NULL,NULL,'Leyes',230,241),(164,163,NULL,NULL,'index',231,232),(165,163,NULL,NULL,'view',233,234),(166,163,NULL,NULL,'add',235,236),(167,163,NULL,NULL,'edit',237,238),(168,163,NULL,NULL,'delete',239,240),(169,2,NULL,NULL,'planilla_pdf',21,22),(170,2,NULL,NULL,'no_concretada',23,24);
/*!40000 ALTER TABLE `acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros`
--

DROP TABLE IF EXISTS `aros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros`
--

LOCK TABLES `aros` WRITE;
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;
INSERT INTO `aros` VALUES (1,NULL,'Group',1,NULL,1,4),(2,NULL,'Group',2,NULL,5,16),(3,NULL,'Group',3,NULL,17,18),(4,NULL,'Group',4,NULL,19,20),(5,NULL,'Group',5,NULL,21,22),(6,NULL,'Group',6,NULL,23,24),(7,NULL,'Group',7,NULL,25,26),(8,NULL,'Group',8,NULL,27,28),(9,NULL,'Group',9,NULL,29,32),(10,NULL,'Group',99,NULL,33,36),(11,1,'User',1,NULL,2,3),(12,10,'User',2,NULL,34,35),(15,2,'User',5,NULL,6,7),(17,2,'User',7,NULL,8,9),(18,2,'User',8,NULL,10,11),(19,2,'User',9,NULL,12,13),(20,2,'User',10,NULL,14,15),(21,9,'User',11,NULL,30,31);
/*!40000 ALTER TABLE `aros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros_acos`
--

LOCK TABLES `aros_acos` WRITE;
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;
INSERT INTO `aros_acos` VALUES (1,1,1,'1','1','1','1'),(2,2,1,'1','1','1','1'),(3,2,168,'-1','-1','-1','-1'),(4,2,162,'-1','-1','-1','-1'),(5,9,1,'1','1','1','1'),(6,9,168,'-1','-1','-1','-1'),(7,9,162,'-1','-1','-1','-1'),(8,10,1,'-1','-1','-1','-1'),(9,10,107,'1','1','1','1');
/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atencioncategorias`
--

DROP TABLE IF EXISTS `atencioncategorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atencioncategorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atencioncategorias`
--

LOCK TABLES `atencioncategorias` WRITE;
/*!40000 ALTER TABLE `atencioncategorias` DISABLE KEYS */;
INSERT INTO `atencioncategorias` VALUES (1,'Declaracion Jurada de Patrimonio'),(2,'Usuario Bloqueado');
/*!40000 ALTER TABLE `atencioncategorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atencionconsultas`
--

DROP TABLE IF EXISTS `atencionconsultas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atencionconsultas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `atencione_id` int(11) NOT NULL,
  `ciudadano_id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_atencionconsultas_atenciones1_idx` (`atencione_id`),
  CONSTRAINT `fk_atencionconsultas_atenciones1` FOREIGN KEY (`atencione_id`) REFERENCES `atenciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atencionconsultas`
--

LOCK TABLES `atencionconsultas` WRITE;
/*!40000 ALTER TABLE `atencionconsultas` DISABLE KEYS */;
INSERT INTO `atencionconsultas` VALUES (3,5,6104,'prueba','2014-03-24 19:19:19'),(4,11,6104,'test','2014-03-24 19:24:06'),(5,24,6302,'asd','2014-04-04 14:33:16');
/*!40000 ALTER TABLE `atencionconsultas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atenciones`
--

DROP TABLE IF EXISTS `atenciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atenciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `atenciontipo_id` int(11) NOT NULL,
  `ciudadano_id` int(11) NOT NULL,
  `nro_atencion` varchar(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `ended` datetime DEFAULT NULL,
  `motivo_atencion` text,
  `atencion_brindada` text,
  `leye_id` int(11) DEFAULT NULL,
  `atencioncategoria_id` int(11) DEFAULT NULL,
  `fundamentacion_legal` text,
  `observaciones` text,
  `tiene_acompanante` tinyint(1) NOT NULL DEFAULT '0',
  `tiene_documentos` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `completada` tinyint(1) DEFAULT '0',
  `paso` varchar(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_atenciones_atenciontipos1_idx` (`atenciontipo_id`),
  KEY `fk_atenciones_leyes1_idx` (`leye_id`),
  KEY `fk_atenciones_atencioncategorias1_idx` (`atencioncategoria_id`),
  CONSTRAINT `fk_atenciones_atencioncategorias1` FOREIGN KEY (`atencioncategoria_id`) REFERENCES `atencioncategorias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_atenciones_atenciontipos1` FOREIGN KEY (`atenciontipo_id`) REFERENCES `atenciontipos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_atenciones_leyes1` FOREIGN KEY (`leye_id`) REFERENCES `leyes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atenciones`
--

LOCK TABLES `atenciones` WRITE;
/*!40000 ALTER TABLE `atenciones` DISABLE KEYS */;
INSERT INTO `atenciones` VALUES (2,3,6302,'03-001-2014','2014-02-26 15:40:46','2014-02-26 16:04:46','<p>dasdasdsaadsasdasd</p>\r\n','<p>asdadsdasdasdsa</p>\r\n',1,1,'<p>Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Ã³</p>\r\n','',1,1,5,1,'4'),(3,1,6104,'01-004-2014','2014-02-26 14:34:36','2014-02-26 00:23:00','<p>dasads</p>\r\n','<p>asddsa</p>\r\n',1,2,'<p>Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2</p>\r\n',NULL,1,0,1,1,'4'),(4,2,3769,'02-003-2014','2014-03-12 13:37:17','2014-02-26 00:23:00','<p>asesoria para DJP</p>\r\n','<p>lo que se le dijo</p>\r\n',4,1,'<p>Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fund</p>\r\n',NULL,0,0,1,1,'3'),(5,1,6302,'01-001-2014','2014-03-14 15:40:56','2014-03-14 16:00:56','<p>Un motivo de pruebas</p>\r\n','<p>Una atencion brindada</p>\r\n',1,2,'<p>Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal&nbsp;</p>\r\n','',0,0,5,1,'4'),(6,1,3817,'01-010-2014','2014-03-19 13:28:43','2014-03-19 13:50:43','<p>Cvfbgdghnfgjgkjhm, kjÃ±lb m lknlkcklj, llkmm&nbsp; clqmnlksmlkm&nbsp; qnfqmÃ±eofmÃ±elf lkemflkemfr&nbsp; kemrfÃ±kmeÃ±m lekmfÃ±wemf dlpkmmhg mgmgmg.</p>\r\n','<p>Cvfbgdghnfgjgkjhm, kjÃ±lb m lknlkcklj, llkmm&nbsp; clqmnlksmlkm&nbsp; qnfqmÃ±eofmÃ±elf lkemflkemfr&nbsp; kemrfÃ±kmeÃ±m lekmfÃ±wemf dlpkmmhg mgmgmg.</p>\r\n',1,2,'<p>Cvfbgdghnfgjgkjhm, kj&ntilde;lb m lknlkcklj, llkmm&nbsp; clqmnlksmlkm&nbsp; qnfqm&ntilde;eofm&ntilde;elf lkemflkemfr&nbsp; kemrf&ntilde;kme&ntilde;m lekmf&ntilde;wemf dlpkmmhg mgmgmg.</p>\r\n','',0,1,5,1,'4'),(7,5,3817,'05-001-2014','2014-03-20 17:30:09','2014-02-26 00:23:00','<p>asd</p>\r\n','<p>asd</p>\r\n',1,1,'<p>Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal&nbsp;</p>\r\n',NULL,0,0,1,1,'4'),(8,5,3817,'05-002-2014','2014-03-20 18:49:52','2014-02-26 00:23:00','<p>El ciudadano viene porque tiene <strong>tal y </strong>tal problema, dice que le pa<strong>sa tal cosa, esta</strong> tratando de tal broima, pero nada de eso puede porque el tipo este viene y le hace esto y lo otro, luego que se va entonces viene otro y le dice tal y tal broma para <strong>que haga esto </strong>y lo <strong>otro pero nada </strong>ocurre, ni lo uno lo otro pasa lo siguiente</p>\r\n\r\n<ul>\r\n	<li>Esto</li>\r\n	<li>Esto</li>\r\n	<li>esto tambien</li>\r\n	<li>Lo otro y</li>\r\n	<li>Todo esto</li>\r\n</ul>\r\n\r\n<p>en esta secuencia</p>\r\n\r\n<ol>\r\n	<li>primero esto</li>\r\n	<li>luego esto</li>\r\n	<li>finalizando con esto</li>\r\n</ol>\r\n\r\n<p>El esta <em><strong>cansado</strong></em></p>\r\n','<p>Sele dijo al ciudadano que hiciera esto y esto. El ciudadano viene porque tiene&nbsp;<strong>tal y&nbsp;</strong>tal problema, dice que le pa<strong>sa tal cosa, esta</strong>&nbsp;tratando de tal broima, pero nada de eso puede porque el tipo este viene y le hace esto y lo otro, luego que se va entonces viene otro y le dice tal y tal broma para&nbsp;<strong>que haga esto&nbsp;</strong>y lo&nbsp;<strong>otro pero nada&nbsp;</strong>ocurre, ni lo uno lo otro pasa lo siguiente</p>\r\n\r\n<ul>\r\n	<li>Esto</li>\r\n	<li>Esto</li>\r\n	<li>esto tambien</li>\r\n	<li>Lo otro y</li>\r\n	<li>Todo esto</li>\r\n</ul>\r\n\r\n<p>en esta secuencia</p>\r\n\r\n<ol>\r\n	<li>primero esto</li>\r\n	<li>luego esto</li>\r\n	<li>finalizando con esto</li>\r\n</ol>\r\n\r\n<p>El esta&nbsp;<em><strong>cansado</strong></em></p>\r\n',1,1,'<p>Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacio<strong>n legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentaci</strong>on legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de <u><strong><em>pruebas</em></strong> para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pr</u>uebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este <strong>es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion leg</strong>al Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro tex<strong>to de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es </strong>otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal&nbsp;</p>\r\n','<p>nada que decir</p>',0,0,1,1,'4'),(9,4,6104,'04-002-2014','2014-03-20 19:10:39','2014-02-26 00:23:00','<p>Vengo a reclamar por tal y tal razon</p>\r\n','<p>Se le indicaron los pasos para poder hacer efectivo su reclamo</p>\r\n',1,2,'<p>Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1</p>\r\n',NULL,0,0,1,1,'4'),(10,2,6302,'02-001-2014','2014-03-21 15:41:05','2014-03-21 15:59:05','<p>Este es otro texto de prueacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamenta</p>\r\n','<p>Este es otro texto de prueacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentaci</p>\r\n',3,2,'<p>Este es otro texto de prueacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundamentacion legal Este es otro texto de pruebas para la &nbsp;fundam</p>\r\n','',1,0,5,1,'4'),(11,1,6104,'01-005-2014','2014-03-24 18:20:09','2014-02-26 00:23:00','<p>El ciudadano vino a denunciar por x y y causa</p>\r\n','<p>Se le indico que debe hacer esto esto y lo otro</p>\r\n',1,2,'<p>Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1</p>\r\n',NULL,1,0,1,1,'4'),(12,1,6302,'01-009-2014','2014-03-26 15:41:51','2014-03-26 15:59:51','<p>dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;</p>\r\n','<p>dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd L</p>\r\n',1,1,'<p>dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;</p>\r\n','',1,0,5,1,'4'),(13,1,6302,'01-002-2014','2014-03-27 15:41:54','2014-03-27 15:58:54','<p>asd</p>\r\n','<p>asd</p>\r\n',1,1,'<p>asd</p>\r\n','',0,0,5,1,'4'),(16,2,6302,'02-002-2014','2014-03-27 15:41:57','2014-03-27 15:59:57','<p>Vino por su DJ</p>\r\n','<p>se le dio su dj</p>\r\n',2,2,'<p>Se le dio su DJ&nbsp;Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1</p>\r\n','',0,0,5,1,'4'),(17,4,3817,'04-003-2014','2014-03-27 13:38:52','2014-02-26 00:23:00','<p>El ciudadano vino, porque necesita hacer su declaracion jurada</p>\r\n','<p>se le indico tal y tal cosa, esta y la otra, con esta otra y esta otra,</p>\r\n',1,2,'<p>Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Tex<em><strong>to de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Text</strong></em>o d<u>e prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1Texto de prueba 1</u></p>\r\n',NULL,1,0,1,1,'4'),(18,1,6302,'01-003-2014','2014-03-27 15:42:00','2014-03-27 16:09:00','<p>asd</p>\r\n','<p>dsa</p>\r\n',4,1,'<p>asdTexto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2Texto de prueba 2</p>\r\n','',0,0,5,1,'4'),(19,4,6302,'04-001-2014','2014-03-27 15:42:11','2014-03-27 16:12:11','<p>test ada &nbsp;dadads a dsa dstest ada &nbsp;dadads a dsa dstest ada &nbsp;dadads a dsa dstest ada &nbsp;dadads a dsa dstest ada &nbsp;dadads a dsa dstest ada &nbsp;dadads a dsa dstest ada &nbsp;dadads a dsa dstest ada &nbsp;dadads a dsa dstest ada &nbsp;dadads a dsa dstest ada &nbsp;dadads a dsa dstest ada &nbsp;dadads a dsa dstest ada &nbsp;dadasadadsdsaadsdsaasd</p>\r\n','<ol>\r\n	<li>testdasdasd</li>\r\n	<li>ads</li>\r\n	<li>sad</li>\r\n	<li>ads</li>\r\n	<li>das</li>\r\n	<li>asd</li>\r\n	<li>aasdsadasda</li>\r\n</ol>\r\n',4,2,NULL,'',1,0,5,1,'4'),(24,1,6302,'01-006-2014','2014-04-04 15:43:42','2014-04-04 16:09:42','<p>ads</p>\r\n','<p>asd</p>\r\n',1,1,NULL,'',1,0,5,1,'4'),(25,4,6302,'04-004-2014','2014-04-04 15:43:51','2014-04-04 16:05:51','<p>ads</p>\r\n','<p>asd</p>\r\n',1,1,NULL,'',0,0,5,1,'4'),(26,3,6302,'03-002-2014','2014-04-06 15:43:49','2014-04-06 15:58:49','<p>das</p>\r\n','<p>asd</p>\r\n',4,2,NULL,'',0,0,5,1,'4'),(27,1,6302,'01-007-2014','2014-04-04 15:43:46','2014-04-04 16:10:46','<p>fgjfjjjjjkkmÃ²ppÃ²Ã²Ã²`p kljljl &nbsp;juhoho8 uoyhoyu &nbsp;ouhyo bu 8y uhyyoy &nbsp; uigiuy iuhgoiy &nbsp;fgjfjjjjjkkmÃ²ppÃ²Ã²Ã²`p kljljl &nbsp;juhoho8 uoyhoyu &nbsp;ouhyo bu 8y uhyyoy &nbsp; uigiuy iuhgoiy&nbsp;fgjfjjjjjkkmÃ²ppÃ²Ã²Ã²`p kljljl &nbsp;juhoho8 uoyhoyu &nbsp;ouhyo bu 8y uhyyoy &nbsp; uigiuy iuhgoiy&nbsp;fgjfjjjjjkkmÃ²ppÃ²Ã²Ã²`p kljljl &nbsp;juhoho8 uoyhoyu &nbsp;ouhyo bu 8y uhyyoy &nbsp; uigiuy iuhgoiy lkjhjhkhhhiuhu</p>\r\n','<p>fgjfjjjjjkkmÃ²ppÃ²Ã²Ã²`p kljljl &nbsp;juhoho8 uoyhoyu &nbsp;ouhyo bu 8y uhyyoy &nbsp; uigiuy iuhgoiy &nbsp;fgjfjjjjjkkmÃ²ppÃ²Ã²Ã²`p kljljl &nbsp;juhoho8 uoyhoyu &nbsp;ouhyo bu 8y uhyyoy &nbsp; uigiuy iuhgoiy&nbsp;fgjfjjjjjkkmÃ²ppÃ²Ã²Ã²`p kljljl &nbsp;juhoho8 uoyhoyu &nbsp;ouhyo bu 8y uhyyoy &nbsp; uigiuy iuhgoiy&nbsp;fgjfjjjjjkkmÃ²ppÃ²Ã²Ã²`p kljljl &nbsp;juhoho8 uoyhoyu &nbsp;ouhyo bu 8y uhyyoy &nbsp; uigiuy iuhgoiy&nbsp;</p>\r\n',4,1,NULL,'',0,0,5,1,'4'),(28,1,6302,'01-008-2014','2014-04-07 15:39:35','2014-04-07 16:03:35','<p>test</p>\r\n','<p>test</p>\r\n',1,2,NULL,'',1,0,5,1,'3'),(29,1,6302,'01-011-2014','2014-04-07 00:00:00','2014-04-07 00:19:00','<p>hksdhksadhkasdkasdkhasdkhasd</p>\r\n','<p>sadadsdsadsa</p>\r\n',1,1,NULL,'<p>test</p>\r\n',1,0,10,1,'4'),(30,1,3,NULL,'2014-04-07 10:35:25','2014-02-26 00:23:00',NULL,NULL,NULL,NULL,NULL,NULL,0,0,10,0,'2'),(31,1,8,NULL,'2014-04-07 10:49:45','2014-02-26 00:23:00',NULL,NULL,NULL,NULL,NULL,NULL,0,0,10,0,'2'),(32,1,9,NULL,'2014-04-07 10:50:45','2014-02-26 00:23:00',NULL,NULL,NULL,NULL,NULL,NULL,0,0,1,0,'2'),(33,1,3,NULL,'2014-04-07 10:37:00','2014-02-26 00:23:00',NULL,NULL,NULL,NULL,NULL,NULL,0,0,1,0,'3'),(34,1,8035,NULL,'2014-04-08 10:54:46','2014-02-26 00:23:00',NULL,NULL,NULL,NULL,NULL,NULL,1,0,1,0,'4'),(35,1,8035,NULL,'2014-04-10 13:58:49','2014-02-26 00:23:00',NULL,NULL,NULL,NULL,NULL,NULL,0,0,1,0,'2'),(36,3,6302,'03-003-2014','2014-04-10 00:00:00','2014-04-10 00:20:00','<p>sda</p>\r\n','<p>sad</p>\r\n',1,1,NULL,'',0,0,7,1,'4'),(37,2,6302,'02-004-2014','2014-04-02 15:33:38','2014-04-02 15:54:38','<p>ads</p>\r\n','<p>ad</p>\r\n',2,1,NULL,'',0,0,5,1,'4'),(38,5,6302,'05-003-2014','2014-04-11 10:05:46','2014-04-11 10:31:46','<p>dssd</p>\r\n','<p>dfsfds</p>\r\n',1,2,NULL,'',0,0,5,1,'4'),(39,1,6302,'01-014-2014','2014-04-14 14:00:43','2014-04-14 14:18:43','<p>asdadsads</p>\r\n','<p>asddsaasd</p>\r\n',2,1,NULL,'<p>asdads</p>\r\n',0,0,5,1,'4'),(40,1,6302,'01-015-2014','2014-04-15 14:26:47','2014-04-15 14:45:47','<p>test asd&nbsp;test asd&nbsp;test asd&nbsp;test<u> asd&nbsp;test asd</u>&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;te</p>\r\n\r\n<ol>\r\n	<li>st asd</li>\r\n	<li>&nbsp;test asd</li>\r\n	<li>&nbsp;test asd</li>\r\n	<li>&nbsp;test asd&nbsp;test asd&nbsp;test</li>\r\n	<li>asd&nbsp;test asd</li>\r\n</ol>\r\n\r\n<p>test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test</p>\r\n\r\n<ul>\r\n	<li>asd&nbsp;test asd&nbsp;test <strong>asd&nbsp;test asd</strong>&nbsp;test</li>\r\n	<li>asd&nbsp;test</li>\r\n	<li>asd&nbsp;test asd&nbsp;test asd</li>\r\n	<li>&nbsp;test</li>\r\n	<li>asd&nbsp;test asd&nbsp;test</li>\r\n	<li>asd&nbsp;test asd&nbsp;test asd&nbsp;test</li>\r\n</ul>\r\n\r\n<p>asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;<em><u>test asd&nbsp;test asd&nbsp;test asd</u></em>&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;</p>\r\n','<p>testtest asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;test asd&nbsp;</p>\r\n',1,2,NULL,'',1,0,5,1,'4'),(41,1,3769,'01-017-2014','2014-06-02 13:12:25','2014-06-02 13:35:25','<p>Esto es una prueba test test test test lkjadslkjasd<strong> adsdsa dasEsto es una prueba test test test test lkjadslkjasd adsdsa dasEsto es una prueba test test test test lkjadslkjasd adsdsa dasEsto es una prueba test test test test lkj</strong>adslkjasd adsdsaasEsto es un<span style=\"line-height:1.6\">a prueba test</span><span style=\"line-height:1.6\">&nbsp;</span><span style=\"line-height:1.6\">d</span><span style=\"line-height:1.6\">&nbsp;<u>test test test lkjadslkjasd</u> adsdsa dasEsto es una prueba test test test test lkjadslkjasd adsdsa das</span></p>\r\n\r\n<ul>\r\n	<li>dsadasdasasd</li>\r\n	<li>adsdasdasdsa</li>\r\n	<li>adsadsasddsa</li>\r\n	<li>adsadsasd</li>\r\n</ul>\r\n','<p>Esto es una prueba test test test tes231321321132321t lkjadslkjasd<strong>&nbsp;adsdsa dasEsto es una pru123321132123132132123132eba test test test test lkjadslkjasd adsdsa dasEsto es una prueba test test test test lkjadslkjasd adsdsa dasEsto es una prueba test test test test 132321</strong>&nbsp;adsdsaasEsto es una prueba test&nbsp;d&nbsp;<u>test test test lkjadslkjasd</u>&nbsp;adsdsa dasEsto es una prueba test test test test lkjadslkjasd adsdsa das321</p>\r\n\r\n<ul>\r\n	<li>dsadasdasasd321321312</li>\r\n	<li>adsdasdasdsa123332131</li>\r\n	<li>adsadsasddsa312321312</li>\r\n	<li>adsadsasdadsdas132312</li>\r\n</ul>\r\n',1,1,NULL,'',0,0,5,1,'4'),(42,1,6302,'01-016-2014','2014-06-16 15:12:55','2014-06-16 15:36:55','<p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estÃ¡ndar de las industrias desde el aÃ±o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usÃ³ una galerÃ­a de textos y los mezclÃ³ de tal manera que logrÃ³ hacer un libro de textos especimen. No sÃ³lo sobreviviÃ³ 500 aÃ±os, sino que tambien ingresÃ³ como texto de relleno en documentos electrÃ³nicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creaciÃ³n de las hojas &quot;Letraset&quot;, las cuales contenian pasajes de Lorem Ipsum, y mÃ¡s recientemente con software de autoediciÃ³n, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium etali.&nbsp;On the other hand, we denounce with righteous indignation and dislike men who&nbsp;porro quisquam est, qui&nbsp;dolorem etasi</p>\r\n','<p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estÃ¡ndar de las industrias desde el aÃ±o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usÃ³ una galerÃ­a de textos y los mezclÃ³ de tal manera que logrÃ³ hacer un libro de textos especimen. No sÃ³lo sobreviviÃ³ 500 aÃ±os, sino que tambien ingresÃ³ como texto de relleno en documentos electrÃ³nicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creaciÃ³n de las hojas &quot;Letraset&quot;, las cuales contenian pasajes de Lorem Ipsum, y mÃ¡s recientemente con software de autoediciÃ³n, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium etali.&nbsp;On the other hand, we denounce with righteous indignation and dislike men who&nbsp;porro quisquam est, qui&nbsp;dolorem etasi</p>\r\n',1,1,NULL,'',1,0,5,1,'3'),(43,9,6302,NULL,'2014-06-18 12:08:06','2014-07-18 12:21:00','<p>Vino pero se fue</p>\r\n','<p>Se le dieron los requisitos</p>\r\n',NULL,NULL,NULL,'',0,0,1,1,'1'),(44,9,3769,NULL,'2014-06-18 13:36:33','2014-06-18 13:37:38','<ul>\r\n	<li>asdasdasdaasd</li>\r\n	<li>a</li>\r\n	<li>asd</li>\r\n	<li>sad</li>\r\n	<li>ads</li>\r\n	<li>ads</li>\r\n</ul>\r\n','<p>asdasdasd</p>\r\n\r\n<p>asd</p>\r\n\r\n<p>asda</p>\r\n\r\n<ol>\r\n	<li>adsasads</li>\r\n	<li>das</li>\r\n	<li>ads</li>\r\n	<li>ads</li>\r\n	<li>dsa</li>\r\n	<li>ads</li>\r\n</ol>\r\n',NULL,NULL,NULL,'',0,0,1,1,'1'),(45,9,6302,NULL,'2014-06-19 08:30:12','2014-06-19 08:30:25','<p>El venia a hacer DJP</p>\r\n','<p>No se le dio porqie sol ajdalkdlasdljad</p>\r\n',NULL,NULL,NULL,'',0,0,1,1,'4'),(46,9,6302,NULL,'2014-06-20 12:06:39','2014-06-20 12:06:43','<p>test 2</p>\r\n','<p>test 2</p>\r\n',NULL,NULL,NULL,'',0,0,1,1,'1'),(47,3,6302,'03-004-2014','2014-07-16 10:56:13','2014-07-16 11:01:35','<p>asd</p>\r\n','<p>dsa</p>\r\n',2,1,NULL,'<p>asd</p>\r\n',0,0,1,1,'4'),(48,4,6302,'04-005-2014','2014-07-16 10:56:13','2014-07-16 11:02:20','<p>test</p>\r\n','<p>test</p>\r\n',1,1,NULL,'<p>test</p>\r\n',0,0,1,1,'4'),(49,5,3769,'05-004-2014','2014-07-16 11:04:19','2014-07-16 11:04:50','<p>TEST</p>\r\n','<p>TEST</p>\r\n',1,1,NULL,'<p>TEST</p>\r\n',0,0,1,1,'4'),(50,1,6302,'01-018-2014','2014-07-16 11:05:02','2014-07-16 11:11:46','<p>dasddsaad</p>\r\n','<p>sdsaadsadsadsads</p>\r\n',2,2,NULL,'<p>asdadsdasdasads</p>\r\n',0,0,1,1,'4'),(51,3,6302,'03-005-2014','2014-07-16 11:05:02','2014-07-16 11:11:20','<p>TEST</p>\r\n','<p>TEST</p>\r\n',2,2,NULL,'<p>TEST</p>\r\n',0,0,1,1,'4'),(52,9,6302,NULL,'2014-07-16 11:18:25','2014-07-16 11:18:33','<p>TEST</p>\r\n','<p>TEST</p>\r\n',NULL,NULL,NULL,'<p>TEST</p>\r\n',0,0,1,1,'1'),(53,1,6302,'01-019-2014','2014-07-16 11:44:59','2014-08-20 11:04:49','<p>645156</p>\r\n','<p>15546</p>\r\n',2,2,NULL,'<p>456465</p>\r\n',0,0,10,1,'4');
/*!40000 ALTER TABLE `atenciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atencionprocedencias`
--

DROP TABLE IF EXISTS `atencionprocedencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atencionprocedencias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ciudadano_id` int(11) NOT NULL,
  `atencione_id` int(11) NOT NULL,
  `parroquia_id` varchar(6) DEFAULT NULL,
  `procedencia` varchar(1) NOT NULL DEFAULT 'I' COMMENT 'Institucion = I,\nParticular = P,\nComunidad = C',
  PRIMARY KEY (`id`),
  KEY `fk_atencionprocedencias_atenciones1_idx` (`atencione_id`),
  CONSTRAINT `fk_atencionprocedencias_atenciones1` FOREIGN KEY (`atencione_id`) REFERENCES `atenciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atencionprocedencias`
--

LOCK TABLES `atencionprocedencias` WRITE;
/*!40000 ALTER TABLE `atencionprocedencias` DISABLE KEYS */;
INSERT INTO `atencionprocedencias` VALUES (19,6104,3,'140605','C'),(20,3769,4,'141204','I'),(21,6302,5,'141204','I'),(22,3817,6,'140605','C'),(23,3817,7,'141205','I'),(24,3817,8,'030101','P'),(25,6104,9,'141201','P'),(26,6302,10,'141201','P'),(27,6104,11,'141205','I'),(28,6302,12,'140201','I'),(29,6302,16,'141201','P'),(30,6302,13,'141201','P'),(31,3817,17,'140201','I'),(34,6302,18,NULL,'P'),(35,6302,19,'140201','I'),(36,6302,24,'141501','I'),(37,6302,25,'140104','C'),(38,6302,26,NULL,'P'),(39,6302,27,'141205','I'),(40,6302,28,'880701','C'),(41,6302,29,NULL,'P'),(42,8035,34,NULL,'P'),(43,6302,36,NULL,'P'),(44,6302,37,NULL,'P'),(46,6302,38,'','P'),(47,6302,39,'','P'),(50,6302,40,'','P'),(51,3769,41,'','P'),(52,6302,42,'140104','I'),(53,6302,47,'','P'),(54,6302,48,'','P'),(55,3769,49,'','P'),(56,6302,51,'','P'),(57,6302,50,'','P'),(58,6302,53,'','P');
/*!40000 ALTER TABLE `atencionprocedencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atenciontipos`
--

DROP TABLE IF EXISTS `atenciontipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atenciontipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denominacion` varchar(200) DEFAULT NULL,
  `bgcolor` varchar(40) DEFAULT NULL,
  `btnicon` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atenciontipos`
--

LOCK TABLES `atenciontipos` WRITE;
/*!40000 ALTER TABLE `atenciontipos` DISABLE KEYS */;
INSERT INTO `atenciontipos` VALUES (1,'PeticiÃ³n','bg-emerald fg-white','icon-file-powerpoint'),(2,'AsesorÃ­a','bg-cobalt fg-white','icon-help-2'),(3,'Sugerencia','bg-amber fg-white','icon-info'),(4,'Reclamo','bg-teal fg-white','icon-clipboard-2'),(5,'Queja','bg-darkMagenta fg-white','icon-lightning'),(6,'Denuncia','bg-darkRed fg-white','icon-phone'),(9,'No concretada / Revisita','bg-yellow fg-black','icon-cancel');
/*!40000 ALTER TABLE `atenciontipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `denunciacomunidades`
--

DROP TABLE IF EXISTS `denunciacomunidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `denunciacomunidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denuncia_id` int(11) NOT NULL,
  `comunidade_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `denuncia_id_UNIQUE` (`denuncia_id`),
  KEY `fk_denunciacomunidades_denuncias1_idx` (`denuncia_id`),
  CONSTRAINT `fk_denunciacomunidades_denuncias1` FOREIGN KEY (`denuncia_id`) REFERENCES `denuncias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `denunciacomunidades`
--

LOCK TABLES `denunciacomunidades` WRITE;
/*!40000 ALTER TABLE `denunciacomunidades` DISABLE KEYS */;
INSERT INTO `denunciacomunidades` VALUES (2,2,50),(3,6,1),(4,8,607);
/*!40000 ALTER TABLE `denunciacomunidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `denunciaconsultas`
--

DROP TABLE IF EXISTS `denunciaconsultas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `denunciaconsultas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denuncia_id` int(11) NOT NULL,
  `ciudadano_id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_denunciaconsultas_denuncias1_idx` (`denuncia_id`),
  CONSTRAINT `fk_denunciaconsultas_denuncias1` FOREIGN KEY (`denuncia_id`) REFERENCES `denuncias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `denunciaconsultas`
--

LOCK TABLES `denunciaconsultas` WRITE;
/*!40000 ALTER TABLE `denunciaconsultas` DISABLE KEYS */;
INSERT INTO `denunciaconsultas` VALUES (1,1,6302,'Prueba de consulta de denuncia MODIFICADA2','2014-03-10 18:55:07'),(2,1,6302,'Viuno a consultar otra vez, se le dijo que todo OK','2014-03-10 19:30:22'),(3,5,6104,'El ciudadano vino a ver que paso con la consulta','2014-03-24 18:56:29');
/*!40000 ALTER TABLE `denunciaconsultas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `denunciadocumentos`
--

DROP TABLE IF EXISTS `denunciadocumentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `denunciadocumentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denuncia_id` int(11) NOT NULL,
  `originales` int(11) NOT NULL,
  `copias_cetificadas` int(11) NOT NULL,
  `paginas` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `denuncia_id_UNIQUE` (`denuncia_id`),
  KEY `fk_denunciadocumentos_denuncias1_idx` (`denuncia_id`),
  CONSTRAINT `fk_denunciadocumentos_denuncias1` FOREIGN KEY (`denuncia_id`) REFERENCES `denuncias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `denunciadocumentos`
--

LOCK TABLES `denunciadocumentos` WRITE;
/*!40000 ALTER TABLE `denunciadocumentos` DISABLE KEYS */;
INSERT INTO `denunciadocumentos` VALUES (1,3,1,0,15),(2,5,1,2,20);
/*!40000 ALTER TABLE `denunciadocumentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `denunciados`
--

DROP TABLE IF EXISTS `denunciados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `denunciados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` varchar(8) DEFAULT NULL,
  `nombres` varchar(200) NOT NULL,
  `apellidos` varchar(200) NOT NULL,
  `parroquia_id` varchar(6) DEFAULT NULL,
  `direccion_habitacion` text,
  `telefono_habitacion` varchar(11) DEFAULT NULL,
  `telefono_celular` varchar(11) DEFAULT NULL,
  `telefono_oficina` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `denunciados`
--

LOCK TABLES `denunciados` WRITE;
/*!40000 ALTER TABLE `denunciados` DISABLE KEYS */;
INSERT INTO `denunciados` VALUES (3,'11222333','Wuilliam','Moreno','141203','Av. Los prÃ³ceres, Residencias Rosa \"E\" Torre 1 Apto 7-28','04266784570','04266784570','04266784570'),(6,'22333444','Gilza','Carolina','150104','Av. Los prÃ³ceres, Residencias Rosa \"E\" Torre 1 Apto 7-27','04160792009','04160792009','04160792009'),(12,'17129072','Pedro','Marmol','151203','Santa Barbara',NULL,NULL,NULL),(13,'16444162','Gilza','Baleta','','Rosa E','','',''),(15,'','Maria','Juana','140104','','','','');
/*!40000 ALTER TABLE `denunciados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `denunciados_denuncias`
--

DROP TABLE IF EXISTS `denunciados_denuncias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `denunciados_denuncias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denuncia_id` int(11) NOT NULL,
  `denunciado_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_denuncias_has_denunciados_denunciados1_idx` (`denunciado_id`),
  KEY `fk_denuncias_has_denunciados_denuncias1_idx` (`denuncia_id`),
  CONSTRAINT `fk_denuncias_has_denunciados_denunciados1` FOREIGN KEY (`denunciado_id`) REFERENCES `denunciados` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_denuncias_has_denunciados_denuncias1` FOREIGN KEY (`denuncia_id`) REFERENCES `denuncias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `denunciados_denuncias`
--

LOCK TABLES `denunciados_denuncias` WRITE;
/*!40000 ALTER TABLE `denunciados_denuncias` DISABLE KEYS */;
INSERT INTO `denunciados_denuncias` VALUES (6,1,6),(10,2,12),(18,5,13),(20,4,6),(23,8,3);
/*!40000 ALTER TABLE `denunciados_denuncias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `denunciaorganismos`
--

DROP TABLE IF EXISTS `denunciaorganismos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `denunciaorganismos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denuncia_id` int(11) NOT NULL,
  `organismo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `denuncia_id_UNIQUE` (`denuncia_id`),
  KEY `fk_denunciaorganismos_denuncias1_idx` (`denuncia_id`),
  CONSTRAINT `fk_denunciaorganismos_denuncias1` FOREIGN KEY (`denuncia_id`) REFERENCES `denuncias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `denunciaorganismos`
--

LOCK TABLES `denunciaorganismos` WRITE;
/*!40000 ALTER TABLE `denunciaorganismos` DISABLE KEYS */;
INSERT INTO `denunciaorganismos` VALUES (1,1,2),(2,3,2),(3,4,2),(4,5,3),(5,7,2);
/*!40000 ALTER TABLE `denunciaorganismos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `denuncias`
--

DROP TABLE IF EXISTS `denuncias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `denuncias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ciudadano_id` int(11) NOT NULL,
  `nro_expediente` varchar(16) DEFAULT NULL,
  `created` datetime NOT NULL,
  `ended` datetime DEFAULT NULL,
  `descripcion_denuncia` text,
  `anexa_documentos` tinyint(1) NOT NULL DEFAULT '0',
  `organismo_denunciado` varchar(1) NOT NULL DEFAULT 'I' COMMENT 'I = Institucion,C = Comunidad Organizada',
  `paso` varchar(1) DEFAULT '1',
  `completada` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `denuncias`
--

LOCK TABLES `denuncias` WRITE;
/*!40000 ALTER TABLE `denuncias` DISABLE KEYS */;
INSERT INTO `denuncias` VALUES (1,6302,'OAC-01-02-14-001','2014-03-07 12:55:24','2014-03-27 13:38:34','<p>El ciudadano denuncia que se estan llevando los&nbsp;</p>\r\n',0,'I','3',1,1),(2,6302,'OAC-01-02-14-002','2014-03-14 20:07:37','2014-03-27 13:38:21','<p>Una denuncia mas de pruebas</p>\r\n',0,'C','3',1,1),(3,3817,'OAC-01-02-14-003','2014-03-15 20:26:46',NULL,'<p>jnunundnf ljijpfjf koijfojdf iijojdf</p>\r\n',1,'I','3',1,3),(4,6302,'OAC-01-02-14-004','2014-03-21 13:46:38','2014-03-27 13:38:10','<p>fsdsfdsfdsdf</p>\r\n',0,'I','3',1,1),(5,6104,'OAC-01-02-14-005','2014-03-24 18:37:24','2014-03-27 13:42:42','<p>Se <strong>estan llevando</strong> los cobres dice el <u>ciudadanoads</u> jads ljkad jkladljkalkjad jlkadsljk sadjladslkjal <em><strong>jkdaljkad jkladljkalkjad</strong></em> jlkadsljk sadjladslkjal jkdaljkad jkladljkalkjad jlkadsljk sadjladslkjal jkdaljkad jkladljkalkjad jlkadsljk sadjladslkjal <strong>jkdaljkad jkladljkalkjad jlkadsljk</strong> sadjladslkjal jkdaljkad jkladljkalkjad jlkadsljk sadjladslkjal jkdaljkad jkladljkalkjad jlkadsljk</p>\r\n\r\n<ol>\r\n	<li>sadjladslkjal <strong>jkdaassdlk alskdja djalksdj</strong> lajdlas dahf kdf dkashd adkasdkajshd kajdka hdkahdkahkd akd kd akjsdh kajda kjshda fdhga iuef ksadvn ,<strong>mvsiufdgasdhf avbsdmvshdkfasjdfa bvc</strong>&nbsp;</li>\r\n	<li>asdasdkljsadjkdasjklasd</li>\r\n	<li>asdlksalksdaljkadslkjdsalkad</li>\r\n	<li>adslkjsadlkjadsljkadsljkas</li>\r\n</ol>\r\n',1,'I','3',1,1),(6,6302,'OAC-01-02-14-006','2014-03-26 11:48:19',NULL,'<p>dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;dsaadsasdasd adsd asd&nbsp;</p>\r\n',0,'C','3',1,5),(7,6302,'OAC-01-02-14-007','2014-03-27 12:59:37','2014-03-27 13:36:45','<p>adsasdadsadaddas</p>\r\n',0,'I','3',1,1),(8,6302,'OAC-01-02-14-008','2014-03-27 13:37:28','2014-03-27 13:37:53','<p>adsdasadsads</p>\r\n',0,'C','3',1,1);
/*!40000 ALTER TABLE `denuncias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leyes`
--

DROP TABLE IF EXISTS `leyes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leyes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(400) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leyes`
--

LOCK TABLES `leyes` WRITE;
/*!40000 ALTER TABLE `leyes` DISABLE KEYS */;
INSERT INTO `leyes` VALUES (1,'Ley contra la corrupcion'),(2,'Ley de precios justos'),(3,'Ley de algo mas'),(4,'Otra ley');
/*!40000 ALTER TABLE `leyes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nivelinstrucciones`
--

DROP TABLE IF EXISTS `nivelinstrucciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nivelinstrucciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nivelinstrucciones`
--

LOCK TABLES `nivelinstrucciones` WRITE;
/*!40000 ALTER TABLE `nivelinstrucciones` DISABLE KEYS */;
INSERT INTO `nivelinstrucciones` VALUES (1,'No cursÃ³ estudios'),(2,'EducaciÃ³n primaria'),(3,'EducaciÃ³n bÃ¡sica'),(4,'TÃ©cnico medio'),(7,'TÃ©cnico superior universitario'),(8,'Profesional'),(9,'Estudios de 4to nivel'),(12,'Otro');
/*!40000 ALTER TABLE `nivelinstrucciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametros`
--

DROP TABLE IF EXISTS `parametros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  `valor` text,
  `descripcion` varchar(400) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametros`
--

LOCK TABLES `parametros` WRITE;
/*!40000 ALTER TABLE `parametros` DISABLE KEYS */;
INSERT INTO `parametros` VALUES (1,'ejercicio_fiscal','2014','Ejercicio fiscal'),(2,'denuncia_control','7','Control de denuncias'),(3,'control_1','19','Control de peticiones'),(4,'control_2','4','Control de asesorÃ­as'),(5,'control_3','5','Control de sugerencias'),(6,'control_4','5','Control de reclamos'),(7,'control_5','4','Control de quejas'),(8,'director_area','Rosaura CalderÃ³n','Jefe/a de la Oficina de AtenciÃ³n al Ciudadano'),(9,'pie_constancia','ResoluciÃ³n NÂ° 473 de fecha 22-07-2013, publicada en Gaceta Oficial del estado Bolivariano de MÃ©rida<br />NÂ° Extraordinario de fecha 22-07-2013.','Pie de la Constancia');
/*!40000 ALTER TABLE `parametros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfilciudadanos`
--

DROP TABLE IF EXISTS `perfilciudadanos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfilciudadanos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ciudadano_id` int(11) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `sexo` tinyint(1) NOT NULL,
  `estadocivile_id` int(11) NOT NULL,
  `nivelinstruccione_id` int(11) NOT NULL,
  `parroquia_id` varchar(6) NOT NULL,
  `tiene_telefonos` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ciudadano_id_UNIQUE` (`ciudadano_id`),
  KEY `user_id` (`fecha_nacimiento`),
  KEY `fk_perfilciudadanos_nivelinstrucciones1_idx` (`nivelinstruccione_id`),
  CONSTRAINT `fk_perfilciudadanos_nivelinstrucciones1` FOREIGN KEY (`nivelinstruccione_id`) REFERENCES `nivelinstrucciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfilciudadanos`
--

LOCK TABLES `perfilciudadanos` WRITE;
/*!40000 ALTER TABLE `perfilciudadanos` DISABLE KEYS */;
INSERT INTO `perfilciudadanos` VALUES (1,6302,'1986-02-07',1,2,7,'141201',1),(2,6104,'1975-02-15',1,1,0,'141201',0),(3,3769,'1980-07-27',1,1,3,'141210',1),(4,3,'1950-01-18',0,1,1,'140104',0),(7,8,'1970-01-20',0,1,1,'141501',0),(8,9,'1990-01-24',0,1,2,'140702',0),(9,8035,'1989-10-25',0,1,8,'141208',1);
/*!40000 ALTER TABLE `perfilciudadanos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procedenciacomunidades`
--

DROP TABLE IF EXISTS `procedenciacomunidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedenciacomunidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `atencionprocedencia_id` int(11) NOT NULL,
  `comunidade_id` int(11) NOT NULL,
  `responsabilidade_id` int(11) NOT NULL,
  `ocupacion` varchar(400) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `atencionprocedencia_id_UNIQUE` (`atencionprocedencia_id`),
  KEY `fk_procedenciacomunidades_atencionprocedencias1_idx` (`atencionprocedencia_id`),
  KEY `fk_procedenciacomunidades_responsabilidades1_idx` (`responsabilidade_id`),
  CONSTRAINT `fk_procedenciacomunidades_atencionprocedencias1` FOREIGN KEY (`atencionprocedencia_id`) REFERENCES `atencionprocedencias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_procedenciacomunidades_responsabilidades1` FOREIGN KEY (`responsabilidade_id`) REFERENCES `responsabilidades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedenciacomunidades`
--

LOCK TABLES `procedenciacomunidades` WRITE;
/*!40000 ALTER TABLE `procedenciacomunidades` DISABLE KEYS */;
INSERT INTO `procedenciacomunidades` VALUES (1,19,1,1,'Albanil'),(2,22,1,1,'Comerciante'),(3,37,273,1,'Obrero'),(4,40,1,1,'Obrero'),(5,46,1,1,'Comerciante');
/*!40000 ALTER TABLE `procedenciacomunidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procedenciaorganismos`
--

DROP TABLE IF EXISTS `procedenciaorganismos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedenciaorganismos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `atencionprocedencia_id` int(11) NOT NULL,
  `organismo_id` int(11) NOT NULL,
  `dependencia` varchar(400) NOT NULL,
  `cargo` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `atencionprocedencia_id_UNIQUE` (`atencionprocedencia_id`),
  KEY `fk_procedenciaorganismos_atencionprocedencias1_idx` (`atencionprocedencia_id`),
  CONSTRAINT `fk_procedenciaorganismos_atencionprocedencias1` FOREIGN KEY (`atencionprocedencia_id`) REFERENCES `atencionprocedencias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedenciaorganismos`
--

LOCK TABLES `procedenciaorganismos` WRITE;
/*!40000 ALTER TABLE `procedenciaorganismos` DISABLE KEYS */;
INSERT INTO `procedenciaorganismos` VALUES (5,20,6,'Direccion de Informatica','Analista de Sistemas'),(6,21,8,'Secretaria de Despacho','Coordinador'),(7,23,2,'Direccion de Talento Humano','Analista de Talento Humano'),(8,27,80,'Direccion de Comunicacion Corporativa','Comunicador Social'),(9,28,2,'Coordinacion de Deposito','Encargado del Deposito'),(10,31,2,'Despacho','Asistente Administrativo'),(12,35,2,'Departamento de Biblioteca','Bibliotecario'),(13,36,54,'Direccion de Comunicacion Corporativa','Analista de Sistemas'),(14,39,46,'Paisajismo','Arquitecto'),(15,46,2,'Coordinacion de Deposito','Encargado del Deposito'),(16,52,86,'Coordinacion de Deposito','Encargado del Deposito');
/*!40000 ALTER TABLE `procedenciaorganismos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `responsabilidades`
--

DROP TABLE IF EXISTS `responsabilidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `responsabilidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(400) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `responsabilidades`
--

LOCK TABLES `responsabilidades` WRITE;
/*!40000 ALTER TABLE `responsabilidades` DISABLE KEYS */;
INSERT INTO `responsabilidades` VALUES (1,'Miembro del consejo comunal'),(2,'Vocero o vocera de la unidad ejecutiva'),(3,'Vocero o vocera de la unidad administrativa y financiera comunitaria'),(4,'Vocero o vocera de la unidad de contralorÃ­a social'),(5,'Miembro de la comisiÃ³n comunal de contrataciones');
/*!40000 ALTER TABLE `responsabilidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefonos`
--

DROP TABLE IF EXISTS `telefonos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefonos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ciudadano_id` int(11) NOT NULL,
  `telefono_habitacion` varchar(11) DEFAULT NULL,
  `telefono_oficina` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefonos`
--

LOCK TABLES `telefonos` WRITE;
/*!40000 ALTER TABLE `telefonos` DISABLE KEYS */;
INSERT INTO `telefonos` VALUES (2,6302,'02742444982','02742512572'),(3,8035,'',''),(4,3769,'','');
/*!40000 ALTER TABLE `telefonos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-22 10:17:06
