<?php if ($in_pdf_mode): ?>
	<style type="text/css">
	<!--
	hr {
	    border: none;
	    border-top: 1px dashed #aaa;
	}
	table
	{
		font-size: 7.5pt;
	    width: 740px;
	    border: solid 1px #000;
	    border-collapse: collapse
	}

	th
	{
		font-size: 7.5pt;
	    text-align: left;
	    border: solid 1px #000;
	    font-weight: normal;
	    vertical-align: top;
	}

	td
	{
		font-size: 7.5pt;
	    text-align: left;
	    border: solid 1px #000;
	    font-weight: normal;
	    vertical-align: top;
	}
	h4, h5 { text-align: center; }
	h4 { margin: 0;}
	h5 {margin-top: 0.3em}
	img.cem { width: 60px; float: left;}
	img.escudo { width: 40px;}
	p { word-wrap: break-word; text-align: justify; font-weight: bold; padding: 0; margin:0}
	-->
	</style>

	<page backleft="5mm" backright="5mm" backbottom="20mm">
	    <img src="<?php echo APP . '/webroot/img/logo_cem.jpg'; ?>" class="cem" />
	    <h4><?php echo mb_strtoupper('Contraloría del estado bolivariano de Mérida'); ?></h4>
	    <h4><?php echo mb_strtoupper('Oficina de Atención al Ciudadano'); ?></h4>
	    <h5><?php echo mb_strtoupper('Reporte de atenciones'); ?></h5>
		<?php if ($atenciones): ?>
			<?php $i = 0; ?>
			<table cellpadding="0" cellspacing="0" class="table hovered">
				<thead>
					<tr>
						<th>Nro</th>
						<th><?php echo __('Nro Atencion'); ?></th>
						<th><?php echo __('Ciudadano atendido'); ?></th>
						<th><?php echo __('Created'); ?></th>
		                <th><?php echo __('Atencioncategoria'); ?></th>
		                <th><?php echo __('Atenciontipo'); ?></th>
		                <th><?php echo __('Atendido Por'); ?></th>
		                <th><?php echo __('Tiempo de atencion'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($atenciones as $atencione): ?>
						<tr>
		                    <td><?php echo ++$i; ?></td>
		                    <td><?php echo $atencione['Atencione']['nro_atencion']; ?></td>
		                    <td><?php echo $atencione['Ciudadano']['cedula']; ?> - <?php echo $atencione['Ciudadano']['nombres']; ?> <?php echo $atencione['Ciudadano']['apellidos']; ?></td>
		                    <td><?php echo h($this->Time->format('d-m-Y g:ia', $atencione['Atencione']['created'])); ?>&nbsp;</td>
		                    <td><?php echo ($atencione['Atencioncategoria']['nombre']) ? h(strip_tags(html_entity_decode($atencione['Atencioncategoria']['nombre']))) : '-'; ?>&nbsp;</td>
		                    <td><?php echo $atencione['Atenciontipo']['denominacion']; ?></td>
		                    <td><?php echo $atencione['User']['first_name']; ?> <?php echo $atencione['User']['last_name']; ?></td>
		                    <td><?php echo ($atencione['Atencione']['tiempo_atencion']) ? __dn('cake', '%d minute', '%d minutes', $atencione['Atencione']['tiempo_atencion'], $atencione['Atencione']['tiempo_atencion']) : __('Por finalizar'); ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p class="text-info"><?php echo __('No hay atenciones registradas con esos criterios de busqueda'); ?></p>
		<?php endif; ?>
	</page>
<?php else: ?>
	<?php $this->Html->addCrumb(__('Reportes'), '/reportes/reporte_atenciones'); ?>
	<div class="container reportes index">
		<div class="grid fluid">
			<div class="row">
				<div class="span12 bg-white padding20">
					<div class="span12 bg-white">
						<h2><?php echo __('Reporte de Atenciones'); ?></h2>
					</div>
					<div class="span12">
						<?php echo $this->Search->create('Reporte', array('inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12'))); ?>
							<fieldset>
								<legend><?php echo __('Filtros:'); ?></legend>
								<div class="row">
									<div class="span2">
										<?php echo $this->Search->input('nro_atencion', array('div' => array('class' => 'input-control text'))); ?>
									</div>
									<div class="span2">
										<?php echo $this->Search->input('atenciontipo_id', array('empty' => 'Todas', 'div' => array('class' => 'input-control select'))); ?>
									</div>
									<div class="span3">
										<?php echo $this->Search->input('atencioncategoria_id', array('empty' => 'Todas', 'div' => array('class' => 'input-control select'))); ?>
									</div>
									<div class="span3">
										<?php echo $this->Search->input('user_id', array('empty' => 'Todas', 'div' => array('class' => 'input-control select'))); ?>
									</div>
								</div>
							</fieldset>
							<div class="form-actions">
								<button type="submit" class="button success"><?php echo __('Buscar'); ?></button>
								<?php if ($atenciones): ?>
									<?php echo $this->Html->link('Enviar a PDF', array('action' => 'reporte_atenciones_pdf'), array('class' => 'button danger')); ?>
								<?php endif; ?>
							</div>
						<?php echo $this->Search->end(); ?>
						<?php echo $this->Form->create('Reporte', array('action' => 'reporte_atenciones_pdf')); ?>
							<?php echo $this->Form->input('source', array('value' => str_replace('reporte_atenciones',	'reporte_atenciones/pdf', $this->Html->url( null, true ) . $this->here))); ?>
						<?php echo $this->Form->end("Print PDF"); ?>
					</div>
					<?php if ($atenciones): ?>
						<?php $i = 0; ?>
						<table cellpadding="0" cellspacing="0" class="table hovered">
							<thead>
								<tr>
									<th>Nro</th>
	    							<th><?php echo __('Nro Atencion'); ?></th>
	    							<th><?php echo __('Ciudadano atendido'); ?></th>
	    							<th><?php echo __('Created'); ?></th>
	                                <th><?php echo __('Atencioncategoria'); ?></th>
	                                <th><?php echo __('Atenciontipo'); ?></th>
	                                <th><?php echo __('Atendido Por'); ?></th>
	                                <th><?php echo __('Tiempo de atencion'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($atenciones as $atencione): ?>
									<tr>
	                                    <td><?php echo ++$i; ?></td>
	                                    <td><?php echo $atencione['Atencione']['nro_atencion']; ?></td>
	                                    <td><?php echo $atencione['Ciudadano']['cedula']; ?> - <?php echo $atencione['Ciudadano']['nombres']; ?> <?php echo $atencione['Ciudadano']['apellidos']; ?></td>
	                                    <td><?php echo h($this->Time->format('d-m-Y g:ia', $atencione['Atencione']['created'])); ?>&nbsp;</td>
	                                    <td><?php echo ($atencione['Atencioncategoria']['nombre']) ? h(strip_tags(html_entity_decode($atencione['Atencioncategoria']['nombre']))) : '-'; ?>&nbsp;</td>
	                                    <td><?php echo $atencione['Atenciontipo']['denominacion']; ?></td>
	                                    <td><?php echo $atencione['User']['first_name']; ?> <?php echo $atencione['User']['last_name']; ?></td>
	                                    <td><?php echo ($atencione['Atencione']['tiempo_atencion']) ? __dn('cake', '%d minute', '%d minutes', $atencione['Atencione']['tiempo_atencion'], $atencione['Atencione']['tiempo_atencion']) : __('Por finalizar'); ?></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					<?php else: ?>
						<p class="text-info"><?php echo __('No hay atenciones registradas con esos criterios de busqueda'); ?></p>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>