<?php $this->Html->addCrumb(__('Reportes'), '/reportes/index'); ?>
<div class="container reportes index">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 bg-white padding20">
				<div class="span10 bg-white">
					<h2><?php echo __('Reportes'); ?></h2>
				</div>
				<?php if ($reportes): ?>
					<table cellpadding="0" cellspacing="0" class="table hovered sortable-table">
						<thead>
							<tr>
								<th><?php echo __('Nombre del reporte'); ?></th>
								<th class="actions span4"><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($reportes['Reporte'] as $reporte): ?>
								<tr>
									<td><?php echo h($reporte['name']); ?>&nbsp;</td>
									<td class="actions">
										<?php echo $this->Html->link('Ver<i class="icon-eye bg-green"></i>', array('action' => $reporte['action']), array('class' => 'image-button bg-darkGreen fg-white', 'title' => __('View'), 'escape' => false, 'target' => '_blank')); ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				<?php else: ?>
					<p class="text-info"><?php echo __('No hay reportes registrados'); ?></p>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>