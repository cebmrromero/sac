<?php $cakeDescription = __d('cake_dev', 'Sistema de Atencion al Ciudadano'); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="product" content="Metro UI CSS Framework">
		<meta name="description" content="Simple responsive css framework">
		<meta name="author" content="Sergey S. Pimenov, Ukraine, Kiev">
		<meta name="keywords" content="js, css, metro, framework, windows 8, metro ui">
		<?php
			echo $this->Html->meta('icon');
			
			echo $this->Html->css(array(
				'metro-bootstrap',
				'metro-bootstrap-responsive',
				'docs',
				'prettify',
				'flick/jquery-ui-1.10.4.custom.min',
				'main',
			));
		
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
			echo $this->Html->scriptBlock('var BASE_URL = "/sac/"');
		?>
		<title><?php echo $cakeDescription ?>: <?php echo $title_for_layout; ?></title>
	</head>
	<body class="metro" style="background-color: #efeae3">
		<?php $auth_user = $this->Session->read("Auth.User"); ?>
		<header class="bg-dark">
			<?php echo $this->element('top-nav', array("auth_user" => $auth_user)); ?>
		</header>
		
		<div class="">
			<div id="top-image">
				<div class="container" style="padding: 50px 20px">
					<h1 class="fg-white">SAC v1.0</h1>
					<h2 class="fg-white">
						<?php echo __("Sistema de Atencion al Ciudadano") ?><br />
						<?php echo __("Garantizando calidad en la atencion al publico.") ?>
					</h2>
				</div>
				<div class="container" style="padding: 20px 0px">
					<nav class="breadcrumbs">
						<?php echo $this->Html->getCrumbList(array('separator' => '', 'lastClass' => array('class' => 'active')), array('text' => '<i class="icon-home"></i>', 'escape' => false)); ?>
					</nav>
				</div>
			</div>
			
			<?php //if ($this->Session->check('Message.flash')): ?>
				<div class="container">
					<div class="grid fluid">
						<div class="row">
							<div class="span12 text-center">
								<?php echo $this->Session->flash(); ?>
								<?php echo $this->Session->flash('auth'); ?>
							</div>
						</div>
					</div>
				</div>
			<?php //endif; ?>
			
			<?php echo $this->fetch('content'); ?>
			
			
			<div class="bg-dark">
				<div class="container" style="padding: 10px 0;">
					<div class="grid no-margin">
						<div class="row no-margin">
							<div class="span3 padding10">
								<br />
								<!-- <a class="button success " style="width: 100%; margin-bottom: 5px"  href="http://192.168.0.134/trac/sac/">Trac</a>
								<a class="button info " style="width: 100%; margin-bottom: 5px"  href="http://192.168.0.134/svn/sac/">Subversion</a> -->
								<a class="button warning " style="width: 100%; margin-bottom: 5px;"  href="http://contraloriaestadomerida.gob.ve/contraloria_merida/">Portal de la Contraloría del estado Mérida</a>
							</div>
							<div class="span5 padding10">
								<h3 class="fg-white">Desarrollador</h3>
								<p class="fg-white">Wuilliam A. Lacruz M. - Analista de Sistemas I<br />
									<small>Dirección Técnica, Planificación y Control de Gestión - 2014</small></p>
							</div>
							<div class="span6 padding10">
								<h3 class="fg-white">Contraloría del estado Mérida</h3>
								<p class="fg-white">Oficina de Atención al Ciudadano (OAC)</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php 
			echo $this->Html->script(array(
				'jquery.min',
				//'jquery.widget.min',
				'jquery-ui-1.10.4.custom.min',
				'jquery.mousewheel',
				'ckeditor',
				'adapters/jquery',
				'prettify',
				'metro.min',
				'docs',
				'plugins/jquery-datatable/js/jquery.dataTables.min',
				'plugins/jquery-datatable/extra/js/TableTools.min',
				'plugins/datatables-responsive/js/datatables.responsive',
				'plugins/datatables-responsive/js/lodash.min',
				'datatables',
				'main',
			));
		?>
	</body>
</html>
