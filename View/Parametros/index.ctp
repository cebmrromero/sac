<?php $ciudadano = $this->Session->read('ciudadano'); ?>
<?php if ($ciudadano): ?>
	<?php echo $this->element('Atenciones/top-menu', array('atenciontipo_id' => null)); ?>
<?php endif; ?>
<?php $this->Html->addCrumb(__('Parametros'), '/parametros/index'); ?>
<div class="container parametros index">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 bg-white padding20">
				<div class="span10 bg-white">
					<h2><?php echo __('Parametros'); ?></h2>
				</div>
				<?php if ($parametros): ?>
					<table cellpadding="0" cellspacing="0" class="table hovered sortable-table">
						<thead>
							<tr>
								<th><?php echo $this->Paginator->sort('descripcion'); ?></th>
								<th><?php echo $this->Paginator->sort('nombre'); ?></th>
								<th><?php echo $this->Paginator->sort('valor'); ?></th>
								<th class="actions span1"><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($parametros as $parametro): ?>
								<tr>
									<td><?php echo h($parametro['Parametro']['descripcion']); ?>&nbsp;</td>
									<td><?php echo h($parametro['Parametro']['nombre']); ?>&nbsp;</td>
									<td><?php echo h($parametro['Parametro']['valor']); ?>&nbsp;</td>
									<td class="actions">
										<?php echo $this->Html->link('Editar<i class="icon-pencil bg-darkCyan"></i>', array('action' => 'edit', $parametro['Parametro']['id']), array('class' => 'image-button bg-darkCobalt fg-white', 'title' => __('View'), 'escape' => false)); ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				<?php else: ?>
					<p class="text-info"><?php echo __('No hay parametros registrados recientemente'); ?></p>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>