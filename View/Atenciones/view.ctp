<?php if ($ciudadano): ?>
    <?php echo $this->element('Atenciones/top-menu', array('atenciontipo_id' => null)); ?>
<?php endif; ?>
<?php $this->Html->addCrumb(__('Atenciones'), '/atenciones/main'); ?>
<?php $this->Html->addCrumb(__('Consultar'), '/atenciones/view/' . $atencione['Atencione']['id']); ?>
<div class="container atenciones view">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 bg-white padding20">
				<div class="span8 bg-white">
					<h2>
						<?php echo __('Consulta de %s ', $atencione['Atenciontipo']['denominacion']); ?>
						<?php if ($atencione['Atencione']['nro_atencion']): ?>
							<strong class="fg-white <?php echo $atencione['Atenciontipo']['bgcolor']; ?>">&nbsp;<?php echo h($atencione['Atencione']['nro_atencion']); ?>&nbsp;</strong>
						<?php endif; ?>
					</h2>
				</div>
				<div class="span4 actions bg-white text-right">
					<div class="tile half bg-red">
						<?php echo $this->Form->postLink(
							'<div class="tile-content icon"><i class="icon-remove"></i></div>',
							array('action' => 'delete', $atencione['Atencione']['id']),
							array(
								'class' => 'fg-white',
								'title' => __('Delete'),
								'escape' => false
							),
							__('Are you sure you want to delete # %s?', $atencione['Atencione']['id'])
						); ?>
					</div>
					<div class="tile half bg-orange">
						<?php echo $this->Html->link(
							'<div class="tile-content icon"><i class="icon-list"></i></div>',
							array('action' => 'main'),
							array(
								'class' => 'fg-white',
								'title' => __('List'),
								'escape' => false
							)
						); ?>
					</div>
					<?php if ($atencione['Atencione']['nro_atencion']): ?>
						<div class="tile half bg-blue">
							<?php echo $this->Html->link(
								'<div class="tile-content icon"><i class="icon-pencil"></i></div>',
								array('action' => 'continuar', $atencione['Atencione']['id'], $atencione['Atencione']['paso']),
								array(
									'class' => 'fg-white',
									'title' => __('Edit'),
									'escape' => false
								)
							); ?>
						</div>
					<?php else : ?>
						<div class="tile half bg-blue">
							<?php echo $this->Html->link(
								'<div class="tile-content icon"><i class="icon-pencil"></i></div>',
								array('action' => 'no_concretada', $atencione['Atencione']['id']),
								array(
									'class' => 'fg-white',
									'title' => __('Edit'),
									'escape' => false
								)
							); ?>
						</div>

					<?php endif; ?>
					<div class="tile half bg-green">
						<?php echo $this->Html->link(
							'<div class="tile-content icon"><i class="icon-plus"></i></div>',
							array('action' => 'add', $atencione['Atencione']['atenciontipo_id']),
							array(
								'class' => 'fg-white ',
								'title' => __('Add'),
								'escape' => false
							)
						); ?>
					</div>
					<?php if ($atencione['Atencione']['nro_atencion']): ?>
						<div class="tile half bg-yellow">
							<?php echo $this->Html->link(
								'<div class="tile-content icon"><i class="icon-printer"></i></div>',
								array('action' => 'planilla_pdf', $atencione['Atencione']['id']),
								array(
									'class' => 'fg-white ',
									'title' => __('Print'),
									'target' => '_blank',
									'escape' => false
								)
							); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="span6 bg-white padding20">
				<fieldset>
					<div class="row">
						<div class="span12">
							<?php echo $this->Form->create('Atencionconsulta', array(
								'url' => array('controller' => 'atencionconsultas', 'action' => 'add', $atencione['Atencione']['id']),
								'inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false)
							)); ?>
							<fieldset>
								<legend><?php echo __("Registro de la Consulta"); ?></legend>
								<?php
								echo $this->Form->label('descripcion', __('Motivo de la Consulta'));
								echo $this->Form->input('descripcion', array('type' => 'textarea', 'rows' => 5, 'autofocus' => true, 'placeholder' => ($ciudadano) ? '' : __('Para registrar una consulta debe seleccionar un ciudadano'), 'disabled' => ($ciudadano) ? false : true ));
								?>
							</fieldset>
							&nbsp;
							<div class="form-actions">
								<?php if ($ciudadano): ?>
									<button type="submit" class="button success"><?php echo __('Guardar la Consulta'); ?></button>
								<?php else: ?>
									<button type="button" class="button disabled" disabled="disabled"><?php echo __('Guardar la Consulta'); ?></button>
								<?php endif; ?>
							</div>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
					<div class="row">
						<div class="span12">
							<fieldset>
								<legend><?php echo __("Consultas a la %s", $atencione['Atenciontipo']['denominacion']); ?></legend>
								<?php if ($atencione['Atencionconsulta']): ?>
									<div class="listview-outlook" data-role="listview">
										<?php foreach ($atencione['Atencionconsulta'] as $atencionconsulta): ?>
											<a class="list show-consulta-details" href="#">
											    <div class="list-content">
												<span class="list-title"><?php //echo $atencionconsulta['Ciudadano']['cedula']; ?><?php echo $atencionconsulta['Ciudadano']['nombres']; ?> <?php echo $atencionconsulta['Ciudadano']['apellidos']; ?> <?php echo h($this->Time->format('d-m-Y g:ia', $atencionconsulta['created'])); ?></span>
												<span class="list-subtitle" data-url-edit="<?php echo $this->Html->url(array('controller' => 'atencionconsultas', 'action' => 'edit', $atencionconsulta['id'])); ?>" data-url-delete="<?php echo $this->Html->url(array('controller' => 'atencionconsultas', 'action' => 'delete', $atencionconsulta['id'])); ?>" alt="<?php echo $atencionconsulta['descripcion']; ?>"><?php echo $this->Text->truncate($atencionconsulta['descripcion'], 95); ?></span>
												<span class="list-remark fg-green"><span class="place-left icon-eye"></span> Click para ver detalles</span>
											    </div>
											</a>
										<?php endforeach; ?>
									</div>
								<?php else: ?>
									<p class="text-info"><?php echo __('No posee consultas.'); ?></p>
								<?php endif; ?>
							</fieldset>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="span6 bg-white padding20">
				<fieldset>
					<legend><?php echo __("Datos de la %s", $atencione['Atenciontipo']['denominacion']); ?></legend>
					<div class="row">
						<div class="span12">
							<dl>
								<dt><?php echo __('Ciudadano atendido'); ?></dt>
								<dd>
									<?php echo h($atencione['Ciudadano']['cedula']); ?> - <?php echo h($atencione['Ciudadano']['nombres']); ?> <?php echo h($atencione['Ciudadano']['apellidos']); ?>
									&nbsp;
								</dd>
							</dl>
						</div>
					</div>
					<div class="row">
						<div class="span12">
							<dl>
								<dt><?php echo __('Fecha Registro'); ?></dt>
								<dd>
									<?php echo h($this->Time->format('d-m-Y g:ia', $atencione['Atencione']['created'])); ?>
									&nbsp;
								</dd>
							</dl>
						</div>
					</div>
					<?php if ($atencione['Atencione']['nro_atencion']): ?>
						<div class="row">
							<div class="span12">
								<dl>
									<dt><?php echo __('Fundamentacion Legal'); ?></dt>
									<dd>
										<?php echo h($atencione['Leye']['nombre']); ?>
										&nbsp;
									</dd>
								</dl>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<dl>
									<dt><?php echo __('Atencioncategoria'); ?></dt>
									<dd>
										<?php echo h($atencione['Atencioncategoria']['nombre']); ?>
										&nbsp;
									</dd>
								</dl>
							</div>
						</div>
					<?php endif; ?>
					<div class="row">
						<div class="span12">
							<dl>
								<dt><?php echo __('Motivo Atencion'); ?></dt>
								<dd>
									<?php echo html_entity_decode(h($atencione['Atencione']['motivo_atencion'])); ?>
								</dd>
							</dl>
						</div>
					</div>
					<div class="row">
						<div class="span12">
							<dl>
								<dt><?php echo __('Atencion Brindada'); ?></dt>
								<dd>
									<?php echo html_entity_decode(h($atencione['Atencione']['atencion_brindada'])); ?>
								</dd>
							</dl>
						</div>
					</div>
				</fieldset>
			</div>
        </div>
    </div>
</div>
<?php if ($print): ?>
	<script type="text/javascript">
		window.open('<?php echo $this->Html->url(array('action' => 'planilla_pdf', $atencione['Atencione']['id'])); ?>', '_blank');
	</script>
<?php endif; ?>
