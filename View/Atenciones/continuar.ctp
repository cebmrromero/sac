<?php if ($ciudadano): ?>
    <?php echo $this->element('Atenciones/top-menu', array('atenciontipo_id' => null)); ?>
<?php endif; ?>
<?php $this->Html->addCrumb(__('Atenciones'), '/atenciones/main'); ?>
<?php $this->Html->addCrumb(__('Continuar'), "/atenciones/continuar/$id/$paso/"); ?>
<div class="container atenciones form">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 bg-white padding20">
				<div class="span3 bg-white">
					<h2><?php echo __('Registrar %s <strong>Paso %s</strong>', $atenciontipo['Atenciontipo']['denominacion'], $paso); ?></h2>
				</div>
				<div class="span7 bg-white my-stepper">
					<div class="stepper" data-steps="4" data-role="stepper" data-start="<?php echo $paso; ?>"></div>
				</div>
				<div class="span2 actions bg-white text-right">
					<div class="tile half bg-orange">
						<?php echo $this->Html->link(
							'<div class="tile-content icon"><i class="icon-list"></i></div>',
							array('action' => 'main'),
							array(
								'class' => 'fg-white',
								'title' => __('List'),
								'escape' => false
							)
						); ?>
					</div>
					<div class="tile half bg-red">
						<?php echo $this->Form->postLink(
							'<div class="tile-content icon"><i class="icon-remove"></i></div>',
							array('action' => 'delete', $id),
							array(
								'class' => 'fg-white',
								'title' => __('Delete'),
								'escape' => false
							),
							__('Are you sure you want to delete # %s?', $id)
						); ?>

					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="span12 bg-white padding20">
				<fieldset>
					<div class="row">
						<div class="span12">
							<?php echo $this->element('Atenciones/add/paso' . $paso, array('ciudadano' => $ciudadano)); ?>
						</div>
					</div>
				</fieldset>
			</div>
                </div>
        </div>
</div>