<?php $ciudadano = $this->Session->read('ciudadano'); ?>
<?php if ($ciudadano): ?>
    <?php echo $this->element('Atenciones/top-menu', array('atenciontipo_id' => null)); ?>
<?php endif; ?>
<?php $this->Html->addCrumb(__('Atenciones'), '/atenciones/main'); ?>
<div class="container atenciones index">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 bg-white padding20">
                <h2><?php echo __('Ultimas Atenciones'); ?></h2>
                <?php if ($atenciones): ?>
				    <table cellpadding="0" cellspacing="0" class="table hovered sortable-table-custom">
				        <thead>
    						<tr>
                                <th>Nro</th>
    							<th class="span2"><?php echo __('Nro Atencion'); ?></th>
    							<th><?php echo __('Ciudadano atendido'); ?></th>
    							<th><?php echo __('Created'); ?></th>
                                <th><?php echo __('Atencioncategoria'); ?></th>
                                <th><?php echo __('Atendido Por'); ?></th>
                                <th><?php echo __('Tiempo de atencion'); ?></th>
    						</tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; ?>
                            <?php foreach ($atenciones as $atencione): ?>
                                <tr>
                                    <td><?php echo ++$i; ?></td>
            						<?php if ($atencione['Atencione']['completada']): ?>
                                        <?php if (!$atencione['Atencione']['nro_atencion']): ?>
                                            <td><?php echo $this->Html->link($atencione['Atenciontipo']['denominacion'] . ' <i class="place-right icon-enter"></i>', array('action' => 'view', $atencione['Atencione']['id']), array('escape' => false, 'title' => $atencione['Atenciontipo']['denominacion'] . ' #' . $atencione['Atencione']['nro_atencion'], 'class' => 'button span12 ' . $atencione['Atenciontipo']['bgcolor'])); ?></td>
                                        <?php else: ?>
                                            <td><?php echo $this->Html->link($atencione['Atencione']['nro_atencion'] . ' <i class="place-right icon-enter"></i>', array('action' => 'view', $atencione['Atencione']['id']), array('escape' => false, 'title' => $atencione['Atenciontipo']['denominacion'] . ' #' . $atencione['Atencione']['nro_atencion'], 'class' => 'button span12 ' . $atencione['Atenciontipo']['bgcolor'])); ?></td>
                                        <?php endif; ?>
            						<?php else :?>
            							<td><?php echo $this->Html->link(__('Continuar') . ' <i class="place-right icon-enter"></i>', array('action' => 'continuar', $atencione['Atencione']['id'], $atencione['Atencione']['paso']), array('escape' => false, 'class' => 'button span12 ' . $atencione['Atenciontipo']['bgcolor'])); ?></td>
            						<?php endif; ?>
                                    <td><?php echo $atencione['Ciudadano']['cedula']; ?> - <?php echo $atencione['Ciudadano']['nombres']; ?> <?php echo $atencione['Ciudadano']['apellidos']; ?></td>
                                    <td><?php echo h($this->Time->format('d-m-Y g:ia', $atencione['Atencione']['created'])); ?>&nbsp;</td>
                                    <td><?php echo ($atencione['Atencioncategoria']['nombre']) ? h(strip_tags(html_entity_decode($atencione['Atencioncategoria']['nombre']))) : '-'; ?>&nbsp;</td>
                                    <td><?php echo $atencione['User']['first_name']; ?> <?php echo $atencione['User']['last_name']; ?></td>
                                    <td><?php echo ($atencione['Atencione']['tiempo_atencion']) ? __dn('cake', '%d minute', '%d minutes', $atencione['Atencione']['tiempo_atencion'], $atencione['Atencione']['tiempo_atencion']) : __('Por finalizar'); ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php else: ?>
                    <p class="text-info"><?php echo __('No hay atenciones registradas recientemente'); ?></p>
                <?php endif; ?>
			</div>
        </div>
		<!-- <div class="row">
			<div class="span12 bg-white padding20">
                <h2><?php //echo __('Ultimas Denuncias'); ?></h2>
                <?php //if ($denuncias): ?>
    				<table cellpadding="0" cellspacing="0" class="table hovered">
				        <thead>
    						<tr>
    							<th class="span2"><?php //echo __('Nro Expediente'); ?></th>
    							<th><?php //echo __('Ciudadano atendido'); ?></th>
    							<th><?php //echo __('Created'); ?></th>
                                <th><?php //echo __('Descripcion Denuncia'); ?></th>
                                <th><?php //echo __('Atendido Por'); ?></th>
                                <th><?php //echo __('Tiempo de atencion'); ?></th>
    						</tr>
                        </thead>
                        <tbody>
                            <?php //foreach ($denuncias as $denuncia): ?>
                                <tr>
            						<?php //if ($denuncia['Denuncia']['completada']): ?>
            							<td><?php //echo $this->Html->link($denuncia['Denuncia']['nro_expediente'] . ' <i class="place-right icon-enter"></i>', array('controller' => 'denuncias', 'action' => 'view', $denuncia['Denuncia']['id']), array('escape' => false, 'title' => __('Denuncia') . ' #' . $denuncia['Denuncia']['nro_expediente'], 'class' => 'button small span12 fg-white bg-darkRed')); ?></td>
            						<?php //else :?>
            							<td><?php //echo $this->Html->link(__('Continuar') . ' <i class="place-right icon-enter"></i>', array('controller' => 'denuncias', 'action' => 'continuar', $denuncia['Denuncia']['id'], $denuncia['Denuncia']['paso']), array('escape' => false, 'class' => 'button span12 fg-white bg-darkRed')); ?></td>
            						<?php //endif; ?>
                                    <td><?php //echo $denuncia['Ciudadano']['cedula']; ?><?php //echo $denuncia['Ciudadano']['nombres']; ?> <?php //echo $denuncia['Ciudadano']['apellidos']; ?></td>
                                    <td><?php //echo h($this->Time->format('d-m-Y g:ia', $denuncia['Denuncia']['created'])); ?>&nbsp;</td>
                                    <td><?php //echo $this->Text->truncate(h(strip_tags(html_entity_decode($denuncia['Denuncia']['descripcion_denuncia']))), 40); ?>&nbsp;</td>
                                    <td><?php //echo $denuncia['User']['first_name']; ?> <?php ///echo $denuncia['User']['last_name']; ?></td>
                                    <td><?php //echo ($denuncia['Denuncia']['tiempo_atencion']) ? $this->Time->timeAgoInWords(time() + $denuncia['Denuncia']['tiempo_atencion']) : __('Por finalizar'); ?></td>
                                </tr>
                            <?php //endforeach; ?>
                        </tbody>
                    </table>
                <?php //else: ?>
                    <p class="text-info"><?php //echo __('No hay denuncias registradas recientemente'); ?></p>
                <?php //endif; ?>
	       </div>
        </div> -->
    </div>
</div>