<?php if ($ciudadano): ?>
    <?php echo $this->element('Atenciones/top-menu', array('atenciontipo_id' => $atenciontipo_id)); ?>
<?php endif; ?>
<?php $this->Html->addCrumb(__('Atenciones'), '/atenciones/main'); ?>
<?php $this->Html->addCrumb(__('Add'), '/atenciones/add/' . $atenciontipo_id); ?>
<div class="container atenciones form">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 bg-white padding20">
				<div class="span3 bg-white">
					<h2><?php echo __('Registrar %s <strong>Paso %s</strong>', $atenciontipo['Atenciontipo']['denominacion'], $paso); ?></h2>
				</div>
				<div class="span7 bg-white my-stepper">
					<div class="stepper" data-steps="4" data-role="stepper" data-start="<?php echo $paso; ?>"></div>
				</div>
				<div class="span2 actions bg-white text-right">
					<div class="tile half bg-orange">
						<?php echo $this->Html->link(
							'<div class="tile-content icon"><i class="icon-list"></i></div>',
							array('action' => 'main'),
							array(
								'class' => 'fg-white',
								'title' => __('List'),
								'escape' => false
							)
						); ?>
					</div>
					<div class="tile half bg-green">
						<?php echo $this->Html->link(
							'<div class="tile-content icon"><i class="icon-plus"></i></div>',
							array('action' => 'add', $atenciontipo_id, $paso),
							array(
								'class' => 'fg-white',
								'title' => __('Add'),
								'escape' => false
							)
						); ?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="span12 bg-white padding20">
				<fieldset>
					<div class="row">
						<div class="span12">
							<?php echo $this->element('Atenciones/add/paso' . $paso, array('ciudadano' => $ciudadano)); ?>
						</div>
					</div>
				</fieldset>
			</div>
                </div>
        </div>
</div>