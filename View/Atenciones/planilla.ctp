<?php $meses = array(
    '01' => 'Enero',
    '02' => 'Febrero',
    '03' => 'Marzo',
    '04' => 'Abril',
    '05' => 'Mayo',
    '06' => 'Junio',
    '07' => 'Julio',
    '08' => 'Agosto',
    '09' => 'Septiembre',
    '10' => 'Octubre',
    '11' => 'Noviembre',
    '12' => 'Diciembre',
); ?>
<?php $procedencias = array('P' => __('Particular'), 'I' => __('Organo o Ente'), 'C' => __('Comunidad Organizada')) ?>
<?php // print_r($atencione['Atencionprocedencia']['procedencia']);die; ?>
<style type="text/css">
<!--
hr {
    border: none;
    border-top: 1px dashed #aaa;
}
table
{
	font-size: 7.5pt;
    width: 740px;
    border: solid 1px #000;
    border-collapse: collapse
}

th
{
	font-size: 7.5pt;
    text-align: left;
    border: solid 1px #000;
    font-weight: normal;
    vertical-align: top;
}

td
{
	font-size: 7.5pt;
    text-align: left;
    border: solid 1px #000;
    font-weight: normal;
    vertical-align: top;
}
h4, h5 { text-align: center; }
h4 { margin: 0;}
h5 {margin-top: 0.3em}
img.cem { width: 60px; float: left;}
img.escudo { width: 40px;}
p { word-wrap: break-word; text-align: justify; font-weight: bold; padding: 0; margin:0}
-->
</style>
<page backleft="5mm" backright="5mm" backbottom="20mm">
    <img src="<?php echo APP . '/webroot/img/logo_cem.jpg'; ?>" class="cem" />
    <h4><?php echo mb_strtoupper('Contraloría del estado bolivariano de Mérida'); ?></h4>
    <h4><?php echo mb_strtoupper('Oficina de Atención al Ciudadano'); ?></h4>
    <h5><?php echo mb_strtoupper('Control de Atención al Ciudadano'); ?></h5>
<table>
    <col style="width: 20%">
    <col style="width: 20%">
    <col style="width: 15%">
    <col style="width: 15%">
    <col style="width: 15%">
    <col style="width: 15%">
        <tr>
            <td style="background-color:#eee; font-size: 10pt !important" colspan="6"><?php echo mb_strtoupper('Datos del ciudadano'); ?></td>
       	</tr>
        <tr>
            <td>
            	<?php echo mb_strtoupper(__('Nro Atencion')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencione']['nro_atencion']); ?></p>
            </td>
            <td>
            	<?php echo mb_strtoupper(__('Created')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($this->Time->format('d-m-Y', $atencione['Atencione']['created'])); ?></p>
            </td>
            <td>
                <?php echo mb_strtoupper(__('Hora')); ?>:<br />
                <p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($this->Time->format('n:i a', $atencione['Atencione']['created'])); ?></p>
            </td>
            <td colspan="2">
                <?php echo mb_strtoupper(__('Procedencia')); ?>:<br />
                <p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($procedencias[$atencione['Atencionprocedencia']['procedencia']]); ?></p>
            </td>
            <td>
            	<?php echo mb_strtoupper(__('Modalidad')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Atenciontipo']['denominacion']); ?></p>
            </td>
        </tr>
        <tr>
            <td>
            	<?php echo mb_strtoupper(__('Cedula')); ?>:<br />
            	<p style="text-align: center; font-weight: bold">V-<?php echo mb_strtoupper(number_format($atencione['Ciudadano']['cedula'], 0, '', '.')); ?></p>
            </td>
            <td colspan="2">
            	<?php echo mb_strtoupper(__('Apellidos')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Ciudadano']['apellidos']); ?></p>
            </td>
            <td colspan="3">
            	<?php echo mb_strtoupper(__('Nombres')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Ciudadano']['nombres']); ?></p>
            </td>
        </tr>
        <tr>
            <td>
            	<?php echo mb_strtoupper(__('Fecha de Nacimiento')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($this->Time->format('d-m-Y', $atencione['Ciudadano']['Perfilciudadano']['fecha_nacimiento'])); ?></p>
            </td>
            <td>
            	<?php echo mb_strtoupper(__('Sexo')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper(($atencione['Ciudadano']['Perfilciudadano']['sexo']) ? 'Masculino' : 'Femenino'); ?></p>
            </td>
            <td>
            	<?php echo mb_strtoupper(__('Estadocivile')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Ciudadano']['Perfilciudadano']['Estadocivile']['denominacion']); ?></p>
            </td>
            <td colspan="3">
            	<?php echo mb_strtoupper(__('Nivelinstruccione')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Ciudadano']['Perfilciudadano']['Nivelinstruccione']['nombre']); ?></p>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="height:26px">
            	<?php echo mb_strtoupper(__('Direccion')); ?>:<br />
            	<p style="text-align: justify; font-weight: bold"><?php echo mb_strtoupper($atencione['Ciudadano']['direccion']); ?></p>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            	<?php echo mb_strtoupper(__('Estado')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Ciudadano']['Perfilciudadano']['Entidade']['nombre']); ?></p>
            </td>
            <td colspan="2">
            	<?php echo mb_strtoupper(__('Municipio')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Ciudadano']['Perfilciudadano']['Municipio']['nombre']); ?></p>
            </td>
            <td colspan="2">
            	<?php echo mb_strtoupper(__('Parroquia')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Ciudadano']['Perfilciudadano']['Parroquia']['nombre']); ?></p>
            </td>
        </tr>
        <tr>
            <td>
            	<?php echo mb_strtoupper(__('Telefono')); ?>:<br />
                <?php if ($atencione['Ciudadano']['telefono']): ?>
            	   <p style="text-align: center; font-weight: bold">
                        <?php echo sprintf("%s-%s.%s.%s", substr($atencione['Ciudadano']['telefono'], 0, 4), substr($atencione['Ciudadano']['telefono'], 4, 3), substr($atencione['Ciudadano']['telefono'], 7, 2), substr($atencione['Ciudadano']['telefono'], 9, 2)); ?></p>
                <?php endif; ?>
            </td>
            <td>
            	<?php echo mb_strtoupper(__('Telefono Habitacion')); ?>:<br />
                <?php if ($atencione['Ciudadano']['Telefono']['telefono_habitacion']): ?>
            	   <p style="text-align: center; font-weight: bold"><?php echo sprintf("%s-%s.%s.%s", substr($atencione['Ciudadano']['Telefono']['telefono_habitacion'], 0, 4), substr($atencione['Ciudadano']['Telefono']['telefono_habitacion'], 4, 3), substr($atencione['Ciudadano']['Telefono']['telefono_habitacion'], 7, 2), substr($atencione['Ciudadano']['Telefono']['telefono_habitacion'], 9, 2)); ?></p>
                <?php endif; ?>
            </td>
            <td colspan="2">
            	<?php echo mb_strtoupper(__('Telefono Oficina')); ?>:<br />
                <?php if ($atencione['Ciudadano']['Telefono']['telefono_oficina']): ?>
            	   <p style="text-align: center; font-weight: bold"><?php echo sprintf("%s-%s.%s.%s", substr($atencione['Ciudadano']['Telefono']['telefono_oficina'], 0, 4), substr($atencione['Ciudadano']['Telefono']['telefono_oficina'], 4, 3), substr($atencione['Ciudadano']['Telefono']['telefono_oficina'], 7, 2), substr($atencione['Ciudadano']['Telefono']['telefono_oficina'], 9, 2)); ?></p>
                <?php endif; ?>
            </td>
            <td colspan="2">
            	<?php echo mb_strtoupper(__('Email')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Ciudadano']['email']); ?></p>
            </td>
        </tr>

        <?php if ($atencione['Atencionprocedencia']['procedencia'] == 'I'): ?>
            <tr>
                <td style="background-color:#eee; font-size: 10pt !important" colspan="6">
                    <?php echo mb_strtoupper(__('Procedencia del Ciudadano')); ?>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <?php echo mb_strtoupper(__('Organo o Ente')); ?>:<br />
                    <p style="text-align: justify; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencionprocedencia']['Procedenciaorganismo']['Organismo']['nombre']); ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="height:26px">
                    <?php echo mb_strtoupper(__('Domicilio Fiscal')); ?>:<br />
                    <p style="text-align: justify; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencionprocedencia']['Procedenciaorganismo']['Organismo']['domicilio_fiscal']); ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php echo mb_strtoupper(__('Estado')); ?>:<br />
                    <p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencionprocedencia']['Entidade']['nombre']); ?></p>
                </td>
                <td colspan="2">
                    <?php echo mb_strtoupper(__('Municipio')); ?>:<br />
                    <p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencionprocedencia']['Municipio']['nombre']); ?></p>
                </td>
                <td colspan="2">
                    <?php echo mb_strtoupper(__('Parroquia')); ?>:<br />
                    <p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencionprocedencia']['Parroquia']['nombre']); ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <?php echo mb_strtoupper(__('Dependencia de adscripción')); ?>:<br />
                    <p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencionprocedencia']['Procedenciaorganismo']['dependencia']); ?></p>
                </td>
                <td colspan="3">
                    <?php echo mb_strtoupper(__('Cargo')); ?>:<br />
                    <p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencionprocedencia']['Procedenciaorganismo']['cargo']); ?></p>
                </td>
            </tr>

        <?php endif; ?>

        <?php if ($atencione['Atencionprocedencia']['procedencia'] == 'C'): ?>
            <tr>
                <td style="background-color:#eee; font-size: 10pt !important" colspan="6">
                    <?php echo mb_strtoupper(__('Procedencia del Ciudadano')); ?>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <?php echo mb_strtoupper(__('Comunidad Organizada')); ?>:<br />
                    <p style="text-align: justify; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencionprocedencia']['Procedenciacomunidade']['Comunidade']['nombre']); ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="height:26px">
                    <?php echo mb_strtoupper(__('Dirección')); ?>:<br />
                    <p style="text-align: justify; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencionprocedencia']['Procedenciacomunidade']['Comunidade']['ubicacion']); ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php echo mb_strtoupper(__('Estado')); ?>:<br />
                    <p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencionprocedencia']['Entidade']['nombre']); ?></p>
                </td>
                <td colspan="2">
                    <?php echo mb_strtoupper(__('Municipio')); ?>:<br />
                    <p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencionprocedencia']['Municipio']['nombre']); ?></p>
                </td>
                <td colspan="2">
                    <?php echo mb_strtoupper(__('Parroquia')); ?>:<br />
                    <p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencionprocedencia']['Parroquia']['nombre']); ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <?php echo mb_strtoupper(__('Responsabilidad')); ?>:<br />
                    <p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencionprocedencia']['Procedenciacomunidade']['Responsabilidade']['nombre']); ?></p>
                </td>
                <td colspan="3">
                    <?php echo mb_strtoupper(__('Ocupacion')); ?>:<br />
                    <p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencionprocedencia']['Procedenciacomunidade']['ocupacion']); ?></p>
                </td>
            </tr>

        <?php endif; ?>

        <tr style="background-color:#eee;">
            <td colspan="6" style="font-size: 10pt !important">
            	<?php echo mb_strtoupper(__('Datos de la atención')); ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
            	<?php echo mb_strtoupper(__('Fundamentacion Legal')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Leye']['nombre']); ?></p>
            </td>
            <td colspan="2">
            	<?php echo mb_strtoupper(__('Categoría de la atención')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Atencioncategoria']['nombre']); ?></p>
            </td>
        </tr>
        <tr>
            <?php if ($atencione['Atencionprocedencia']['procedencia'] == 'C'): ?>
                <td colspan="6" style="height:80px">
            <?php endif; ?>
            <?php if ($atencione['Atencionprocedencia']['procedencia'] == 'I'): ?>
                <td colspan="6" style="height:80px">
            <?php endif; ?>
            <?php if ($atencione['Atencionprocedencia']['procedencia'] == 'P'): ?>
                <td colspan="6" style="height:210px">
            <?php endif; ?>
            	<?php echo mb_strtoupper(__('Motivo de la atención')); ?>:<br />
            	<?php echo mb_strtoupper(str_replace('&nbsp;', ' ', htmlspecialchars_decode($atencione['Atencione']['motivo_atencion']))); ?>
            </td>
        </tr>
        <tr>
            <?php if ($atencione['Atencionprocedencia']['procedencia'] == 'C'): ?>
                <td colspan="6" style="height:170px">
            <?php endif; ?>
            <?php if ($atencione['Atencionprocedencia']['procedencia'] == 'I'): ?>
                <td colspan="6" style="height:170px">
            <?php endif; ?>
            <?php if ($atencione['Atencionprocedencia']['procedencia'] == 'P'): ?>
                <td colspan="6" style="height:190px">
            <?php endif; ?>
            	<?php echo mb_strtoupper(__('Atención brindada')); ?>:<br />
            	<?php echo mb_strtoupper(str_replace('&nbsp;', ' ', htmlspecialchars_decode($atencione['Atencione']['atencion_brindada']))); ?>
            </td>
        </tr>

        <tr style="border:none;">
            <td colspan="6" style="border:none; border-bottom:1px solid #000">
                &nbsp;
            </td>
        </tr>
<!--         <tr>
            <td colspan="6" style="height:20px">
            	<?php // echo mb_strtoupper(__('Observaciones')); ?>:<br />
            	<?php // echo mb_strtoupper(str_replace('&nbsp;', ' ', htmlspecialchars_decode($atencione['Atencione']['observaciones']))); ?>
            </td>
        </tr> -->

        <tr style="background-color:#eee;">
            <td colspan="6" style="font-size: 10pt !important">
            	<?php echo mb_strtoupper(__('Relación de Acompañantes')); ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
            	<?php echo mb_strtoupper(__('Nº')); ?>
            </td>
            <td style="text-align: center;" colspan="3">
            	<?php echo mb_strtoupper(__('Apellidos y Nombres')); ?>
            </td>
            <td style="text-align: center;" colspan="2">
            	<?php echo mb_strtoupper(__('Cedula')); ?>
            </td>
        </tr>
        <?php if ($atencione['Atencione']['tiene_acompanante']): ?>
        	<?php for ($i = 0; $i < 99; $i++): ?>
                <?php if ($i > 3) { break; } ?>
    	        <tr>
    	            <td>
    	            	<p style="text-align: center; font-weight: bold"><?php echo $i + 1; ?></p>
    	            </td>
                    <?php if (isset($atencione['Acompanante'][$i])): ?>
        	            <td colspan="3">
        	            	<p style="text-align: left; font-weight: bold"><?php echo mb_strtoupper($atencione['Acompanante'][$i]['Ciudadano']['apellidos'], 'iso-8859-1'); ?> <?php echo mb_strtoupper($atencione['Acompanante'][$i]['Ciudadano']['nombres'], 'iso-8859-1'); ?></p>
        	            </td>
        	            <td colspan="2">
        	            	<p style="text-align: center; font-weight: bold">V-<?php echo number_format($atencione['Acompanante'][$i]['Ciudadano']['cedula'], 0, '', '.'); ?></p>
        	            </td>
                    <?php else: ?>
                        <td colspan="3">
                            <p style="text-align: left; font-weight: bold"><hr /></p>
                        </td>
                        <td colspan="2">
                            <p style="text-align: center; font-weight: bold"><hr /></p>
                        </td>
                    <?php endif; ?>
    	        </tr>
                <?php if (($i > 2) && (sizeof($atencione['Acompanante']) - 4) > 0): ?>
                    <tr>
                        <td colspan="6">
                            <p style="text-align: left; font-weight: bold"><?php echo mb_strtoupper('Y ' . $num2text->ValorEnLetras(sizeof($atencione['Acompanante']) - 4) . '(' . (sizeof($atencione['Acompanante']) - 4) . ') ciudadanos más'); ?></p>
                        </td>
                    </tr>

                <?php endif; ?>
    	    <?php endfor; ?>
        <?php else: ?>
            <?php for ($i = 1; $i < 5; $i++): ?>
                <tr>
                    <td>
                        <p style="text-align: center; font-weight: bold"><?php echo $i; ?></p>
                    </td>
                    <td colspan="3">
                        <p style="text-align: left; font-weight: bold"><hr /></p>
                    </td>
                    <td colspan="2">
                        <p style="text-align: center; font-weight: bold"><hr /></p>
                    </td>
                </tr>
            <?php endfor; ?>
        <?php endif; ?>
        <tr style="border:none;">
            <td colspan="6" style="border:none; border-bottom:1px solid #000">
                &nbsp;
            </td>
        </tr>

        <tr style="background-color:#eee;">
            <td colspan="6" style="font-size: 10pt !important">
                <?php echo mb_strtoupper(__('Firma del ciudadano')); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo mb_strtoupper(__('Cedula')); ?>:<br />
                <p style="text-align: center; font-weight: bold">V-<?php echo mb_strtoupper(number_format($atencione['Ciudadano']['cedula'], 0, '', '.')); ?></p>
            </td>
            <td colspan="2">
                <?php echo mb_strtoupper(__('Nombres y Apellidos')); ?>:<br />
                <p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['Ciudadano']['nombres']); ?> <?php echo mb_strtoupper($atencione['Ciudadano']['apellidos']); ?></p>
            </td>
            <td>
                <?php echo mb_strtoupper(__('Fecha')); ?>:<br />
                <p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper(date('d-m-Y')); ?></p>
            </td>
            <td colspan="2">
                <?php echo mb_strtoupper(__('Firma')); ?>:<br />
                <p style="text-align: left; font-weight: bold">&nbsp;<br /><br /></p>
            </td>
        </tr>
        <tr style="border:none;">
            <td colspan="6" style="border:none; border-bottom:1px solid #000">
                &nbsp;
            </td>
        </tr>
        <tr style="background-color:#eee;">
            <td colspan="6" style="font-size: 10pt !important">
                <?php echo mb_strtoupper(__('Sólo para uso del funcionario')); ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
            	<?php echo mb_strtoupper(__('Funcionario Actuante')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($atencione['User']['first_name']); ?> <?php echo mb_strtoupper($atencione['User']['last_name']); ?></p>
                <p style="text-align: center;; font-weight: normal; font-size:90%"><?php echo mb_strtoupper($atencione['User']['cargo']); ?></p>
            </td>
            <td>
            	<?php echo mb_strtoupper(__('Fecha')); ?>:<br />
            	<p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper(date('d-m-Y')); ?></p>
            </td>
            <td colspan="2">
            	<?php echo mb_strtoupper(__('Firma')); ?>:<br />
            	<p style="text-align: left; font-weight: bold">&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <?php echo mb_strtoupper(__('Funcionario Revisor')); ?>:<br />
                <p style="text-align: center; font-weight: bold"><?php echo mb_strtoupper($director_area['Parametro']['valor']); ?></p>
                <p style="text-align: center; font-weight: normal; font-size:90%"><?php echo mb_strtoupper($director_area['Parametro']['descripcion']); ?></p>
            </td>
            <td>
                <?php echo mb_strtoupper(__('Fecha')); ?>:<br />
                <p style="text-align: center; font-weight: bold">&nbsp;</p>
            </td>
            <td colspan="2">
                <?php echo mb_strtoupper(__('Firma')); ?>:<br />
                <p style="text-align: left; font-weight: bold">&nbsp;</p>
            </td>
        </tr>


        <tr style="border:1px solid white !important">
            <td style="border:1px solid white !important">
            	&nbsp;
            </td>
            <td style="border:1px solid white !important">
            	&nbsp;
            </td>
            <td style="border:1px solid white !important">
            	&nbsp;
            </td>
            <td style="border:1px solid white !important">
            	&nbsp;
            </td>
            <td style="border:1px solid white !important">
            	&nbsp;
            </td>
            <td style="border:1px solid white !important">
            	&nbsp;
            </td>
        </tr>
</table>
    <page_footer backleft="5mm" backright="5mm" backbottom="20mm">
        <hr />
        <br />
        <table style="margin-left:20px;width:720px; border:none">
            <tr>
                <td style="text-align:center;text-transform: none; width:8%; border-right:none;border-bottom:none;padding:5px">
                    <img src="<?php echo APP . '/webroot/img/escudomerida.png'; ?>" class="escudo" /><br />
                </td>

                <td style="text-transform: none; text-align:left; width:52%;border-right:none;border-bottom:none;padding:5px">
                    <br />
                    <strong>REPÚBLICA BOLIVARIANA DE VENEZUELA</strong><br />
                    <strong>ESTADO BOLIVARIANO DE MÉRIDA</strong><br />
                    <strong>CONTRALORÍA DEL ESTADO BOLIVARIANO DE MÉRIDA</strong>
                </td>
                
                <td style="text-transform: none;border-bottom:none; width:40%;padding:5px">
                    <br />
                    <div style="text-align:right;"><?php echo mb_strtoupper("N° de atención"); ?>: <strong><?php echo mb_strtoupper($atencione['Atencione']['nro_atencion']); ?></strong></div>
                    <div style="text-align:right;"><?php echo mb_strtoupper("Fecha y hora de atención"); ?>: <strong><?php echo mb_strtoupper($this->Time->format('d-m-Y n:i a', $atencione['Atencione']['created'])); ?></strong></div>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-transform: none; text-align:justify; width:720px;padding:5px">
                    <br />
                    El/la ciudadano/a <strong><?php echo $atencione['Ciudadano']['nombres']; ?> <?php echo $atencione['Ciudadano']['apellidos']; ?></strong>, cédula de identidad Nº <strong>V-<?php echo number_format($atencione['Ciudadano']['cedula'], 0, '', '.'); ?></strong>, fue atendido en la Oficina de Atención al Ciudadano de la Contraloría del estado Bolivariano de Mérida, en fecha <?php echo mb_strtolower($num2text->ValorEnLetras($this->Time->format('d', $atencione['Atencione']['created']))); ?> (<?php echo $this->Time->format('d', $atencione['Atencione']['created']); ?>) de <?php echo mb_strtolower($meses[$this->Time->format('m', $atencione['Atencione']['created'])]); ?> del año <?php echo mb_strtolower($num2text->ValorEnLetras($this->Time->format('Y', $atencione['Atencione']['created']))); ?> (<?php echo $this->Time->format('Y', $atencione['Atencione']['created']); ?>), y quedó registrado bajo el N° de Atención <strong> <?php echo mb_strtoupper($atencione['Atencione']['nro_atencion']); ?></strong>.<br /><br />

                </td>
            </tr>
        </table>
        <br /><br /><br /><br /><br /><br /><br />
    </page_footer>
</page>