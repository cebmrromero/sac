<?php if ($ciudadano): ?>
    <?php echo $this->element('Atenciones/top-menu', array('atenciontipo_id' => null)); ?>
<?php endif; ?>
<?php $this->Html->addCrumb(__('Atenciones'), '/atenciones/main'); ?>
<?php $this->Html->addCrumb(__('No concretada'), "/atenciones/no_concretada"); ?>
<div class="container atenciones form">
    <div class="grid fluid">
        <div class="row">
            <div class="span12 bg-white padding20">
                <div class="span10 bg-white">
                    <h2><?php echo __('Registrar atención %s', $atenciontipo['Atenciontipo']['denominacion']); ?></h2>
                </div>
                <div class="span2 actions bg-white text-right">
                    <div class="tile half bg-orange">
                        <?php echo $this->Html->link(
                            '<div class="tile-content icon"><i class="icon-list"></i></div>',
                            array('action' => 'main'),
                            array(
                                'class' => 'fg-white',
                                'title' => __('List'),
                                'escape' => false
                            )
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="span12 bg-white padding20">
                <fieldset>
                    <div class="row">
                        <div class="span12">
                            <?php echo $this->Html->link(__('<i class="icon-arrow-left-5"></i> Volver al listado'), array('controller' => 'atenciones', 'action' => 'main'), array('class' => 'button', 'escape' => false)); ?><p>&nbsp;</p>
                            <?php echo $this->Form->create('Atencione', array('inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
                                <?php if (isset($this->request->data['Atencione']['id'])): ?>
                                    <?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
                                <?php endif; ?>
                                <fieldset>
                                    <legend><?php echo __('Datos propios de la Atencion'); ?></legend>
                                    <div class="row">
                                        <div class="span12 required">
                                            <?php echo $this->Form->input('created', array('type' => 'hidden', 'value' => date('Y-m-d H:i:s'))); ?>
                                            <?php echo $this->Form->input('completada', array('type' => 'hidden', 'value' => 1)); ?>
                                            <?php echo $this->Form->input('atenciontipo_id', array('type' => 'hidden', 'value' => 9)); ?>
                                            <?php echo $this->Form->input('ciudadano_id', array('type' => 'hidden', 'value' => $ciudadano['Ciudadano']['id_ciudadano'])); ?>
                                            <?php echo $this->Form->input('user_id', array('type' => 'hidden', 'value' => $auth_user['id'])); ?>
                                            <?php echo $this->Form->label('motivo_atencion'); ?>
                                            <?php echo $this->Form->input('motivo_atencion', array('type' => 'textarea', 'autofocus' => true, 'required' => true, 'class' => 'ckeditor', 'div' => array('class' => 'input-control textarea'))); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="span12 required">
                                            <?php echo $this->Form->label('atencion_brindada'); ?>
                                            <?php echo $this->Form->input('atencion_brindada', array('type' => 'textarea', 'required' => true, 'class' => 'ckeditor', 'div' => array('class' => 'input-control textarea'))); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="span12">
                                            <?php echo $this->Form->label('observaciones'); ?>
                                            <?php echo $this->Form->input('observaciones', array('type' => 'textarea', 'class' => 'ckeditor', 'div' => array('class' => 'input-control textarea'))); ?>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-actions">
                                        <button type="submit" class="button success"><?php echo __('Finalizar <i class="icon-checkmark"></i>'); ?></button>
                                </div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</div>