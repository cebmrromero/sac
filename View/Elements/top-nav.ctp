<?php $ciudadano = $this->Session->read('ciudadano'); ?>
<nav class="navigation-bar dark">
    <nav class="navigation-bar-content">
        <div class="element">
            <?php echo $this->Html->link(__('SAC v1.0 :: Menu'), '#', array('class' => 'dropdown-toggle')); ?>
            <ul class="dropdown-menu" data-role="dropdown">
                <li><?php echo $this->Html->link(__('Home'), array('controller' => 'atenciones', 'action' => 'main')); ?></li>
                <li class="divider"></li>
                <li><?php echo $this->Html->link(__('Ayudas'), array('controller' => 'pages', 'action' => 'ayudas')); ?></li>
                <li><?php echo $this->Html->link(__('Acerca de...'), array('controller' => 'pages', 'action' => 'about')); ?></li>
            </ul>
        </div>
        <span class="element-divider"></span>
        <?php if (isset($auth_user)): ?>
            <?php if (!$ciudadano): ?>
                <div class="element input-element">
                    <?php echo $this->Form->create('Ciudadano', array('url' => array('controller' => 'ciudadanos', 'action' => 'buscar'), 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'span3'))); ?>
                        <div class="input-control text">
                            <?php echo $this->Form->input('cedula', array('placeholder' => __('Cedula de Identidad'), 'autofocus' => true, 'after' => '<button class="btn-search"></button>')); ?>
                            <div class="cedula-help" data-content="<?php echo __('Para iniciar la consulta debe indicar la Cedula de Identidad del Ciudadano'); ?>">&nbsp;</div>
                        </div>
                        <small><?php echo __('Ej: 1122333'); ?></small>
                    <?php echo $this->Form->end(); ?>
                </div>
            <?php else: ?>
                <div class="element input-element">
                    <?php echo $this->Form->create('Ciudadano', array('url' => array('controller' => 'ciudadanos', 'action' => 'cambiar'), 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'span3'))); ?>
                        Atendiendo al Ciudadano: <img src="<?php echo $ciudadano['Ciudadano']['ruta_foto']; ?>" class='rounded ciudadano_img' /><strong><?php echo $ciudadano['Ciudadano']['cedula']; ?> - <?php echo $ciudadano['Ciudadano']['nombres']; ?> <?php echo $ciudadano['Ciudadano']['apellidos']; ?></strong>
                        <button class="button success exit"><?php echo __('Cambiar de Ciudadano'); ?></button>
                        <?php echo $this->Html->link(__('Modificar Datos'), array('controller' => 'ciudadanos', 'action' => 'edit', $ciudadano['Ciudadano']['id_ciudadano']), array('class' => 'button warning exit')); ?>
                    <?php echo $this->Form->end(); ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        
        <?php echo $this->Html->link('<span class="icon-help"></span>', array('controller' => 'pages', 'action' => 'display', 'ayudas'), array('class' => 'element place-right', 'escape' => false)); ?>
        <span class="element-divider place-right"></span>
        
        <?php if (!isset($auth_user)): ?>
            <?php echo $this->Html->link('<span class="icon-locked-2"></span>', array('controller' => 'users', 'action' => 'login'), array('class' => 'element place-right', 'escape' => false)); ?>
        <?php endif; ?>
        <?php if (isset($auth_user['group_id']) && $auth_user['group_id'] == 1): ?>
            <div class="element place-right">
                <a class="dropdown-toggle" href="#">
                    <span class="icon-cog"></span>
                </a>
                <ul class="dropdown-menu place-right" data-role="dropdown">
                        <li><?php echo $this->Html->link(__('Atenciontipos'), array('controller' => 'atenciontipos', 'action' => 'index')); ?></li>
                        <li><?php echo $this->Html->link(__('Atencioncategorias'), array('controller' => 'atencioncategorias', 'action' => 'index')); ?></li>
                        <li><?php echo $this->Html->link(__('Leyes'), array('controller' => 'leyes', 'action' => 'index')); ?></li>
                        <li><?php echo $this->Html->link(__('Responsabilidades'), array('controller' => 'responsabilidades', 'action' => 'index')); ?></li>
                        <li><?php echo $this->Html->link(__('Nivelinstrucciones'), array('controller' => 'nivelinstrucciones', 'action' => 'index')); ?></li>
                        <li class="divider"></li>
                        <li><?php echo $this->Html->link(__('Parametros'), array('controller' => 'parametros', 'action' => 'index')); ?></li>
                        <li class="divider"></li>
                        <li><?php echo $this->Html->link(__('Backup'), array('controller' => 'security', 'action' => 'backup')); ?></li>
                        <li><?php echo $this->Html->link(__('Restore'), array('controller' => 'security', 'action' => 'restore')); ?></li>
                        <li class="divider"></li>
                        <li><?php echo $this->Html->link(__('Reportes'), array('controller' => 'reportes', 'action' => 'index')); ?></li>
                    </li>
                </ul>
            </div>
        <?php endif; ?>
        <?php if (isset($auth_user['group_id']) && $auth_user['group_id'] == 9): ?>
            <div class="element place-right">
                <a class="dropdown-toggle" href="#">
                    <span class="icon-cog"></span>
                </a>
                <ul class="dropdown-menu place-right" data-role="dropdown">
                        <li><?php echo $this->Html->link(__('Atencioncategorias'), array('controller' => 'atencioncategorias', 'action' => 'index')); ?></li>
                        <li><?php echo $this->Html->link(__('Leyes'), array('controller' => 'leyes', 'action' => 'index')); ?></li>
                    </li>
                </ul>
            </div>
        <?php endif; ?>
        
        <span class="element-divider place-right"></span>
        <?php if (isset($auth_user)): ?>
            <!-- <button class="element image-button place-right"> -->
                <?php echo $this->Html->link('Salir', array('controller' => 'users', 'action' => 'logout'), array('class' => 'element image-button place-right danger')); ?>
            <!-- </button> -->
            <!-- <button class="element image-button place-right"> -->
                <?php echo $this->Html->link($auth_user['first_name'] . ' ' . $auth_user['last_name'], array('controller' => 'users', 'action' => 'profile'), array('class' => 'element image-button place-right')); ?>
            <!-- </button> -->
        <?php else: ?>
            <div class="element input-element  place-right">
                <?php echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'login'), 'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'span2'))); ?>
                    <div class="input-control text">
                        <?php echo $this->Form->input('username', array('placeholder' => __('Username'))); ?>
                    </div>
                    <div class="input-control text">
                        <?php echo $this->Form->input('password', array('placeholder' => __('Password'))); ?>
                    </div>
                    <button type="submit" class="button success"><i class="icon-unlocked"></i></button>
                <?php echo $this->Form->end(); ?>
            </div>
        <?php endif; ?>
    </nav>
</nav>