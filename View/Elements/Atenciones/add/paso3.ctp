<?php echo $this->Html->link(__('<i class="icon-arrow-left-5"></i> Volver al Paso 2'), array('action' => 'continuar', $id, 2), $options = array('class' => 'button', 'escape' => false)); ?><p>&nbsp;</p>
<?php echo $this->Form->create('Atencionprocedencia', array('inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
    <fieldset>
        <legend><?php echo __('Procedencia del/la Ciudadano(a)'); ?></legend>
        <div class="row">
            <div class="span12">
                <label><?php echo __('Procede de alguna institucion, comunidad o es particular?'); ?></label>
                <div id="procedencia" class="button-set" data-target="#AtencionprocedenciaProcedencia">
                    <?php foreach($procedencias as $k => $v): ?>
                        <?php echo $this->Html->link($v, array('action' => 'continuar', $id, 3, $k), array('class' => ($k == $procedencia_selected) ? 'button active' : 'button')); ?>
                    <?php endforeach; ?>
                </div>
                <?php if (isset($this->request->data['Atencionprocedencia']['id'])): ?>
                    <?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
                <?php endif; ?>
                <?php echo $this->Form->input('procedencia', array('type' => 'hidden', 'value' => $procedencia_selected)); ?>
                <?php if ($procedencia_selected == 'P'): ?>
                    <?php echo $this->Form->input('parroquia_id', array('type' => 'hidden', 'value' => '')); ?>
                <?php endif; ?>
            </div>
        </div>

        <?php if ($procedencia_selected == 'I'): ?>
            <div class="row">
                <div class="span12 required">
                    <?php if (isset($this->request->data['Procedenciaorganismo']['id'])): ?>
                        <?php echo $this->Form->input('Procedenciaorganismo.id'); ?>
                    <?php endif; ?>
                    <?php echo $this->Form->label('Procedenciaorganismo.organismo_id', __('Institucion u Organismo')); ?>
                    <?php echo $this->Form->input('Procedenciaorganismo.organismo_id', array('type' => 'hidden',  'empty' => true, 'data-url' => 'organismos/getByIdJson/', 'data-child' => '#PaiseId', 'div' => array('class' => 'input-control select'))); ?>
                    <?php echo $this->Form->input('Procedenciaorganismo.organismo_ac', array('type' => 'text', 'autofocus' => true, 'data-url' => 'organismos/getByNombreJSONP', 'class' => 'autocomplete', 'data-minkeys' => '5', 'data-target-update' => '#ProcedenciaorganismoOrganismoId', 'value' => (isset($this->request->data['Procedenciaorganismo']['Organismo']['nombre'])) ? $this->request->data['Procedenciaorganismo']['Organismo']['nombre'] : '')); ?>
                    <?php echo $this->Form->error('Procedenciaorganismo.organismo_id'); ?>
                </div>
            </div>
        
            <div class="row">
                <div class="span2">
                    <?php if (isset($this->request->data['Procedenciaorganismo']['id'])): ?>
                        <?php echo $this->Form->input('Procedenciaorganismo.atencionprocedencia_id', array('type' => 'hidden')); ?>
                    <?php endif; ?>
                    <?php echo $this->Form->label('Paise.id', __('Paise')); ?>
                    <?php echo $this->Form->input('Paise.id', array('type' => 'select', 'class' => 'pais-select', 'value' => (isset($organismo['Paise']['id'])) ? $organismo['Paise']['id'] : '', 'empty' => true, 'options' => $paises, 'data-child' => '#EntidadeId', 'data-url' => 'entidades/getAllByPaisIdJson/', 'div' => array('class' => 'input-control select'))); ?>
                </div>
                <div class="span3">
                    <?php echo $this->Form->label('Entidade.id', __('Entidade')); ?>
                    <?php echo $this->Form->input('Entidade.id', array('type' => 'select', 'class' => 'entidade-select', 'alt' => (isset($organismo['Entidade']['id'])) ? $organismo['Entidade']['id'] : '', 'empty' => true, 'options' => array(), 'data-child' => '#MunicipioId', 'data-url' => 'municipios/getAllByEntidadIdJson/', 'div' => array('class' => 'input-control select'))); ?>
                </div>
                <div class="span3">
                    <?php echo $this->Form->label('Municipio.id', __('Municipio')); ?>
                    <?php echo $this->Form->input('Municipio.id', array('type' => 'select', 'class' => 'municipio-select', 'alt' => (isset($organismo['Municipio']['id'])) ? $organismo['Municipio']['id'] : '', 'empty' => true, 'options' => array(), 'data-child' => '#AtencionprocedenciaParroquiaId', 'data-url' => 'parroquias/getAllByMunicipioIdJson/', 'div' => array('class' => 'input-control select'))); ?>
                </div>
                <div class="span4">
                    <?php echo $this->Form->label('parroquia_id', __('Parroquia')); ?>
                    <?php echo $this->Form->input('parroquia_id', array('type' => 'select', 'class' => 'parroquia-select', 'alt' => (isset($organismo['Parroquia']['id'])) ? $organismo['Parroquia']['id'] : '', 'empty' => true, 'options' => array(), 'div' => array('class' => 'input-control select'))); ?>
                </div>
            </div>
            <div class="row">
                <div class="span12">
                    <?php echo $this->Form->label('Procedenciaorganismo.domicilio_fiscal', __('Domicilio Fiscal')); ?>
                    <?php echo $this->Form->input('Procedenciaorganismo.domicilio_fiscal', array('type' => 'textarea', 'value' => (isset($this->request->data['Procedenciaorganismo']['Organismo']['domicilio_fiscal'])) ? $this->request->data['Procedenciaorganismo']['Organismo']['domicilio_fiscal'] : '', 'rows' => 2, 'div' => array('class' => 'input-control textarea'))); ?>
                </div>
            </div>
            
            <div class="row">
                <div class="span6">
                    <?php echo $this->Form->label('Procedenciaorganismo.dependencia', __('Dependencia')); ?>
                    <?php echo $this->Form->input('Procedenciaorganismo.dependencia', array('data-url' => 'procedenciaorganismos/getDependenciasJSONP', 'data-minkeys' => '5', 'class' => 'autocomplete')); ?>
                </div>
                <div class="span6">
                    <?php echo $this->Form->label('Procedenciaorganismo.cargo', __('Cargo')); ?>
                    <?php echo $this->Form->input('Procedenciaorganismo.cargo', array('data-url' => 'procedenciaorganismos/getCargosJSONP', 'data-minkeys' => '3', 'class' => 'autocomplete')); ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if ($procedencia_selected == 'C'): ?>
            <div class="row">
                <div class="span12 required">
                    <?php if (isset($this->request->data['Procedenciacomunidade']['id'])): ?>
                        <?php echo $this->Form->input('Procedenciacomunidade.id'); ?>
                    <?php endif; ?>
                    <?php echo $this->Form->label('Procedenciacomunidade.comunidade_id', __('Comunidad Organizada')); ?>
                    <?php echo $this->Form->input('Procedenciacomunidade.comunidade_id', array('type' => 'hidden', 'empty' => true, 'data-url' => 'comunidades/getByIdJson/', 'data-child' => '#PaiseId', 'div' => array('class' => 'input-control select'))); ?>
                    <?php echo $this->Form->input('Procedenciacomunidade.comunidade_ac', array('type' => 'text', 'autofocus' => true, 'data-url' => 'comunidades/getByNombreJSONP', 'class' => 'autocomplete', 'data-minkeys' => '5', 'data-target-update' => '#ProcedenciacomunidadeComunidadeId', 'value' => (isset($this->request->data['Procedenciacomunidade']['Comunidade']['nombre'])) ? $this->request->data['Procedenciacomunidade']['Comunidade']['nombre'] : '')); ?>
                    <?php echo $this->Form->error('Procedenciacomunidade.comunidade_id'); ?>
                </div>
            </div>
        
            <div class="row">
                <div class="span2">
                    <?php if (isset($this->request->data['Procedenciacomunidade']['id'])): ?>
                        <?php echo $this->Form->input('Procedenciacomunidade.atencionprocedencia_id', array('type' => 'hidden')); ?>
                    <?php endif; ?>
                    <?php echo $this->Form->label('Paise.id', __('Paise')); ?>
                    <?php echo $this->Form->input('Paise.id', array('type' => 'select', 'class' => 'pais-select', 'value' => (isset($comunidade['Paise']['id'])) ? $comunidade['Paise']['id'] : '', 'empty' => true, 'options' => $paises, 'data-child' => '#EntidadeId', 'data-url' => 'entidades/getAllByPaisIdJson/', 'div' => array('class' => 'input-control select'))); ?>
                </div>
                <div class="span3">
                    <?php echo $this->Form->label('Entidade.id', __('Entidade')); ?>
                    <?php echo $this->Form->input('Entidade.id', array('type' => 'select', 'class' => 'entidade-select', 'alt' => (isset($comunidade['Entidade']['id'])) ? $comunidade['Entidade']['id'] : '', 'empty' => true, 'options' => array(), 'data-child' => '#MunicipioId', 'data-url' => 'municipios/getAllByEntidadIdJson/', 'div' => array('class' => 'input-control select'))); ?>
                </div>
                <div class="span3">
                    <?php echo $this->Form->label('Municipio.id', __('Municipio')); ?>
                    <?php echo $this->Form->input('Municipio.id', array('type' => 'select', 'class' => 'municipio-select', 'alt' => (isset($comunidade['Municipio']['id'])) ? $comunidade['Municipio']['id'] : '', 'empty' => true, 'options' => array(), 'data-child' => '#AtencionprocedenciaParroquiaId', 'data-url' => 'parroquias/getAllByMunicipioIdJson/', 'div' => array('class' => 'input-control select'))); ?>
                </div>
                <div class="span4 required">
                    <?php echo $this->Form->label('parroquia_id', __('Parroquia')); ?>
                    <?php echo $this->Form->input('parroquia_id', array('type' => 'select', 'class' => 'parroquia-select', 'alt' => (isset($comunidade['Parroquia']['id'])) ? $comunidade['Parroquia']['id'] : '', 'empty' => true, 'required' => 'required', 'options' => array(), 'div' => array('class' => 'input-control select'))); ?>
                </div>
            </div>
            <div class="row">
                <div class="span12 required">
                    <?php echo $this->Form->label('Procedenciacomunidade.ubicacion', __('Ubicacion')); ?>
                    <?php echo $this->Form->input('Procedenciacomunidade.ubicacion', array('type' => 'textarea', 'required' => 'required', 'value' => (isset($this->request->data['Procedenciacomunidade']['Comunidade']['ubicacion'])) ? $this->request->data['Procedenciacomunidade']['Comunidade']['ubicacion'] : '', 'rows' => 2, 'div' => array('class' => 'input-control textarea'))); ?>
                </div>
            </div>
            
            <div class="row">
                <div class="span6 required">
                    <?php echo $this->Form->label('Procedenciacomunidade.responsabilidade_id', __('Responsabilidade')); ?>
                    <?php echo $this->Form->input('Procedenciacomunidade.responsabilidade_id', array('empty' => true, 'required' => 'required', 'div' => array('class' => 'input-control select'))); ?>
                </div>
                <div class="span6">
                    <?php echo $this->Form->label('Procedenciacomunidade.ocupacion', __('Ocupacion')); ?>
                    <?php echo $this->Form->input('Procedenciacomunidade.ocupacion', array('data-url' => 'procedenciacomunidades/getOcupacionesJSONP', 'data-minkeys' => '3', 'class' => 'autocomplete')); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (isset($procedencia_selected) && !empty($procedencia_selected)): ?>
            <?php echo $this->Form->input('atencione_id', array('type' => 'hidden')); ?>
            <?php echo $this->Form->input('ciudadano_id', array('type' => 'hidden')); ?>
        <?php endif; ?>
    </fieldset>
    <?php if (isset($procedencia_selected) && !empty($procedencia_selected)): ?>
        <div class="form-actions">
            <?php echo $this->Html->link(__('<i class="icon-arrow-left-5"></i> Volver al Paso 2'), array('action' => 'continuar', $id, 2), $options = array('class' => 'button', 'escape' => false)); ?>
            <button type="submit" class="button success"><?php echo __('Continuar <i class="icon-arrow-right-5"></i>'); ?></button>
        </div>
    <?php endif; ?>
<?php echo $this->Form->end(); ?>