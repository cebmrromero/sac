<?php echo $this->Html->link(__('<i class="icon-arrow-left-5
"></i> Volver al Paso 3'), array('action' => 'continuar', $this->request->data['Atencione']['id'], 3), $options = array('class' => 'button', 'escape' => false)); ?><p>&nbsp;</p>
<?php echo $this->Form->create('Atencione', array('inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
    <fieldset>
        <legend><?php echo __('Datos propios de la Atencion'); ?></legend>
        <div class="row">
            <div class="span6 required">
                <?php echo $this->Form->label('leye_id', __('Fundamentacion Legal')); ?>
                <?php echo $this->Form->input('leye_id', array('empty' => true, 'div' => array('class' => 'input-control select'))); ?>
            </div>
            <div class="span6 required">
                <?php echo $this->Form->label('atencioncategoria_id'); ?>
                <?php echo $this->Form->input('atencioncategoria_id', array('empty' => true, 'div' => array('class' => 'input-control select'))); ?>
            </div>
        </div>
        <div class="row">
            <div class="span12 required">
                <?php echo $this->Form->input('id'); ?>
                <?php echo $this->Form->input('nro_atencion', array('type' => 'hidden')); ?>
                <?php echo $this->Form->input('atenciontipo_id', array('type' => 'hidden')); ?>
                <?php echo $this->Form->label('motivo_atencion'); ?>
                <?php echo $this->Form->input('motivo_atencion', array('type' => 'textarea', 'autofocus' => true, 'required' => true, 'class' => 'ckeditor motivo', 'div' => array('class' => 'input-control textarea'))); ?>
            </div>
        </div>
        <div class="row">
            <div class="span12 required">
                <?php echo $this->Form->label('atencion_brindada'); ?>
                <?php echo $this->Form->input('atencion_brindada', array('type' => 'textarea', 'required' => true, 'class' => 'ckeditor atencion', 'div' => array('class' => 'input-control textarea'))); ?>
            </div>
        </div>
        <div class="row">
            <div class="span12">
                <?php echo $this->Form->label('observaciones'); ?>
                <?php echo $this->Form->input('observaciones', array('type' => 'textarea', 'class' => 'ckeditor observacion', 'div' => array('class' => 'input-control textarea'))); ?>
                <?php echo $this->Form->input('ended', array('type' => 'hidden')); ?>
            </div>
        </div>
        <?php if ($auth_user['group_id'] == 1): ?>
            <div class="row">
                <div class="span12">
                    <?php echo $this->Form->label('user_id'); ?>
                    <?php echo $this->Form->input('user_id', array('div' => array('class' => 'input-control textarea'))); ?>
                </div>
            </div>
        <?php endif; ?>
    </fieldset>
    <div class="form-actions">
            <?php echo $this->Html->link(__('<i class="icon-arrow-left-5"></i> Volver al Paso 3'), array('action' => 'continuar', $this->request->data['Atencione']['id'], 3), $options = array('class' => 'button', 'escape' => false)); ?>
            <button type="submit" class="button success"><?php echo __('Finalizar <i class="icon-checkmark"></i>'); ?></button>
    </div>
<?php echo $this->Form->end(); ?>