<?php echo $this->Html->link(__('<i class="icon-arrow-left-5"></i> Volver al listado'), array('controller' => 'atenciones', 'action' => 'main'), array('class' => 'button', 'escape' => false)); ?><p>&nbsp;</p>
<fieldset>
    <legend><?php echo __('Datos del/los Acompanante(s)'); ?></legend>
    <div class="row">
        <div class="span3">
            <?php echo $this->Form->create('Atencione', array('inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
                <label><?php echo __('Cedula del Acompanante'); ?></label>
                <?php echo $this->Form->input('id'); ?>
                <?php echo $this->Form->input('Acompanante.cedula', array('data-url' => 'ciudadanos/getCedulasJSONP', 'data-minkeys' => '5', 'class' => 'autocomplete', 'after' => '<button class="btn-search"></button>', 'required' => 'required', 'autofocus' => true)); ?>
                <p class="text-alert text-center"><?php echo __('Si no vino acompanado, presione <strong>continuar</strong>'); ?></p>
            <?php echo $this->Form->end(); ?>
        </div>
        <div class="span5 offset1">
            <fieldset>
                <legend><?php echo __('Listado de Acompanantes'); ?></legend>
                <div class="listview-outlook" data-role="listview">
                    <?php if (!empty($this->request->data['Acompanante'])): ?>
                        <?php foreach ($this->request->data['Acompanante'] as $acompanante): ?>
                            <?php $url = '
                                <div class="list-content">
                                    <span class="list-title">%s: %s</span>
                                    <span class="list-subtitle">%s: %s %s</span>
                                    <span class="list-remark fg-darkRed"><span class="place-left fg-red icon-remove"></span> Click para quitar de la lista</span>
                                </div>';
                            $url = sprintf($url, __('Cedula'), $acompanante['Ciudadano']['cedula'], __('Ciudadano(a)'), $acompanante['Ciudadano']['apellidos'], $acompanante['Ciudadano']['nombres'])
                            ?>
                            <?php echo $this->Form->postLink($url, array('controller' => 'acompanantes', 'action' => 'delete', $acompanante['id']), array('class' => 'list', 'escape' => false), __('Are you sure you want to delete # %s?', $acompanante['id'])); ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <p class="text-info"><?php echo __('Aun no hay acompanantes registrados en la presente atencione'); ?></p>
                    <?php endif; ?>
                </div>
            </fieldset>
        </div>
    </div>
</fieldset>
<div class="form-actions">
    <?php echo $this->Html->link(__('Continuar  <i class="icon-arrow-right-5
"></i>'), array('action' => 'continuar', $this->request->data['Atencione']['id'], 3), array('class' => 'button success', 'escape' => false)); ?>
</div>