<div class="container atenciones index">
	<div class="grid fluid">
		<div class="row">
			<div class="span2">
				<a href="<?php echo $this->Html->url(array('controller' => 'atenciones', 'action' => 'add', '1')); ?>" class="span12 tile <?php echo ($atenciontipo_id == '1') ? 'selected ': ''; ?>bg-emerald fg-white">
				<div class="tile-content icon">
					<i class="icon-file-powerpoint"></i>
				</div>
				<div class="brand">
					<span class="label fg-white"><?php echo __('Nueva Peticion'); ?></span>
					<span class="badge bg-white fg-emerald"><i class="icon-plus"></i></span>
				</div>
				</a>
			</div>
			<div class="span2">
				<a href="<?php echo $this->Html->url(array('controller' => 'atenciones', 'action' => 'add', '2')); ?>" class="span12 tile <?php echo ($atenciontipo_id == '2') ? 'selected ': ''; ?>bg-cobalt fg-white">
				<div class="tile-content icon">
					<i class="icon-help-2"></i>
				</div>
				<div class="brand">
					<span class="label fg-white"><?php echo __('Nueva Asesoria'); ?></span>
					<span class="badge bg-white fg-cobalt"><i class="icon-plus"></i></span>
				</div>
				</a>
			</div>
			<div class="span2">
				<a href="<?php echo $this->Html->url(array('controller' => 'atenciones', 'action' => 'add', '3')); ?>" class="span12 tile <?php echo ($atenciontipo_id == '3') ? 'selected ': ''; ?>bg-amber fg-white">
				<div class="tile-content icon">
					<i class="icon-info"></i>
				</div>
				<div class="brand">
					<span class="label fg-white"><?php echo __('Nueva Sugerencia'); ?></span>
					<span class="badge bg-white fg-amber"><i class="icon-plus"></i></span>
				</div>
				</a>
			</div>
			<div class="span2">
				<a href="<?php echo $this->Html->url(array('controller' => 'atenciones', 'action' => 'add', '4')); ?>" class="span12 tile <?php echo ($atenciontipo_id == '4') ? 'selected ': ''; ?>bg-teal fg-white">
				<div class="tile-content icon">
					<i class="icon-clipboard-2"></i>
				</div>
				<div class="brand">
					<span class="label fg-white"><?php echo __('Nuevo Reclamo'); ?></span>
					<span class="badge bg-white fg-teal"><i class="icon-plus"></i></span>
				</div>
				</a>
			</div>
			<div class="span2">
				<a href="<?php echo $this->Html->url(array('controller' => 'atenciones', 'action' => 'add', '5')); ?>" class="span12 tile <?php echo ($atenciontipo_id == '5') ? 'selected ': ''; ?>bg-darkMagenta fg-white">
				<div class="tile-content icon">
					<i class="icon-lightning"></i>
				</div>
				<div class="brand">
					<span class="label fg-white"><?php echo __('Nueva Queja'); ?></span>
					<span class="badge bg-white fg-darkMagenta"><i class="icon-plus"></i></span>
				</div>
				</a>
			</div>
			<div class="span2">
				<a href="<?php echo $this->Html->url(array('controller' => 'atenciones', 'action' => 'no_concretada')); ?>" class="span12 tile <?php echo ($atenciontipo_id == '9') ? 'selected ': ''; ?>bg-yellow fg-black">
				<div class="tile-content icon">
					<i class="icon-cancel fg-black"></i>
				</div>
				<div class="brand">
					<span class="label fg-black"><?php echo __('No concretada /<br />Revisita'); ?></span>
					<span class="badge bg-black fg-yellow"><i class="icon-plus"></i></span>
				</div>
				</a>
			</div>
			<!-- <div class="span2" title="En Desarrollo" data-hint="Atención|Sección en desarrollo" data-hint-position="top">
				<a href="#" onclick="return false" class="span12 tile bg-darker fg-gray">
				<div class="tile-content icon">
					<i class="icon-phone fg-gray"></i>
				</div>
				<div class="brand">
					<span class="label fg-gray"><?php //echo __('Nueva Denuncia'); ?></span>
					<span class="badge bg-gray fg-darker"><i class="icon-plus"></i></span>
				</div>
				</a>
			</div> -->
			<!--
			<div class="span2">
				<a href="<?php //echo $this->Html->url(array('controller' => 'denuncias', 'action' => 'add')); ?>" class="span12 tile <?php //echo ($atenciontipo_id == 'Denuncia') ? 'selected ': ''; ?>bg-darkRed fg-white">
				<div class="tile-content icon">
					<i class="icon-phone"></i>
				</div>
				<div class="brand">
					<span class="label fg-white"><?php //echo __('Nueva Denuncia'); ?></span>
					<span class="badge bg-white fg-darkRed"><i class="icon-plus"></i></span>
				</div>
				</a>
			</div> -->
		</div>
	</div>
</div>