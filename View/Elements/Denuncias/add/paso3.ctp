<?php echo $this->Html->link(__('<i class="icon-arrow-left-5
"></i> Volver al Paso 2'), array('action' => 'continuar', $id, 2), $options = array('class' => 'button', 'escape' => false)); ?><p>&nbsp;</p>
<?php echo $this->Form->create('Denunciado', array('inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
    <fieldset>
        <legend><?php echo __('Datos del/la Denunciado(a)'); ?></legend>
        <p><?php echo __('Hacer uso del siguiente formulario para agregar nuevos denunciados, si desea consultar los denunciados actuales, debe desplegar el "Listado de Denunciados"') ?></p>
        <div class="row">
            <div class="span2">
                <?php echo $this->Form->input("id", array('class' => 'denunciado_id')); ?>
                <?php echo $this->Form->label("cedula"); ?>
                <?php echo $this->Form->input("cedula", array('class' => 'denunciado_cedula', 'data-url' => $this->Html->url(array('controller' => 'denunciados', 'action' => 'getByCedulaJSON')))); ?>
            </div>
            <div class="span4 required">
                <?php echo $this->Form->label("nombres"); ?>
                <?php echo $this->Form->input("nombres", array('class' => 'denunciado_nombres')); ?>
            </div>
            <div class="span4 required">
                <?php echo $this->Form->label("apellidos"); ?>
                <?php echo $this->Form->input("apellidos", array('class' => 'denunciado_apellidos')); ?>
            </div>
        </div>
        <div class="row">
            <div class="span2">
                <?php echo $this->Form->label('Denunciado.Paise.id', __('Paise')); ?>
                <?php echo $this->Form->input('Denunciado.Paise.id', array('type' => 'select', 'class' => 'pais-select denunciado_paise_id', 'empty' => true, 'options' => $paises, 'data-child' => '#DenunciadoEntidadeId', 'data-url' => 'entidades/getAllByPaisIdJson/', 'div' => array('class' => 'input-control select'))); ?>
            </div>
            <div class="span3">
                <?php echo $this->Form->label('Denunciado.Entidade.id', __('Entidade')); ?>
                <?php echo $this->Form->input('Denunciado.Entidade.id', array('type' => 'select', 'alt' => (isset($this->request->data['Denunciado']['Entidade']['id'])) ? $this->request->data['Denunciado']['Entidade']['id'] : '', 'class' => 'entidade-select denunciado_entidade_id', 'empty' => true, 'options' => array(), 'data-child' => '#DenunciadoMunicipioId', 'data-url' => 'municipios/getAllByEntidadIdJson/', 'div' => array('class' => 'input-control select'))); ?>
            </div>
            <div class="span3">
                <?php echo $this->Form->label('Denunciado.Municipio.id', __('Municipio')); ?>
                <?php echo $this->Form->input('Denunciado.Municipio.id', array('type' => 'select', 'alt' => (isset($this->request->data['Denunciado']['Municipio']['id'])) ? $this->request->data['Denunciado']['Municipio']['id'] : '', 'class' => 'municipio-select denunciado_municipio_id', 'empty' => true, 'options' => array(), 'data-child' => '#DenunciadoParroquiaId', 'data-url' => 'parroquias/getAllByMunicipioIdJson/', 'div' => array('class' => 'input-control select'))); ?>
            </div>
            <div class="span4">
                <?php echo $this->Form->label('parroquia_id', __('Parroquia')); ?>
                <?php echo $this->Form->input('parroquia_id', array('type' => 'select', 'alt' => (isset($this->request->data['Denunciado']['Parroquia']['id'])) ? $this->request->data['Denunciado']['Parroquia']['id'] : '', 'class' => 'parroquia-select denunciado_parroquia_id', 'empty' => true, 'options' => array(), 'div' => array('class' => 'input-control select'))); ?>
            </div>
        </div>
        <div class="row">
            <div class="span12">
                <div class="row">
                    <div class="span12">
                        <?php echo $this->Form->label('direccion_habitacion'); ?>
                        <?php echo $this->Form->input('direccion_habitacion', array('type' => 'textarea', 'class' => 'denunciado_direccion_habitacion', 'rows' => 2, 'div' => array('class' => 'input-control textarea'))); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="span12">
                <div class="row">
                    <div class="span3">
                        <?php echo $this->Form->label('telefono_celular'); ?>
                        <?php echo $this->Form->input('telefono_celular', array('type' => 'tel', 'class' => 'denunciado_telefono_celular')); ?>
                    </div>
                    <div class="span3">
                        <?php echo $this->Form->label('telefono_habitacion'); ?>
                        <?php echo $this->Form->input('telefono_habitacion', array('type' => 'tel', 'class' => 'denunciado_telefono_habitacion')); ?>
                    </div>
                    <div class="span3">
                        <?php echo $this->Form->label('telefono_oficina'); ?>
                        <?php echo $this->Form->input('telefono_oficina', array('type' => 'tel', 'class' => 'denunciado_telefono_oficina')); ?>
                    </div>
                    <div class="span2">
                        <?php echo $this->Form->label('agregar', __('&nbsp;')); ?>
                        <?php echo $this->Form->input(__('Agregar Denunciado'), array('type' => 'button', 'class' => 'button success')); ?>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="form-actions">
        <?php echo $this->Html->link(__('<i class="icon-arrow-left-5"></i> Volver al Paso 2'), array('action' => 'continuar', $id, 2), $options = array('class' => 'button', 'escape' => false)); ?>
        <?php echo $this->Html->link(__('Finalizar <i class="icon-checkmark"></i>'), array('action' => 'finalizar', $id), array('class' => 'button success', 'escape' => false)); ?>
    </div>
<?php echo $this->Form->end(); ?>    
<?php if ($denunciados): ?>
    <?php $i = sizeof($denunciados); ?>
    <?php echo $this->Form->create('Denunciado', array('url' => array('controller' => 'denunciados', 'action' => 'updateMany'), 'inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
        <fieldset>
            <legend><?php echo __('Listado de Denunciados'); ?></legend>
            <div class="accordion" data-role="accordion">
                <div class="accordion-frame">
                    <a href="#" class="heading">
                        <?php if ($i > 1): ?>
                            <?php echo __("Ver Denunciados (%s)", $i); ?>
                        <?php else: ?>
                            <?php echo __("Ver Denunciado"); ?>
                    <?php endif; ?>
                        <i class="icon-toggle-accordion icon-arrow-down-4 place-right"></i>
                    </a>
                    <div class="content">
                        <?php $i = 0; ?>
                        <?php foreach ($denunciados as $denunciado): ?>
                            <div class="row bg-grayLighter padding10">
                                <h3><?php echo __("Denunciado #%s", $i + 1); ?></h3>
                            </div>
                            <div class="row">
                                <div class="span2">
                                    <?php echo $this->Form->input($i . '.id'); ?>
                                    <?php echo $this->Form->label($i . '.cedula'); ?>
                                    <?php echo $this->Form->input($i . '.cedula'); ?>
                                </div>
                                <div class="span4 required">
                                    <?php echo $this->Form->label($i . '.nombres'); ?>
                                    <?php echo $this->Form->input($i . '.nombres'); ?>
                                </div>
                                <div class="span4 required">
                                    <?php echo $this->Form->label($i . '.apellidos'); ?>
                                    <?php echo $this->Form->input($i . '.apellidos'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="span2">
                                    <?php echo $this->Form->label($i . '.Paise.id', __('Paise')); ?>
                                    <?php echo $this->Form->input($i . '.Paise.id', array('type' => 'select', 'class' => 'pais-select', 'empty' => true, 'options' => $paises, 'data-child' => '#Denunciado' . $i . 'EntidadeId', 'data-url' => 'entidades/getAllByPaisIdJson/', 'div' => array('class' => 'input-control select'))); ?>
                                </div>
                                <div class="span3">
                                    <?php echo $this->Form->label($i . '.Entidade.id', __('Entidade')); ?>
                                    <?php echo $this->Form->input($i . '.Entidade.id', array('type' => 'select', 'class' => 'entidade-select', 'alt' => (isset($denunciado['Entidade']['id'])) ? $denunciado['Entidade']['id'] : '', 'empty' => true, 'options' => array(), 'data-child' => '#Denunciado' . $i . 'MunicipioId', 'data-url' => 'municipios/getAllByEntidadIdJson/', 'div' => array('class' => 'input-control select'))); ?>
                                </div>
                                <div class="span3">
                                    <?php echo $this->Form->label($i . '.Municipio.id', __('Municipio')); ?>
                                    <?php echo $this->Form->input($i . '.Municipio.id', array('type' => 'select', 'class' => 'municipio-select', 'alt' => (isset($denunciado['Municipio']['id'])) ? $denunciado['Municipio']['id'] : '', 'empty' => true, 'options' => array(), 'data-child' => '#Denunciado' . $i . 'ParroquiaId', 'data-url' => 'parroquias/getAllByMunicipioIdJson/', 'div' => array('class' => 'input-control select'))); ?>
                                </div>
                                <div class="span4">
                                    <?php echo $this->Form->label($i . '.parroquia_id', __('Parroquia')); ?>
                                    <?php echo $this->Form->input($i . '.parroquia_id', array('type' => 'select', 'class' => 'parroquia-select', 'alt' => (isset($denunciado['Parroquia']['id'])) ? $denunciado['Parroquia']['id'] : '', 'empty' => true, 'options' => array(), 'div' => array('class' => 'input-control select'))); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="span12">
                                    <div class="row">
                                        <div class="span12">
                                            <?php echo $this->Form->label($i . '.direccion_habitacion'); ?>
                                            <?php echo $this->Form->input($i . '.direccion_habitacion', array('type' => 'textarea', 'rows' => 2, 'div' => array('class' => 'input-control textarea'))); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="span12">
                                    <div class="row">
                                        <div class="span3">
                                            <?php echo $this->Form->label($i . '.telefono_celular'); ?>
                                            <?php echo $this->Form->input($i . '.telefono_celular', array('type' => 'tel')); ?>
                                        </div>
                                        <div class="span3">
                                            <?php echo $this->Form->label($i . '.telefono_habitacion'); ?>
                                            <?php echo $this->Form->input($i . '.telefono_habitacion', array('type' => 'tel')); ?>
                                        </div>
                                        <div class="span3">
                                            <?php echo $this->Form->label($i . '.telefono_oficina'); ?>
                                            <?php echo $this->Form->input($i . '.telefono_oficina', array('type' => 'tel')); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="span3">
                                            <?php echo $this->Form->label(null, __('Eliminar?')); ?>
                                            <?php echo $this->Html->link(__('Yes, Delete'), array('controller' => 'denunciados_denuncias', 'action' => 'delete', $denunciado['DenunciadosDenuncia']['id']), array('class' => 'button danger'), __('Are you sure you want to delete # %s?', $denunciado['DenunciadosDenuncia']['id'])); ?>
                                        </div>
                                        <div class="span2">
                                            <?php echo $this->Form->label(null, __('Realizo Cambios?')); ?>
                                            <button type="submit" class="button success"><?php echo __('Guardar Cambios'); ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </fieldset>
    <?php echo $this->Form->end(); ?>
<?php endif; ?>
