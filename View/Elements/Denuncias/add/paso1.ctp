<?php echo $this->Html->link(__('<i class="icon-arrow-left-5"></i> Volver al listado'), array('controller' => 'atenciones', 'action' => 'main'), array('class' => 'button', 'escape' => false)); ?><p>&nbsp;</p>
<?php echo $this->Form->create('Ciudadano', array('inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
    <fieldset>
        <legend><?php echo __('Datos del/la Denunciante'); ?></legend>
        <div class="row">
            <div class="span2">
                <label><?php echo __('Cedula'); ?></label>
                <?php echo $this->Form->input('id_ciudadano'); ?>
                <?php if (isset($ciudadano['Perfilciudadano']['id'])): ?>
                    <?php echo $this->Form->input('Perfilciudadano.id', array('type' => 'hidden')); ?>
                    <?php echo $this->Form->input('Perfilciudadano.ciudadano_id', array('type' => 'hidden')); ?>
                <?php endif; ?>
                <?php echo $this->Form->input('cedula', array('type' => 'hidden')); ?>
                <span><?php echo h($ciudadano['Ciudadano']['cedula']); ?></span>
            </div>
            <div class="span4 required">
                <?php echo $this->Form->label('nombres'); ?>
                <?php echo $this->Form->input('nombres'); ?>
            </div>
            <div class="span4 required">
                <?php echo $this->Form->label('apellidos'); ?>
                <?php echo $this->Form->input('apellidos'); ?>
            </div>
            <div class="span2 required">
                <?php echo $this->Form->label('Perfilciudadano.fecha_nacimiento'); ?>
                <?php echo $this->Form->input('Perfilciudadano.fecha_nacimiento', array('type' => 'text', 'autofocus' => true, 'after' => '<button type="button" class="btn-date"></button>', 'div' => array('class' => 'input-control text datepicker'))); ?>
            </div>
        </div>
        <div class="row">
            <div class="span2 required">
                <?php echo $this->Form->label('Perfilciudadano.sexo'); ?>
                <?php echo $this->Form->input('Perfilciudadano.sexo', array('type' => 'select', 'div' => array('class' => 'input-control select'), 'empty' => true, 'options' => array('0' => __('Femenino'), '1' => __('Masculino')))); ?>
            </div>
            <div class="span2 required">
                <?php echo $this->Form->label('Perfilciudadano.estadocivile_id'); ?>
                <?php echo $this->Form->input('Perfilciudadano.estadocivile_id', array('type' => 'select', 'div' => array('class' => 'input-control select'), 'empty' => true)); ?>
            </div>
            <div class="span8 required">
                <?php echo $this->Form->label('Perfilciudadano.nivelinstruccione_id'); ?>
                <?php echo $this->Form->input('Perfilciudadano.nivelinstruccione_id', array('type' => 'select', 'div' => array('class' => 'input-control select'), 'empty' => true)); ?>
            </div>
        </div>
        <div class="row">
            <div class="span2 required">
                <?php echo $this->Form->label('Paise.id', __('Paise')); ?>
                <?php echo $this->Form->input('Paise.id', array('type' => 'select', 'class' => 'pais-select', 'empty' => true, 'options' => $paises, 'data-child' => '#EntidadeId', 'data-url' => 'entidades/getAllByPaisIdJson/', 'div' => array('class' => 'input-control select'))); ?>
            </div>
            <div class="span3 required">
                <?php echo $this->Form->label('Entidade.id', __('Entidade')); ?>
                <?php echo $this->Form->input('Entidade.id', array('type' => 'select', 'class' => 'entidade-select', 'alt' => (isset($this->request->data['Entidade']['id'])) ? $this->request->data['Entidade']['id'] : '', 'empty' => true, 'options' => array(), 'data-child' => '#MunicipioId', 'data-url' => 'municipios/getAllByEntidadIdJson/', 'div' => array('class' => 'input-control select'))); ?>
            </div>
            <div class="span3 required">
                <?php echo $this->Form->label('Municipio.id', __('Municipio')); ?>
                <?php echo $this->Form->input('Municipio.id', array('type' => 'select', 'class' => 'municipio-select', 'alt' => (isset($this->request->data['Municipio']['id'])) ? $this->request->data['Municipio']['id'] : '', 'empty' => true, 'options' => array(), 'data-child' => '#PerfilciudadanoParroquiaId', 'data-url' => 'parroquias/getAllByMunicipioIdJson/', 'div' => array('class' => 'input-control select'))); ?>
            </div>
            <div class="span4 required">
                <?php echo $this->Form->label('Perfilciudadano.parroquia_id', __('Parroquia')); ?>
                <?php echo $this->Form->input('Perfilciudadano.parroquia_id', array('type' => 'select', 'class' => 'parroquia-select', 'alt' => (isset($this->request->data['Perfilciudadano']['parroquia_id'])) ? $this->request->data['Perfilciudadano']['parroquia_id'] : '', 'empty' => true, 'options' => array(), 'div' => array('class' => 'input-control select'))); ?>
            </div>
        </div>
        <div class="row">
            <div class="span12">
                <div class="row">
                    <div class="span12 required">
                        <?php echo $this->Form->label('direccion'); ?>
                        <?php echo $this->Form->input('direccion', array('type' => 'textarea', 'rows' => 2, 'div' => array('class' => 'input-control textarea'))); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="span3">
                <label><?php echo __('Tiene Telefonos/Email?'); ?>
                    <div class="input-control switch">
                        <label>
                            <?php echo $this->Form->input('Perfilciudadano.tiene_telefonos', array('type' => 'checkbox', 'label' => false, 'div' => false, 'data-transform' => "input-control", 'data-transform-type' => "switch", 'checked' => ($this->request->data['Telefono']['id']) ? 'checked' : false)); ?>
                        </label>
                    </div>
                </label>
            </div>
        </div>
        <div class="row telefonos">
            <div class="span12">
                <div class="row">
                    <div class="span3">
                        <?php echo $this->Form->label('telefono'); ?>
                        <?php echo $this->Form->input('telefono', array('type' => 'tel')); ?>
                    </div>
                    <div class="span3">
                        <?php if ($this->request->data['Telefono']['id']): ?>
                            <?php echo $this->Form->input('Telefono.id'); ?>
                        <?php endif; ?>
                        <?php echo $this->Form->label('Telefono.telefono_habitacion'); ?>
                        <?php echo $this->Form->input('Telefono.telefono_habitacion', array('type' => 'tel')); ?>
                    </div>
                    <div class="span2">
                        <?php echo $this->Form->label('Telefono.telefono_oficina'); ?>
                        <?php echo $this->Form->input('Telefono.telefono_oficina', array('type' => 'tel')); ?>
                    </div>
                    <div class="span4">
                        <?php echo $this->Form->label('email'); ?>
                        <?php echo $this->Form->input('email', array('type' => 'email')); ?>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="form-actions">
        <?php echo $this->Html->link(__('<i class="icon-arrow-left-5"></i> Volver al listado'), array('controller' => 'atenciones', 'action' => 'main'), $options = array('class' => 'button', 'escape' => false)); ?>
        <button type="submit" class="button success"><?php echo __('Continuar <i class="icon-arrow-right-5"></i>'); ?></button>
    </div>
<?php echo $this->Form->end(); ?>