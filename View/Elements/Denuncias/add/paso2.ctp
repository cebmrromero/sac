<?php echo $this->Html->link(__('<i class="icon-arrow-left-5"></i> Volver al listado'), array('controller' => 'atenciones', 'action' => 'main'), array('class' => 'button', 'escape' => false)); ?><p>&nbsp;</p>
<?php echo $this->Form->create('Denuncia', array('inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
    <fieldset>
        <legend><?php echo __('Ente u Organismo o Comunidad Denunciada'); ?></legend>
        <div class="row">
            <div class="span12 required">
                <label><?php echo __('Indicar el Ente u Organismo o Comunidad Denunciada'); ?></label>
                <div id="procedencia" class="button-set" data-target="#DenunciaOrganismoDenunciado">
                    <?php foreach($procedencias as $k => $v): ?>
                        <?php echo $this->Html->link($v, array('action' => 'continuar', $this->request->data['Denuncia']['id'], 2, $k), array('class' => ($k == $procedencia_selected) ? 'button active' : 'button')); ?>
                    <?php endforeach; ?>
                </div>
                <?php if ($this->request->data['Denuncia']['id']): ?>
                    <?php echo $this->Form->input('id'); ?>
                <?php endif; ?>
                <?php echo $this->Form->input('organismo_denunciado', array('type' => 'hidden', 'value' => $procedencia_selected)); ?>
            </div>
        </div>
        <?php if ($procedencia_selected == 'I'): ?>
            <div class="row">
                <div class="span12 required">
                    <?php //print_r($this->request->data); ?>
                    <?php if (isset($this->request->data['Denunciaorganismo']['id'])): ?>
                        <?php echo $this->Form->input('Denunciaorganismo.id'); ?>
                    <?php endif; ?>
                    <?php echo $this->Form->label('Denunciaorganismo.organismo_id', __('Institucion u Organismo')); ?>
                    <?php echo $this->Form->input('Denunciaorganismo.organismo_id', array('type' => 'hidden',  'empty' => true, 'data-url' => 'organismos/getByIdJson/', 'data-child' => '#PaiseId', 'div' => array('class' => 'input-control select'))); ?>
                    <?php echo $this->Form->input('Denunciaorganismo.organismo_ac', array('type' => 'text', 'data-url' => 'organismos/getByNombreJSONP', 'class' => 'autocomplete', 'data-minkeys' => '5', 'data-target-update' => '#DenunciaorganismoOrganismoId', 'value' => (isset($this->request->data['Denunciaorganismo']['Organismo']['nombre'])) ? $this->request->data['Denunciaorganismo']['Organismo']['nombre'] : '')); ?>
                    <?php echo $this->Form->error('Denunciaorganismo.organismo_id'); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if ($procedencia_selected == 'C'): ?>
            <div class="row">
                <div class="span12 required">
                    <?php if (isset($this->request->data['Denunciacomunidade']['id'])): ?>
                        <?php echo $this->Form->input('Denunciacomunidade.id'); ?>
                    <?php endif; ?>
                    <?php echo $this->Form->label('Denunciacomunidade.comunidade_id', __('Comunidad Organizada')); ?>
                    <?php echo $this->Form->input('Denunciacomunidade.comunidade_id', array('type' => 'hidden', 'empty' => true, 'data-url' => 'comunidades/getByIdJson/', 'data-child' => '#PaiseId', 'div' => array('class' => 'input-control select'))); ?>
                    <?php echo $this->Form->input('Denunciacomunidade.comunidade_ac', array('type' => 'text', 'data-url' => 'comunidades/getByNombreJSONP', 'class' => 'autocomplete', 'data-minkeys' => '5', 'data-target-update' => '#DenunciacomunidadeComunidadeId', 'value' => (isset($this->request->data['Denunciacomunidade']['Comunidade']['nombre'])) ? $this->request->data['Denunciacomunidade']['Comunidade']['nombre'] : '')); ?>
                    <?php echo $this->Form->error('Denunciacomunidade.comunidade_id'); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (isset($procedencia_selected) && !empty($procedencia_selected)): ?>
            <div class="row">
                <div class="span12 required">
                    <?php echo $this->Form->input('ciudadano_id', array('type' => 'hidden')); ?>
                    <?php echo $this->Form->label('descripcion_denuncia'); ?>
                    <?php echo $this->Form->input('descripcion_denuncia', array('type' => 'textarea', 'class' => 'ckeditor', 'div' => array('class' => 'input-control textarea'))); ?>
                </div>
            </div>
            
            <div class="row">
                <div class="span3">
                    <label><?php echo __('Anexa Documentos?'); ?>
                        <div class="input-control switch">
                            <label>
                                <?php echo $this->Form->input('anexa_documentos', array('type' => 'checkbox', 'label' => false, 'div' => false, 'data-transform' => "input-control", 'data-transform-type' => "switch", 'checked' => ($this->request->data['Denuncia']['anexa_documentos']) ? 'checked' : false)); ?>
                            </label>
                        </div>
                    </label>
                </div>
                <div class="span1 documentos">
                    <?php if (isset($this->request->data['Denunciadocumento']['id'])): ?>
                        <?php echo $this->Form->input('Denunciadocumento.id'); ?>
                        <?php echo $this->Form->input('Denunciadocumento.denuncia_id', array('type' => 'hidden')); ?>
                    <?php endif; ?>
                    <?php echo $this->Form->label('Denunciadocumento.originales', __('Originales')); ?>
                    <?php echo $this->Form->input('Denunciadocumento.originales', array('type' => 'numeric')); ?>
                </div>
                <div class="span2 documentos">
                    <?php echo $this->Form->label('Denunciadocumento.copias_cetificadas', __('Copias Certificadas')); ?>
                    <?php echo $this->Form->input('Denunciadocumento.copias_cetificadas', array('type' => 'numeric')); ?>
                </div>
                <div class="span1 documentos">
                    <?php echo $this->Form->label('Denunciadocumento.paginas', __('Folios')); ?>
                    <?php echo $this->Form->input('Denunciadocumento.paginas', array('type' => 'numeric')); ?>
                </div>
            </div>
        <?php endif; ?>
    </fieldset>
    <?php if (isset($procedencia_selected) && !empty($procedencia_selected)): ?>
        <div class="form-actions">
            <button type="submit" class="button success"><?php echo __('Continuar <i class="icon-arrow-right-5
"></i>'); ?></button>
        </div>
    <?php endif; ?>
<?php echo $this->Form->end(); ?>