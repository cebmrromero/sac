<?php $ciudadano = $this->Session->read('ciudadano'); ?>
<?php if ($ciudadano): ?>
	<?php echo $this->element('Atenciones/top-menu', array('atenciontipo_id' => null)); ?>
<?php endif; ?>
<?php $this->Html->addCrumb(__('Atenciontipos'), '/atenciones/index'); ?>
	<div class="container atenciontipos index">
		<div class="grid fluid">
			<div class="row">
				<div class="span12 bg-white padding20">
				<h2><?php echo __('Atenciontipos'); ?></h2>
				<?php if ($atenciontipos): ?>
					<table cellpadding="0" cellspacing="0" class="table hovered sortable-table">
						<thead>
							<tr>
							<th><?php echo $this->Paginator->sort('denominacion'); ?></th>
							<th class="actions span3"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
						<tbody>
						<?php foreach ($atenciontipos as $atenciontipo): ?>
							<tr>
								<td>
									<span class="padding5 fg-white <?php echo h($atenciontipo['Atenciontipo']['bgcolor']); ?>">
									<i class="<?php echo h($atenciontipo['Atenciontipo']['btnicon']); ?>"></i> <?php echo h($atenciontipo['Atenciontipo']['denominacion']); ?>
									</span>&nbsp;
								</td>
								<td class="actions">
									<?php echo $this->Html->link('Ver<i class="icon-eye bg-green"></i>', array('action' => 'view', $atenciontipo['Atenciontipo']['id']), array('class' => 'image-button bg-darkGreen fg-white', 'title' => __('View'), 'escape' => false)); ?>
									<?php echo $this->Html->link('Editar<i class="icon-pencil bg-darkCyan"></i>', array('action' => 'edit', $atenciontipo['Atenciontipo']['id']), array('class' => 'image-button bg-darkCobalt fg-white', 'title' => __('View'), 'escape' => false)); ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<?php else: ?>
					<p class="text-info"><?php echo __('No hay atenciontipos registrados recientemente'); ?></p>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>