<?php $ciudadano = $this->Session->read('ciudadano'); ?>
<?php if ($ciudadano): ?>
<?php echo $this->element('Atenciones/top-menu', array('atenciontipo_id' => null)); ?>
<?php endif; ?>
<?php $this->Html->addCrumb(__('Atenciontipos'), '/atenciontipos/index'); ?>
<?php $this->Html->addCrumb(__('Ver'), '/atenciontipos/view/' . $atenciontipo['Atenciontipo']['id']); ?>
<div class="container atenciontipos view">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 bg-white padding20">
				<div class="span10 bg-white">
					<h2><?php echo __('Atenciontipo'); ?>: <span class="fg-white <?php echo h($atenciontipo['Atenciontipo']['bgcolor']); ?> padding5"><?php echo h($atenciontipo['Atenciontipo']['denominacion']); ?></span></h2>
				</div>
				<div class="span2 actions bg-white text-right">
					<div class="tile half bg-orange">
						<?php echo $this->Html->link(
							'<div class="tile-content icon"><i class="icon-list"></i></div>',
							array('action' => 'index'),
							array(
								'class' => 'fg-white',
								'title' => __('List'),
								'escape' => false
							)
						); ?>
					</div>
					<div class="tile half bg-green">
						<?php echo $this->Html->link(
							'<div class="tile-content icon"><i class="icon-plus"></i></div>',
							array('action' => 'add'),
							array(
								'class' => 'fg-white',
								'title' => __('Add'),
								'escape' => false
							)
						); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="span12 bg-white padding20">
				<h2><?php echo __('Related Atenciones'); ?></h2>
				<?php if (!empty($atenciontipo['Atencione'])): ?>
					<table cellpadding="0" cellspacing="0" class="table hovered">
						<thead>
							<tr>
								<th><?php echo __('Nro Atencion'); ?></th>
								<th><?php echo __('Created'); ?></th>
								<th><?php echo __('Ciudadano'); ?></th>
								<!-- <th><?php //echo __('Leye'); ?></th> -->
								<th><?php echo __('Atencioncategoria'); ?></th>
								<th><?php echo __('Atendido por'); ?></th>
								<th class="actions"><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($atenciontipo['Atencione'] as $atencione): ?>
								<tr>
									<?php if ($atencione['completada']): ?>
										<td><span class="padding5 fg-white <?php echo $atencione['Atenciontipo']['bgcolor']; ?>">#<?php echo $atencione['nro_atencion']; ?></span></td>
									<?php else :?>
										<td><span class="padding5 fg-white <?php echo $atencione['Atenciontipo']['bgcolor']; ?>">Sin completar</span></td>
									<?php endif; ?>
								<td><?php echo h($this->Time->format('d-m-Y g:ia', $atencione['created'])); ?>&nbsp;</td>
								<td><?php echo $atencione['Ciudadano']['cedula']; ?> - <?php echo $atencione['Ciudadano']['nombres']; ?> <?php echo $atencione['Ciudadano']['apellidos']; ?></td>
								<!-- <td><?php //echo (isset($atencione['Leye']['nombre'])) ? $atencione['Leye']['nombre'] : ''; ?></td> -->
								<td><?php echo (isset($atencione['Atencioncategoria']['nombre'])) ? $atencione['Atencioncategoria']['nombre'] : ''; ?></td>
								<td><?php echo $atencione['User']['first_name']; ?> <?php echo $atencione['User']['last_name']; ?></td>
								<td class="actions">
									<?php if ($atencione['completada']): ?>
										<?php echo $this->Html->link('Ver<i class="icon-eye bg-green"></i>', array('controller' => 'atenciones', 'action' => 'view', $atencione['id']), array('class' => 'image-button bg-darkGreen fg-white', 'title' => __('View'), 'escape' => false)); ?>
									<?php else: ?>
										<?php echo $this->Html->link('Continuar<i class="icon-eye bg-green"></i>', array('controller' => 'atenciones', 'action' => 'continuar', $atencione['id'], $atencione['paso']), array('class' => 'image-button bg-darkGreen fg-white', 'title' => __('Continuar'), 'escape' => false)); ?>
									<?php endif; ?>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>