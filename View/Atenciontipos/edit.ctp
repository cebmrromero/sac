<?php if ($ciudadano): ?>
    <?php echo $this->element('Atenciones/top-menu', array('atenciontipo_id' => null)); ?>
<?php endif; ?>
<?php $this->Html->addCrumb(__('Atenciontipos'), '/atenciontipos/index'); ?>
<?php $this->Html->addCrumb(__('Modificar'), "/atenciontipos/edit/{$this->request->data['Atenciontipo']['id']}"); ?>
<div class="container atenciontipos form">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 bg-white padding20">
				<div class="span10 bg-white">
					<h2><?php echo __('Modificar Atenciotipo: %s', $this->request->data['Atenciontipo']['denominacion']); ?></h2>
				</div>
				<div class="span2 actions bg-white text-right">
					<div class="tile half bg-orange">
						<?php echo $this->Html->link(
							'<div class="tile-content icon"><i class="icon-list"></i></div>',
							array('action' => 'index'),
							array(
								'class' => 'fg-white',
								'title' => __('List'),
								'escape' => false
							)
						); ?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="span12 bg-white padding20">
				<fieldset>
					<div class="row">
						<div class="span12">
							<?php echo $this->Form->create('Atenciontipo', array('inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
							    <fieldset>
							        <legend><?php echo __('Datos del Atenciontipo'); ?></legend>
							        <div class="row">
							            <div class="span4 required">
							                <?php echo $this->Form->input('id'); ?>
							                <?php echo $this->Form->label('denominacion'); ?>
							                <?php echo $this->Form->input('denominacion', array('readonly' => 'readonly')); ?>
							            </div>
							            <div class="span4 required">
							                <?php echo $this->Form->label('bgcolor'); ?>
							                <?php echo $this->Form->input('bgcolor'); ?>
							            </div>
							            <div class="span4 required">
							                <?php echo $this->Form->label('btnicon'); ?>
							                <?php echo $this->Form->input('btnicon'); ?>
							            </div>
							        </div>
							    </fieldset>
							    <div class="form-actions">
							        <button type="submit" class="button success"><?php echo __('Guardar'); ?></button>
							    </div>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</fieldset>
			</div>
        </div>
    </div>
</div>