<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<title>Atención de la Denuncia</title>
	</head>
	<body>
		<div class="container">
			<header>
				<h1><?php echo __('Atención de la Denuncia'); ?></h1>
			</header>
			<section class="body">
				<div class="left">
					<table>
						<tbody>
							<tr>
								<td>
									<?php echo $this->Html->tag('label', __('1. Fecha')); ?>
								</td>
								<td>
									<?php echo $this->Html->tag('label', __('2. Hora')); ?>
								</td>
							</tr>
							<tr>
								<td class="text-center">
									<?php echo $this->Html->tag('span', $this->Time->format('d-m-Y', $denuncia['Denuncia']['created'])); ?>
								</td>
								<td class="text-center">
									<?php echo $this->Html->tag('span', $this->Time->format('g:i a', $denuncia['Denuncia']['created'])); ?>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="right">
					<table>
						<tbody>
							<tr>
								<td>
									<?php echo $this->Html->tag('label', __('3. Número de Expediente')); ?>
								</td>
							</tr>
							<tr>
								<td class="text-center">
									<?php echo $this->Html->tag('span', $denuncia['Denuncia']['nro_expediente']); ?>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</section>
			<div class="clear"></div>
			<section class="body">
					<table>
						<tbody>
							<tr>
								<td colspan="6">
									<?php echo $this->Html->tag('label', __('4. Datos del Denunciante')); ?>
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<?php echo $this->Html->tag('strong', 'Apellidos y nombres:'); ?>
									<?php echo $this->Html->tag('span', $denuncia['Ciudadano']['apellidos'] . ' ' . $denuncia['Ciudadano']['nombres']); ?>
								</td>
								<td colspan="2">
									<?php echo $this->Html->tag('strong', __('Cedula') . ':'); ?>
									<?php echo $this->Html->tag('span', $denuncia['Ciudadano']['cedula']); ?>
								</td>
							</tr>
							<tr>
								<td colspan="6">
									<?php echo $this->Html->tag('strong', 'Direccion:'); ?>
									<?php echo $this->Html->tag('span', $denuncia['Ciudadano']['direccion']); ?>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<?php echo $this->Html->tag('strong', __('Entidade') . ':'); ?>
									<?php echo $this->Html->tag('span', $denuncia['Ciudadano']['Perfilciudadano']['Entidade']['nombre']); ?>
								</td>
								<td colspan="2">
									<?php echo $this->Html->tag('strong', __('Municipio') . ':'); ?>
									<?php echo $this->Html->tag('span', $denuncia['Ciudadano']['Perfilciudadano']['Municipio']['nombre']); ?>
								</td>
								<td colspan="2">
									<?php echo $this->Html->tag('strong', __('Parroquia') . ':'); ?>
									<?php echo $this->Html->tag('span', $denuncia['Ciudadano']['Perfilciudadano']['Parroquia']['nombre']); ?>
								</td>
							</tr>
							<tr class="text-center">
								<td colspan="6">
									<?php echo $this->Html->tag('span', __('Números Telefónicos')); ?>
								</td>
							</tr>
							<tr class="text-center">
								<td colspan="2">
									<?php echo $this->Html->tag('strong', __('Telefono')); ?>
								</td>
								<td colspan="2">
									<?php echo $this->Html->tag('strong', __('Telefono Oficina')); ?>
								</td>
								<td colspan="2">
									<?php echo $this->Html->tag('strong', __('Telefono Habitacion')); ?>
								</td>
							</tr>
							<tr class="text-center">
								<td colspan="2">
									<?php echo $this->Html->tag('span', $denuncia['Ciudadano']['telefono']); ?>
								</td>
								<td colspan="2">
									<?php echo $this->Html->tag('span', $denuncia['Ciudadano']['Perfilciudadano']['telefono_oficina']); ?>
								</td>
								<td colspan="2">
									<?php echo $this->Html->tag('span', $denuncia['Ciudadano']['Perfilciudadano']['telefono_habitacion']); ?>
								</td>
							</tr>
							<?php $i = sizeof($denuncia['Denunciado']); ?>
							<?php $show_index = $i > 1; ?>
							<?php $j = 0; ?>
							<?php foreach ($denuncia['Denunciado'] as $denunciado): ?>
								<tr>
									<td colspan="6">
										<?php $titulo = __('5. Datos del Denunciado'); ?>
										<?php $titulo .= ($show_index) ? '<div class="index">' . ++$j . '</div>' : ''; ?>
										<?php echo $this->Html->tag('label', $titulo); ?>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<?php echo $this->Html->tag('strong', 'Apellidos y nombres:'); ?>
										<?php echo $this->Html->tag('span', $denunciado['apellidos'] . ' ' . $denunciado['nombres']); ?>
									</td>
									<td colspan="2">
										<?php echo $this->Html->tag('strong', __('Cedula') . ':'); ?>
										<?php echo $this->Html->tag('span', $denunciado['cedula']); ?>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<?php echo $this->Html->tag('strong', 'Direccion:'); ?>
										<?php echo $this->Html->tag('span', $denunciado['direccion_habitacion']); ?>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<?php echo $this->Html->tag('strong', __('Entidade') . ':'); ?>
										<?php echo $this->Html->tag('span', (isset($denunciado['Entidade']['nombre'])) ? $denunciado['Entidade']['nombre'] : ''); ?>
									</td>
									<td colspan="2">
										<?php echo $this->Html->tag('strong', __('Municipio') . ':'); ?>
										<?php echo $this->Html->tag('span', (isset($denunciado['Municipio']['nombre'])) ? $denunciado['Municipio']['nombre'] : ''); ?>
									</td>
									<td colspan="2">
										<?php echo $this->Html->tag('strong', __('Parroquia') . ':'); ?>
										<?php echo $this->Html->tag('span', (isset($denunciado['Parroquia']['nombre'])) ? $denunciado['Parroquia']['nombre'] : ''); ?>
									</td>
								</tr>
								<tr class="text-center">
									<td colspan="6">
										<?php echo $this->Html->tag('span', __('Números Telefónicos')); ?>
									</td>
								</tr>
								<tr class="text-center">
									<td colspan="2">
										<?php echo $this->Html->tag('strong', __('Telefono')); ?>
									</td>
									<td colspan="2">
										<?php echo $this->Html->tag('strong', __('Telefono Oficina')); ?>
									</td>
									<td colspan="2">
										<?php echo $this->Html->tag('strong', __('Telefono Habitacion')); ?>
									</td>
								</tr>
								<tr class="text-center">
									<td colspan="2">
										<?php echo $this->Html->tag('span', $denunciado['telefono_celular']); ?>
									</td>
									<td colspan="2">
										<?php echo $this->Html->tag('span', $denunciado['telefono_oficina']); ?>
									</td>
									<td colspan="2">
										<?php echo $this->Html->tag('span', $denunciado['telefono_habitacion']); ?>
									</td>
								</tr>
							<?php endforeach; ?>
							<?php if ($denuncia['Denuncia']['organismo_denunciado'] == 'I'): ?>
								<tr>
									<td colspan="6">
										<?php echo $this->Html->tag('label', __('6. Entre u Organismo Denunciado')); ?>
									</td>
								</tr>
								<tr class="text-center">
									<td colspan="6">
										<?php echo $this->Html->tag('span', $denuncia['Denunciaorganismo']['Organismo']['nombre']); ?>
									</td>
								</tr>
							<?php endif; ?>
							<?php if ($denuncia['Denuncia']['organismo_denunciado'] == 'C'): ?>
								<tr>
									<td colspan="6">
										<?php echo $this->Html->tag('label', __('6. Comunidad Organizada')); ?>
									</td>
								</tr>
								<tr class="text-center">
									<td colspan="6">
										<?php echo $this->Html->tag('span', $denuncia['Denunciacomunidade']['Comunidade']['nombre']); ?>
									</td>
								</tr>
							<?php endif; ?>
							<tr>
								<td colspan="6">
									<?php echo $this->Html->tag('label', __('7. Breve descripción de la denuncia')); ?>
								</td>
							</tr>
							<tr class="text-justify descripcion">
								<td colspan="6">
									<?php echo $this->Html->tag('span', $denuncia['Denuncia']['descripcion_denuncia']); ?>
								</td>
							</tr>
							<?php $originales = '--'; ?>
							<?php $copias = '--'; ?>
							<?php $paginas = 0; ?>
							<?php if ($denuncia['Denuncia']['anexa_documentos']): ?>
								<?php $originales = ($denuncia['Denunciadocumento']['originales']) ? $denuncia['Denunciadocumento']['originales'] : 0; ?>
								<?php $copias = ($denuncia['Denunciadocumento']['copias_cetificadas']) ? $denuncia['Denunciadocumento']['copias_cetificadas'] : 0; ?>
								<?php $paginas = ($denuncia['Denunciadocumento']['paginas']) ? $denuncia['Denunciadocumento']['paginas'] : 0; ?>
							<?php endif; ?>
							<tr>
								<td colspan="2">
									<?php echo $this->Html->tag('span', __('Anexa Documentos?') . ':'); ?>
									<?php echo $this->Html->tag('span', ($denuncia['Denuncia']['anexa_documentos']) ? __('Si') : __('No')); ?>
								</td>
								<td colspan="3">
									<?php echo $this->Html->tag('span', __('Originales') . ': <span class="numbers">' . $originales . '</span>&nbsp;&nbsp;' . __('Copias') . ': <span class="numbers">' . $copias . '</span>' .  '&nbsp;'); ?>

								</td>
								<td>
									<?php echo $this->Html->tag('span', __('Página(s)') . ': <span class="numbers">' . $paginas . '</span>' .  '&nbsp;'); ?>
								</td>
							</tr>
							<tr>
								<td colspan="6">
									<?php echo $this->Html->tag('label', __('8. Firma del Denunciante')); ?>
								</td>
							</tr>
							<tr>
								<td colspan="6">
									<br /><br />
								</td>
							</tr>
							<tr>
								<td colspan="6">
									<?php echo $this->Html->tag('label', __('9. Por la oficina de atención al ciudadano')); ?>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<?php echo $this->Html->tag('strong', __('Receptor') . ':'); ?>
									<?php echo $this->Html->tag('span', $denuncia['User']['first_name'] . ' ' . $denuncia['User']['last_name']); ?>
								</td>
								<td colspan="2">
									<?php echo $this->Html->tag('strong', __('Cargo') . ':'); ?>
									<?php echo $this->Html->tag('span', $denuncia['User']['cargo']); ?>
								</td>
								<td>
									<?php echo $this->Html->tag('strong', __('Fecha') . ':'); ?>
									<?php echo $this->Html->tag('span', date('d-m-Y g:ia')); ?>
								</td>
								<td>
									<?php echo $this->Html->tag('strong', __('Firma') . ':'); ?>
								</td>
							</tr>
							<tr class="text-justify">
								<td colspan="6">
									<?php echo $this->Html->tag('small', '<strong>Importante:</strong> si la denuncia resultare falsa e infundada, o versare sobre hechos que no ameriten averiguación o cuya sustanciación no corresponda a esta Contraloría, se procederá a dejar constancia mediante auto expreso. <strong>No se admiten denuncias anónimas</strong>'); ?>
								</td>
							</tr>
						</tbody>
					</table>
			</section>
			<div class="clear"></div>
			<section class="hide-on-print">
				<p class="text-center">
					<?php echo $this->Html->link('Cerrar Ventana', '#', array('onclick' => 'return window.close()')); ?>
					|
					<?php echo $this->Html->link('Imprimir', '#', array('onclick' => 'return window.print()')); ?>
					<br /><br /><br />
				</p>
			</section>
		</div>
		<?php echo $this->Html->css('metro-bootstrap'); ?>
		<?php echo $this->Html->css('planilla_denuncia'); ?>
		<?php echo $this->Html->css('hide_on_print', 'stylesheet', array('media' => 'print')); ?>
		<script type="text/javascript">window.print()</script>
	</body>
</html>