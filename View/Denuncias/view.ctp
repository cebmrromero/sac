<?php if ($ciudadano): ?>
    <?php echo $this->element('Atenciones/top-menu', array('atenciontipo_id' => null)); ?>
<?php endif; ?>
<?php $this->Html->addCrumb(__('Atenciones'), '/atenciones/main'); ?>
<?php $this->Html->addCrumb(__('Consultar'), '/denuncias/view/' . $denuncia['Denuncia']['id']); ?>
<div class="container denuncias view">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 bg-white padding20">
				<div class="span8 bg-white">
					<h2><?php echo __('Consulta de Denuncia '); ?><strong class="fg-white bg-darkRed">&nbsp;<?php echo h($denuncia['Denuncia']['nro_expediente']); ?>&nbsp;</strong></h2>
				</div>
				<div class="span4 actions bg-white text-right">
					<div class="tile half bg-red">
						<?php echo $this->Form->postLink(
							'<div class="tile-content icon"><i class="icon-remove"></i></div>',
							array('action' => 'delete', $denuncia['Denuncia']['id']),
							array(
								'class' => 'fg-white',
								'title' => __('Delete'),
								'escape' => false
							),
							__('Are you sure you want to delete # %s?', $denuncia['Denuncia']['id'])
						); ?>
					</div>
					<div class="tile half bg-orange">
						<?php echo $this->Html->link(
							'<div class="tile-content icon"><i class="icon-list"></i></div>',
							array('controller' => 'atenciones', 'action' => 'main'),
							array(
								'class' => 'fg-white',
								'title' => __('List'),
								'escape' => false
							)
						); ?>
					</div>
					<div class="tile half bg-blue">
						<?php echo $this->Html->link(
							'<div class="tile-content icon"><i class="icon-pencil"></i></div>',
							array('action' => 'continuar', $denuncia['Denuncia']['id'], $denuncia['Denuncia']['paso']),
							array(
								'class' => 'fg-white',
								'title' => __('Edit'),
								'escape' => false
							)
						); ?>
					</div>
					<div class="tile half bg-green">
						<?php echo $this->Html->link(
							'<div class="tile-content icon"><i class="icon-plus"></i></div>',
							array('action' => 'add'),
							array(
								'class' => 'fg-white ',
								'title' => __('Add'),
								'escape' => false
							)
						); ?>
					</div>
					<div class="tile half bg-yellow">
						<?php echo $this->Html->link(
							'<div class="tile-content icon"><i class="icon-printer"></i></div>',
							array('action' => 'planilla', $denuncia['Denuncia']['id']),
							array(
								'class' => 'fg-white ',
								'title' => __('Print'),
								'target' => '_blank',
								'escape' => false
							)
						); ?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="span6 bg-white padding20">
				<fieldset>
					<div class="row">
						<div class="span12">
							<?php echo $this->Form->create('Denunciaconsulta', array(
								'url' => array('controller' => 'denunciaconsultas', 'action' => 'add', $denuncia['Denuncia']['id']),
								'inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false)
							)); ?>
							<fieldset>
								<legend><?php echo __("Registro de la Consulta"); ?></legend>
								<?php
								echo $this->Form->label('descripcion', __('Motivo de la Consulta'));
								echo $this->Form->input('descripcion', array('type' => 'textarea', 'rows' => 5, 'autofocus' => true, 'placeholder' => ($ciudadano) ? '' : __('Para registrar una consulta debe seleccionar un ciudadano'), 'disabled' => ($ciudadano) ? false : true ));
								?>
							</fieldset>
							&nbsp;
							<div class="form-actions">
								<?php if ($ciudadano): ?>
									<button type="submit" class="button success"><?php echo __('Guardar la Consulta'); ?></button>
								<?php else: ?>
									<button type="button" class="button disabled" disabled="disabled"><?php echo __('Guardar la Consulta'); ?></button>
								<?php endif; ?>
							</div>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
					<div class="row">
						<div class="span12">
							<fieldset>
								<legend><?php echo __("Consultas a la Denuncia"); ?></legend>
								<?php if ($denuncia['Denunciaconsulta']): ?>
									<div class="listview-outlook" data-role="listview">
										<?php foreach ($denuncia['Denunciaconsulta'] as $denunciaconsulta): ?>
											<a class="list show-consulta-details" href="#">
											    <div class="list-content">
												<span class="list-title"><?php //echo $denunciaconsulta['Ciudadano']['cedula']; ?><?php echo $denunciaconsulta['Ciudadano']['nombres']; ?> <?php echo $denunciaconsulta['Ciudadano']['apellidos']; ?> el <?php echo $this->Time->format('d-m-Y g:ia', $denunciaconsulta['created']); ?></span>
												<span class="list-subtitle" data-url-edit="<?php echo $this->Html->url(array('controller' => 'denunciaconsultas', 'action' => 'edit', $denunciaconsulta['id'])); ?>" data-url-delete="<?php echo $this->Html->url(array('controller' => 'denunciaconsultas', 'action' => 'delete', $denunciaconsulta['id'])); ?>" alt="<?php echo $denunciaconsulta['descripcion']; ?>"><?php echo $this->Text->truncate($denunciaconsulta['descripcion'], 95); ?></span>
												<span class="list-remark fg-green"><span class="place-left icon-eye"></span> Click para ver detalles</span>
											    </div>
											</a>
										<?php endforeach; ?>
									</div>
								<?php else: ?>
									<p class="text-info"><?php echo __('No posee consultas.'); ?></p>
								<?php endif; ?>
							</fieldset>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="span6 bg-white padding20">
				<fieldset>
					<legend><?php echo __("Datos de la Denuncia"); ?></legend>
					<div class="row">
						<div class="span12">
							<dl>
								<dt><?php echo __('Ciudadano atendido'); ?></dt>
								<dd>
									<?php echo h($denuncia['Ciudadano']['cedula']); ?> - <?php echo h($denuncia['Ciudadano']['nombres']); ?> <?php echo h($denuncia['Ciudadano']['apellidos']); ?>
									&nbsp;
								</dd>
							</dl>
						</div>
					</div>
					<div class="row">
						<div class="span12">
							<dl>
								<dt><?php echo __('Fecha Registro'); ?></dt>
								<dd>
									<?php echo h($this->Time->format('d-m-Y g:ia', $denuncia['Denuncia']['created'])); ?>
									&nbsp;
								</dd>
							</dl>
						</div>
					</div>
					<div class="row">
						<div class="span12">
							<?php if ($denuncia['Denuncia']['organismo_denunciado'] == 'I'): ?>
								<dl>
									<dt><?php echo __('Institucion u Organismo'); ?></dt>
									<dd>
										<?php echo h($denuncia['Denunciaorganismo']['Organismo']['nombre']); ?>
										&nbsp;
									</dd>
								</dl>
							<?php endif; ?>
							<?php if ($denuncia['Denuncia']['organismo_denunciado'] == 'C'): ?>
								<dl>
									<dt><?php echo __('Comunidad Organizada'); ?></dt>
									<dd>
										<?php echo h($denuncia['Denunciacomunidade']['Comunidade']['nombre']); ?>
										&nbsp;
									</dd>
								</dl>
							<?php endif; ?>
						</div>
					</div>
					<div class="row">
						<div class="span12">
							<dl>
								<dt><?php echo __('Descripcion Denuncia'); ?></dt>
								<dd>
									<?php echo html_entity_decode(h($denuncia['Denuncia']['descripcion_denuncia'])); ?>
								</dd>
							</dl>
						</div>
					</div>
					<div class="row">
						<div class="span12">
							<dl>
								<dt><?php echo __('Denunciados'); ?></dt>
								<dd>
									<?php if (!empty($denuncia['Denunciado'])): ?>
										<ul>
											<?php foreach ($denuncia['Denunciado'] as $denunciado): ?>
												<li><?php echo h($denunciado['cedula']); ?> - <?php echo h($denunciado['nombres']); ?> <?php echo h($denunciado['apellidos']); ?></li>
												&nbsp;
											<?php endforeach; ?>
										</ul>
									<?php endif; ?>
								</dd>
							</dl>
						</div>
					</div>
				</fieldset>
			</div>
        </div>
    </div>
</div>
<?php if ($print): ?>
	<script type="text/javascript">
		window.open('<?php echo $this->Html->url(array('action' => 'planilla', $denuncia['Denuncia']['id'])); ?>', '_blank');
	</script>
<?php endif; ?>