<?php $ciudadano = $this->Session->read('ciudadano'); ?>
<?php if ($ciudadano): ?>
	<?php echo $this->element('Atenciones/top-menu', array('atenciontipo_id' => null)); ?>
<?php endif; ?>
<?php $this->Html->addCrumb(__('Atencioncategorias'), '/atencioncategorias/index'); ?>
<div class="container atencioncategorias index">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 bg-white padding20">
				<div class="span10 bg-white">
					<h2><?php echo __('Atencioncategorias'); ?></h2>
				</div>
				<div class="span2 actions bg-white text-right">
					<div class="tile half bg-green">
						<?php echo $this->Html->link(
							'<div class="tile-content icon"><i class="icon-plus"></i></div>',
							array('action' => 'add'),
							array(
								'class' => 'fg-white',
								'title' => __('Add'),
								'escape' => false
							)
						); ?>
					</div>
				</div>
				<?php if ($atencioncategorias): ?>
					<table cellpadding="0" cellspacing="0" class="table hovered sortable-table">
						<thead>
							<tr>
								<th><?php echo $this->Paginator->sort('nombre'); ?></th>
								<th class="actions span4"><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($atencioncategorias as $atencioncategoria): ?>
								<tr>
									<td><?php echo h($atencioncategoria['Atencioncategoria']['nombre']); ?>&nbsp;</td>
									<td class="actions">
										<?php echo $this->Html->link('Ver<i class="icon-eye bg-green"></i>', array('action' => 'view', $atencioncategoria['Atencioncategoria']['id']), array('class' => 'image-button bg-darkGreen fg-white', 'title' => __('View'), 'escape' => false)); ?>
										<?php echo $this->Html->link('Editar<i class="icon-pencil bg-darkCyan"></i>', array('action' => 'edit', $atencioncategoria['Atencioncategoria']['id']), array('class' => 'image-button bg-darkCobalt fg-white', 'title' => __('View'), 'escape' => false)); ?>
										<?php echo $this->Form->postLink('Eliminar<i class="icon-remove bg-red"></i>', array('action' => 'delete', $atencioncategoria['Atencioncategoria']['id']), array('class' => 'image-button bg-crimson fg-white', 'title' => __('View'), 'escape' => false), __('Are you sure you want to delete # %s?', $atencioncategoria['Atencioncategoria']['id'])); ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				<?php else: ?>
					<p class="text-info"><?php echo __('No hay atencioncategorias registradas recientemente'); ?></p>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>