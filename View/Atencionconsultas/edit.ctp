<?php if ($ciudadano): ?>
    <?php echo $this->element('Atenciones/top-menu', array('atenciontipo_id' => null)); ?>
<?php endif; ?>
<?php $this->Html->addCrumb(__('Atenciones'), '/atenciones/main'); ?>
<?php $this->Html->addCrumb(__('Atencion #%s', $atencione['Atencione']['nro_atencion']), '/atenciones/view/' . $atencione['Atencione']['id']); ?>
<?php $this->Html->addCrumb(__('Modificar Consulta'), "/atencionconsultas/edit/{$this->request->data['Atencionconsulta']['id']}"); ?>
<div class="container atencionconsultas form">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 bg-white padding20">
				<div class="span10 bg-white">
					<h2><?php echo __('Modificar Consulta'); ?></h2>
				</div>
				<div class="span2 actions bg-white text-right">
					<div class="tile half bg-orange">
						<?php echo $this->Html->link(
							'<div class="tile-content icon"><i class="icon-list"></i></div>',
							array('controller' => 'atenciones', 'action' => 'main'),
							array(
								'class' => 'fg-white',
								'title' => __('List'),
								'escape' => false
							)
						); ?>
					</div>
					<div class="tile half bg-amber">
						<?php echo $this->Html->link(
							'<div class="tile-content icon"><i class="icon-arrow-left-3"></i></div>',
							array('controller' => 'atenciones', 'action' => 'view', $atencione['Atencione']['id']),
							array(
								'class' => 'fg-white',
								'title' => __('Back'),
								'escape' => false
							)
						); ?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="span12 bg-white padding20">
				<fieldset>
					<div class="row">
						<div class="span12">
							<?php echo $this->Form->create('Atencionconsulta', array('inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
							    <fieldset>
							        <legend><?php echo __('Datos de la Consulta'); ?></legend>
							        <div class="row">
							            <div class="span4">
							                <?php 
												echo $this->Form->input('id');
												echo $this->Form->input('atencione_id', array('type' => 'hidden', 'value' => $atencione['Atencione']['id']));
												echo $this->Form->input('ciudadano_id', array('type' => 'hidden', 'value' => $current_ciudadano['Ciudadano']['id_ciudadano']));
											?>
							                <?php echo $this->Form->label('atencione_id'); ?>
							                <span>#<?php echo h($atencione['Atencione']['nro_atencion']); ?></span>
							                <?php echo $this->Form->label('ciudadano_id'); ?>
							                <span><?php echo h($ciudadano['Ciudadano']['cedula']); ?> - <?php echo h($ciudadano['Ciudadano']['apellidos']); ?> <?php echo h($ciudadano['Ciudadano']['nombres']); ?></span>
							                <?php echo $this->Form->label('fecha_consulta'); ?>
							                <span><?php echo h($this->Time->format('d-m-Y g:ia', $this->request->data['Atencionconsulta']['created'])); ?></span>
							            </div>
							            <div class="span5 required">
							                <?php echo $this->Form->label('descripcion'); ?>
					                        <?php echo $this->Form->input('descripcion', array('type' => 'textarea', 'rows' => 8, 'div' => array('class' => 'input-control textarea'))); ?>
							            </div>
							        </div>
							    </fieldset>
							    <div class="form-actions">
							        <button type="submit" class="button success"><?php echo __('Guardar'); ?></button>
							    </div>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</fieldset>
			</div>
                </div>
        </div>
</div>