<?php if ($ciudadano): ?>
    <?php echo $this->element('Atenciones/top-menu', array('atenciontipo_id' => null)); ?>
<?php endif; ?>
<?php $this->Html->addCrumb(__('Recuperacion'), '/security/restore'); ?>
<div class="container security form">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 bg-white padding20">
				<div class="span10 bg-white">
					<h2><?php echo __('Seguridad: Recuperacion de informacion'); ?></h2>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="span12 bg-white padding20">
				<fieldset>
					<div class="row">
						<div class="span12">
							<?php echo $this->Form->create('Restore', array('type' => 'file', 'inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
								<?php echo $this->Form->input('datasource', array('type' => 'hidden', 'value' => 'default')); ?>
							    <fieldset>
							        <legend><?php echo __('Recuperar datos del sistema de atencion al ciudadano (base de datos)'); ?></legend>
							        <div class="row">
							            <div class="span5">
							            	<?php echo $this->Form->label('file', __('Seleccionar archivo de respaldo')); ?>
							            	<?php echo $this->Form->input('file', array('type' => 'file')); ?>
							                <button class="command-button warning span10" onclick="return confirm('<?php echo __('Esta seguro(a) que desea realizar la recuperacion de esta informacion?') ?>')">
												<i class="icon-upload-3 on-right "></i>
												<?php echo __('Iniciar el proceso de restaurar'); ?>
												<small><?php echo __('datos del sistema de atencion al ciudadano'); ?></small>
											</button>
							            </div>
							            <div class="span7">
							                <p><?php echo __('Para restaurar la informacion del sistema de atencion al ciudadano, debera seleccionar un archivo de respaldo que haya sido generado por esta misma herramienta'); ?></p>
							            </div>
							        </div>
							    </fieldset>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</fieldset>
			</div>
        </div>
		
		<div class="row">
			<div class="span12 bg-white padding20">
				<fieldset>
					<div class="row">
						<div class="span12">
							<?php echo $this->Form->create('Restore', array('type' => 'file', 'inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
								<?php echo $this->Form->input('datasource', array('type' => 'hidden', 'value' => 'auth')); ?>
							    <fieldset>
							        <legend><?php echo __('Recuperar datos de usuarios y permisos (globales)'); ?></legend>
							        <div class="row">
							            <div class="span5">
							            	<?php echo $this->Form->label('file', __('Seleccionar archivo de respaldo')); ?>
							            	<?php echo $this->Form->input('file', array('type' => 'file')); ?>
							                <button class="command-button warning span10" onclick="return confirm('<?php echo __('Esta seguro(a) que desea realizar la recuperacion de esta informacion?') ?>')">
												<i class="icon-upload-3 on-right "></i>
												<?php echo __('Iniciar el proceso de restaurar'); ?>
												<small><?php echo __('datos de usuarios y permisos'); ?></small>
											</button>
							            </div>
							            <div class="span7">
							                <p><?php echo __('Para restaurar la informacion de usuarios y permisos del sistema, debera seleccionar un archivo de respaldo que haya sido generado por esta misma herramienta'); ?></p>
							            </div>
							        </div>
							    </fieldset>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</fieldset>
			</div>
        </div>
		
		<div class="row">
			<div class="span12 bg-white padding20">
				<fieldset>
					<div class="row">
						<div class="span12">
							<?php echo $this->Form->create('Restore', array('type' => 'file', 'inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
								<?php echo $this->Form->input('datasource', array('type' => 'hidden', 'value' => 'comun')); ?>
							    <fieldset>
							        <legend><?php echo __('Recuperar datos de informacion comun y compartida entre sistemas'); ?></legend>
							        <div class="row">
							            <div class="span5">
							            	<?php echo $this->Form->label('file', __('Seleccionar archivo de respaldo')); ?>
							            	<?php echo $this->Form->input('file', array('type' => 'file')); ?>
							                <button class="command-button warning span10" onclick="return confirm('<?php echo __('Esta seguro(a) que desea realizar la recuperacion de esta informacion?') ?>')">
												<i class="icon-upload-3 on-right "></i>
												<?php echo __('Iniciar el proceso de restaurar'); ?>
												<small><?php echo __('datos de informacion comun y compartida'); ?></small>
											</button>
							            </div>
							            <div class="span7">
							                <p><?php echo __('Para restaurar la informacion comun y compartida entre sistemas, debera seleccionar un archivo de respaldo que haya sido generado por esta misma herramienta'); ?></p>
							            </div>
							        </div>
							    </fieldset>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</fieldset>
			</div>
        </div>
	</div>
</div>