<?php if ($ciudadano): ?>
    <?php echo $this->element('Atenciones/top-menu', array('atenciontipo_id' => null)); ?>
<?php endif; ?>
<?php $this->Html->addCrumb(__('Respaldo'), '/security/backup'); ?>
<div class="container security form">
	<div class="grid fluid">
		<div class="row">
			<div class="span12 bg-white padding20">
				<div class="span10 bg-white">
					<h2><?php echo __('Seguridad: Respaldo de informacion'); ?></h2>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="span12 bg-white padding20">
				<fieldset>
					<div class="row">
						<div class="span12">
							<?php echo $this->Form->create('Backup', array('inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
								<?php echo $this->Form->input('datasource', array('type' => 'hidden', 'value' => 'default')); ?>
							    <fieldset>
							        <legend><?php echo __('Generar respaldo del sistema de atencion al ciudadano (base de datos)'); ?></legend>
							        <div class="row">
							            <div class="span8">
							                <p><?php echo __('Al presionar el boton se procedera a generar un archivo de respaldo de toda la informacion referente a los datos de las atenciones a los ciudadanos, llevadas a cabo desde el inicio del sistema'); ?></p>
							            </div>
							            <div class="span4">
							                <p><?php echo __('Esta seguro(a) que desea realizar el respaldo?') ?></p>
							                <button class="command-button success span12">
												<i class="icon-database on-right "></i>
												<?php echo __('Si, deseo llevar a cabo el respaldo '); ?>
												<small><?php echo __('del sistema de atencion al ciudadano'); ?></small>
											</button>
							            </div>
							        </div>
							    </fieldset>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</fieldset>
			</div>
        </div>
		
		<div class="row">
			<div class="span12 bg-white padding20">
				<fieldset>
					<div class="row">
						<div class="span12">
							<?php echo $this->Form->create('Backup', array('inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
								<?php echo $this->Form->input('datasource', array('type' => 'hidden', 'value' => 'auth')); ?>
							    <fieldset>
							        <legend><?php echo __('Generar respaldo de usuarios y permisos (globales)'); ?></legend>
							        <div class="row">
							            <div class="span8">
							                <p><?php echo __('Al presionar el boton se procedera a generar un archivo de respaldo de toda la informacion referente a los datos de usuarios y grupos, y, permisologias'); ?></p>
							            </div>
							            <div class="span4">
							                <p><?php echo __('Esta seguro(a) que desea realizar el respaldo?') ?></p>
							                <button class="command-button success span12">
												<i class="icon-database on-right "></i>
												<?php echo __('Si, deseo llevar a cabo el respaldo'); ?>
												<small><?php echo __('de usuarios y peremisos'); ?></small>
											</button>
							            </div>
							        </div>
							    </fieldset>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</fieldset>
			</div>
        </div>
		
		<div class="row">
			<div class="span12 bg-white padding20">
				<fieldset>
					<div class="row">
						<div class="span12">
							<?php echo $this->Form->create('Backup', array('inputDefaults' => array('div' => array('class' => 'input-control text'), 'class' => 'span12', 'label' => false))); ?>
								<?php echo $this->Form->input('datasource', array('type' => 'hidden', 'value' => 'comun')); ?>
							    <fieldset>
							        <legend><?php echo __('Generar respaldo de informacion comun y compartida entre sistemas'); ?></legend>
							        <div class="row">
							            <div class="span8">
							                <p><?php echo __('Al presionar el boton se procedera a generar un archivo de respaldo de toda la informacion referente a los datos que son comunes entre los diferentes sistemas de la CEM'); ?></p>
							            </div>
							            <div class="span4">
							                <p><?php echo __('Esta seguro(a) que desea realizar el respaldo?') ?></p>
							                <button class="command-button success span12">
												<i class="icon-database on-right "></i>
												<?php echo __('Si, deseo llevar a cabo el respaldo '); ?>
												<small><?php echo __('de informacion comun y compartida'); ?></small>
											</button>
							            </div>
							        </div>
							    </fieldset>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</fieldset>
			</div>
        </div>
	</div>
</div>