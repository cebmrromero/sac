<?php
App::uses('AppController', 'Controller');
/**
 * Leyes Controller
 *
 * @property Leye $Leye
 * @property PaginatorComponent $Paginator
 */
class LeyesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Leye->recursive = 0;
		$this->set('leyes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Leye->recursive = 2;
		if (!$this->Leye->exists($id)) {
			throw new NotFoundException(__('Invalid leye'));
		}
		$options = array('conditions' => array('Leye.' . $this->Leye->primaryKey => $id));
		$this->set('leye', $this->Leye->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Leye->create();
			if ($this->Leye->save($this->request->data)) {
				$this->Session->setFlash(__('The leye has been saved.'), 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The leye could not be saved. Please, try again.'), 'flash_error');
			}
		}
		$ciudadano = $this->Session->read('ciudadano');
		$this->set(compact('ciudadano'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Leye->exists($id)) {
			throw new NotFoundException(__('Invalid leye'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Leye->save($this->request->data)) {
				$this->Session->setFlash(__('The leye has been saved.'), 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The leye could not be saved. Please, try again.'), 'flash_error');
			}
		} else {
			$options = array('conditions' => array('Leye.' . $this->Leye->primaryKey => $id));
			$this->request->data = $this->Leye->find('first', $options);
		}
		$ciudadano = $this->Session->read('ciudadano');
		$this->set(compact('ciudadano'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Leye->id = $id;
		if (!$this->Leye->exists()) {
			throw new NotFoundException(__('Invalid leye'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Leye->delete()) {
			$this->Session->setFlash(__('The leye has been deleted.'), 'flash_success');
		} else {
			$this->Session->setFlash(__('The leye could not be deleted. Please, try again.'), 'flash_error');
		}
		return $this->redirect(array('action' => 'index'));
	}}
