<?php
App::uses('AppController', 'Controller');
/**
 * Nivelinstrucciones Controller
 *
 * @property Nivelinstruccione $Nivelinstruccione
 * @property PaginatorComponent $Paginator
 */
class NivelinstruccionesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Nivelinstruccione->recursive = 0;
		$this->set('nivelinstrucciones', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Nivelinstruccione->recursive = 2;
		if (!$this->Nivelinstruccione->exists($id)) {
			throw new NotFoundException(__('Invalid nivelinstruccione'));
		}
		$options = array('conditions' => array('Nivelinstruccione.' . $this->Nivelinstruccione->primaryKey => $id));
		$nivelinstruccione = $this->Nivelinstruccione->find('first', $options);
		$ciudadanos_id = Hash::extract($nivelinstruccione, 'Perfilciudadano.{n}.ciudadano_id');
		$atenciones = $this->Nivelinstruccione->Perfilciudadano->Ciudadano->Atencione->find('all', array('conditions' => array('Atencione.ciudadano_id' => $ciudadanos_id)));
		$this->set(compact('nivelinstruccione', 'atenciones'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Nivelinstruccione->create();
			if ($this->Nivelinstruccione->save($this->request->data)) {
				$this->Session->setFlash(__('The nivelinstruccione has been saved.'), 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The nivelinstruccione could not be saved. Please, try again.'), 'flash_error');
			}
		}
		$ciudadano = $this->Session->read('ciudadano');
		$this->set(compact('ciudadano'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Nivelinstruccione->exists($id)) {
			throw new NotFoundException(__('Invalid nivelinstruccione'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Nivelinstruccione->save($this->request->data)) {
				$this->Session->setFlash(__('The nivelinstruccione has been saved.'), 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The nivelinstruccione could not be saved. Please, try again.'), 'flash_error');
			}
		} else {
			$options = array('conditions' => array('Nivelinstruccione.' . $this->Nivelinstruccione->primaryKey => $id));
			$this->request->data = $this->Nivelinstruccione->find('first', $options);
		}
		$ciudadano = $this->Session->read('ciudadano');
		$this->set(compact('ciudadano'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Nivelinstruccione->id = $id;
		if (!$this->Nivelinstruccione->exists()) {
			throw new NotFoundException(__('Invalid nivelinstruccione'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Nivelinstruccione->delete()) {
			$this->Session->setFlash(__('The nivelinstruccione has been deleted.'), 'flash_success');
		} else {
			$this->Session->setFlash(__('The nivelinstruccione could not be deleted. Please, try again.'), 'flash_error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
