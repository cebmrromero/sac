<?php
App::uses('AppController', 'Controller');
/**
 * Denunciaconsultas Controller
 *
 * @property Denunciaconsulta $Denunciaconsulta
 * @property PaginatorComponent $Paginator
 */
class DenunciaconsultasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * add method
 *
 * @return void
 */
	public function add($denuncia_id = null) {
		$ciudadano = $this->Session->read('ciudadano');
		if (!$ciudadano || !$denuncia_id) {
			if (!$ciudadano) {
				$this->Session->setFlash(__('Debe seleccionar un ciudadano.'), 'flash_warning');
			}
			if (!$denuncia_id) {
				$this->Session->setFlash(__('Invalid request.'), 'flash_warning');
			}
			$this->redirect(array('controller' => 'atenciones', 'action' => 'main'));
		}
		if ($this->request->is('post')) {
			$this->Denunciaconsulta->create();
			$this->request->data['Denunciaconsulta']['ciudadano_id'] = $ciudadano['Ciudadano']['id_ciudadano'];
			$this->request->data['Denunciaconsulta']['denuncia_id'] = $denuncia_id;
			//print_r($this->request->data);die;
			if ($this->Denunciaconsulta->save($this->request->data)) {
				$this->Session->setFlash(__('The denunciaconsulta has been saved.'), 'flash_success');
			} else {
				$this->Session->setFlash(__('The denunciaconsulta could not be saved. Please, try again.'), 'flash_error');
			}
		} else {
			$this->Session->setFlash(__('Invalid request.'), 'flash_warning');
		}
		$this->redirect(array('controller' => 'denuncias', 'action' => 'view', $denuncia_id));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Denunciaconsulta->exists($id)) {
			throw new NotFoundException(__('Invalid denunciaconsulta'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Denunciaconsulta->save($this->request->data)) {
				$this->Session->setFlash(__('The denunciaconsulta has been saved.'), 'flash_success');
				return $this->redirect(array('controller' => 'denuncias', 'action' => 'view', $this->request->data['Denunciaconsulta']['denuncia_id']));
			} else {
				$this->Session->setFlash(__('The denunciaconsulta could not be saved. Please, try again.'), 'flash_error');
			}
		} else {
			$options = array('conditions' => array('Denunciaconsulta.' . $this->Denunciaconsulta->primaryKey => $id));
			$this->request->data = $this->Denunciaconsulta->find('first', $options);
		}
		$ciudadano = $this->Session->read("ciudadano");
		$denuncia = $this->Denunciaconsulta->Denuncia->findById($this->request->data['Denunciaconsulta']['denuncia_id']);
		$current_ciudadano = $this->Denunciaconsulta->Ciudadano->findByIdCiudadano($this->request->data['Denunciaconsulta']['ciudadano_id']);
		$this->set(compact('denuncia', 'ciudadano', 'current_ciudadano'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Denunciaconsulta->id = $id;
		if (!$this->Denunciaconsulta->exists()) {
			throw new NotFoundException(__('Invalid denunciaconsulta'));
		}
		$denunciaconsulta = $this->Denunciaconsulta->findById($id);
		$denuncia_id = $denunciaconsulta['Denunciaconsulta']['denuncia_id'];
		$this->request->onlyAllow('get');
		if ($this->Denunciaconsulta->delete()) {
			$this->Session->setFlash(__('The denunciaconsulta has been deleted.'), 'flash_success');
		} else {
			$this->Session->setFlash(__('The denunciaconsulta could not be deleted. Please, try again.'), 'flash_error');
		}
		$this->redirect(array('controller' => 'denuncias', 'action' => 'view', $denuncia_id));
	}
}
