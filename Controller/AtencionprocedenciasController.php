<?php
App::uses('AppController', 'Controller');
/**
 * Atencionprocedencias Controller
 *
 * @property Atencionprocedencia $Atencionprocedencia
 * @property PaginatorComponent $Paginator
 */
class AtencionprocedenciasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

}
