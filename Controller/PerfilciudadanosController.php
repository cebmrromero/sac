<?php
App::uses('AppController', 'Controller');
/**
 * Perfilciudadanos Controller
 *
 * @property Perfilciudadano $Perfilciudadano
 * @property PaginatorComponent $Paginator
 */
class PerfilciudadanosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
}
