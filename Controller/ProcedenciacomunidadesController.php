<?php
App::uses('AppController', 'Controller');
/**
 * Procedenciacomunidades Controller
 *
 * @property Procedenciacomunidade $Procedenciacomunidade
 * @property PaginatorComponent $Paginator
 */
class ProcedenciacomunidadesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	public function getOcupacionesJSONP() {
        $sql = "SELECT DISTINCT Procedenciacomunidade.ocupacion FROM procedenciacomunidades as Procedenciacomunidade WHERE Procedenciacomunidade.ocupacion LIKE '%s' ORDER BY Procedenciacomunidade.ocupacion LIMIT 10";
        $sql = sprintf($sql, '%' . mysql_real_escape_string($this->request->query['text'] . '%'));
        $ocupaciones = Hash::extract($this->Procedenciacomunidade->query($sql), '{n}.Procedenciacomunidade.ocupacion');
        echo $this->request->query['callback'] . '(' . json_encode($ocupaciones) . ')';
        exit(1);
	}

}
