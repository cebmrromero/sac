<?php
App::uses('AppController', 'Controller');
/**
 * Atencioncategorias Controller
 *
 * @property Atencioncategoria $Atencioncategoria
 * @property PaginatorComponent $Paginator
 */
class AtencioncategoriasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Atencioncategoria->recursive = 0;
		$this->set('atencioncategorias', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Atencioncategoria->recursive = 2;
		if (!$this->Atencioncategoria->exists($id)) {
			throw new NotFoundException(__('Invalid atencioncategoria'));
		}
		$options = array('conditions' => array('Atencioncategoria.' . $this->Atencioncategoria->primaryKey => $id));
		$this->set('atencioncategoria', $this->Atencioncategoria->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Atencioncategoria->create();
			if ($this->Atencioncategoria->save($this->request->data)) {
				$this->Session->setFlash(__('The atencioncategoria has been saved.'), 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The atencioncategoria could not be saved. Please, try again.'), 'flash_error');
			}
		}
		$ciudadano = $this->Session->read('ciudadano');
		$this->set(compact('ciudadano'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Atencioncategoria->exists($id)) {
			throw new NotFoundException(__('Invalid atencioncategoria'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Atencioncategoria->save($this->request->data)) {
				$this->Session->setFlash(__('The atencioncategoria has been saved.'), 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The atencioncategoria could not be saved. Please, try again.'), 'flash_error');
			}
		} else {
			$options = array('conditions' => array('Atencioncategoria.' . $this->Atencioncategoria->primaryKey => $id));
			$this->request->data = $this->Atencioncategoria->find('first', $options);
		}
		$ciudadano = $this->Session->read('ciudadano');
		$this->set(compact('ciudadano'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Atencioncategoria->id = $id;
		if (!$this->Atencioncategoria->exists()) {
			throw new NotFoundException(__('Invalid atencioncategoria'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Atencioncategoria->delete()) {
			$this->Session->setFlash(__('The atencioncategoria has been deleted.'), 'flash_success');
		} else {
			$this->Session->setFlash(__('The atencioncategoria could not be deleted. Please, try again.'), 'flash_error');
		}
		return $this->redirect(array('action' => 'index'));
	}}
