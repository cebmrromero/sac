<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $components = array(
        // 'DebugKit.Toolbar',
        'Acl',
        'Auth' => array(
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => array(
                        'className' => 'Simple',
                        'hashType' => 'md5'
                    )
                )
            ),
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            ),
            'authError' => '<div id="custom-flash" class="success fg-white bg-red"><i class="icon-locked on-right on-left bg-white fg-red" style="padding: 10px;border-radius: 50%"></i>No tiene permisos para realizar esa operación</div>'
        ),
        'Session',
        'FilterResults.Filter' => array(
            'auto' => array(
                'paginate' => true,
                'explode'  => true,  // recommended
            ),
            'explode' => array(
                'character'   => ' ',
                'concatenate' => 'AND',
            )
        ),
    );

    public $helpers = array(
        'Html', 'Form', 'Session', 'Time',
        'FilterResults.Search' => array(
            'operators' => array(
                'LIKE'       => 'containing',
                'NOT LIKE'   => 'not containing',
                'LIKE BEGIN' => 'starting with',
                'LIKE END'   => 'ending with',
                '='  => 'equal to',
                '!=' => 'different',
                '>'  => 'greater than',
                '>=' => 'greater or equal to',
                '<'  => 'less than',
                '<=' => 'less or equal to'
            )
        )
    );
    
    public function beforeFilter() {
        //Configure AuthComponent
        $this->Auth->authenticate = array(
            'Ldap'
        );
        $this->Auth->loginAction = array(
            'controller' => 'users',
            'action' => 'login'
        );
        $this->Auth->logoutRedirect = array(
            'controller' => 'users',
            'action' => 'login'
        );
        $this->Auth->loginRedirect = array(
            'controller' => 'atenciones',
            'action' => 'main'
        );
        $this->Auth->allow(array('pages' => 'display'));
    }
}
