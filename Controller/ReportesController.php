<?php
App::uses('AppController', 'Controller');
/**
 * Reportes Controller
 *
 * @property Reportes $Reportes
 * @property PaginatorComponent $Paginator
 */
class ReportesController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(); // We can remove this line after we're finished
	}

/**
 * Models
 *
 * @var array
 */
	public $uses = array('Atencione', 'Atenciontipo', 'Atencioncategoria', 'User');
	public $meses = array('01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Septiemre', '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$reportes = array();
		$reportes['Reporte'] = array(
			array(
				'name' => __('Reporte Mensual'),
				'action' => 'reporte_mensual',
				'action_pdf' => 'reporte_mensual_pdf'
			),
			array(
				'name' => __('Reporte de Atenciones'),
				'action' => 'reporte_atenciones',
				'action_pdf' => 'reporte_mensual_pdf'
			),
		);

		$this->set(compact('reportes'));
	}

/**
 * reporte_mensual method
 *
 * @return void
 */
	public function reporte_mensual($in_pdf_mode = false) {
		if ($in_pdf_mode) {
			$this->layout = false;
		}
		$options['fields'] = array(
			'peticiones' => '(SELECT count(*) FROM atenciones WHERE atenciontipo_id = 1) as peticiones',
			'asesorias' => '(SELECT count(*) FROM atenciones WHERE atenciontipo_id = 2) as asesorias',
			'sugerencias' => '(SELECT count(*) FROM atenciones WHERE atenciontipo_id = 3) as sugerencias',
			'reclamos' => '(SELECT count(*) FROM atenciones WHERE atenciontipo_id = 4) as reclamos',
			'quejas' => '(SELECT count(*) FROM atenciones WHERE atenciontipo_id = 5) as quejas',
			'total' => '(SELECT count(*) FROM atenciones) as total',
		);
		$this->Atencione->recursive = -1;
		$aux = $this->Atencione->find('first', $options);
		$reporte['Reporte'] = $aux[0];
		// Array (
		// 	[Reporte] => Array (
		// 		[atenciones] => 425
		// 		[peticiones] => 28
		// 		[sugerencias] => 0
		// 		[quejas] => 0
		// 		[reclamos] => 0
		// 		[total] => 0
		// 	)
		// )
		$this->set(compact('reporte'));
	}

/**
 * reporte_mensual_pdf method
 *
 * @return void
 */
	public function reporte_mensual_pdf() {
		App::import(
			'Vendor',
			'Html2Pdf',
			array('file' => 'html2pdf' . DS . 'html2pdf.class.php')
		);

		$url = Router::url(array('action' => 'reporte_mensual', 'pdf'), true);
		
		$ch = curl_init();

		// set url
		curl_setopt($ch, CURLOPT_URL, $url);

		//return the transfer as a string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// set the UA
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1944.0 Safari/537.36');

		// Alternatively, lie, and pretend to be a browser
		// curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)');

		// $output contains the output string
		$content = curl_exec($ch);

		// close curl resource to free up system resources
		curl_close($ch);

		$html2pdf = new HTML2PDF('P','Legal','es');
		$html2pdf->WriteHTML($content);
		$html2pdf->Output('reporte_mensual.pdf');
		exit(1);
	}

/**
 * reporte_atenciones method
 *
 * @return void
 */
	public function reporte_atenciones($in_pdf_mode = false) {
		if ($in_pdf_mode) {
			print_r($this->request);die;
			$this->layout = false;
		}
		$meses = $this->meses;
		$this->Filter->addFilters(
			array(
				'nro_atencion' => array(
					'Atencione.nro_atencion' => array(
						'operator' => 'LIKE',
						'value' => array(
							'before' => '%', // optional
							'after'  => '%'  // optional
						)
					),
				),
				'atenciontipo_id' => array(
					'Atencione.atenciontipo_id' => array(
						'operator' => '='
					),
				),
				'atencioncategoria_id' => array(
					'Atencione.atencioncategoria_id' => array(
						'operator' => '='
					),
				),
				'user_id' => array(
					'Atencione.user_id' => array(
						'operator' => '='
					),
				),
			)
		);
		// $this->Filter->setPaginate('conditions', $this->Filter->getConditions());
		$atenciones = array();
		$atenciones = $this->Atencione->find('all', array('conditions' => $this->Filter->getConditions()));
		$atenciontipos = $this->Atenciontipo->find('list');
		$atencioncategorias = $this->Atencioncategoria->find('list');
		$user_options['joins'] = array(
			array('table' => 'sistemas_users',
				'alias' => 'SistemasUser',
				'conditions' => array(
					'SistemasUser.user_id = User.id',
					'SistemasUser.sistema_id = ' . Configure::read('sistema')
				)
			)
		);
		$user_options['conditions'] = array('User.bloqueado NOT' => 1, 'User.group_id NOT' => 99);
		$users = $this->User->find('list', $user_options);
		$this->set(compact('in_pdf_mode', 'meses', 'atenciones', 'atenciontipos', 'atencioncategorias', 'users'));
	}

/**
 * reporte_atenciones_pdf method
 *
 * @return void
 */
	public function reporte_atenciones_pdf() {
		echo $url = $this->request->data['Reporte']['source'];die;
		// $url = 'http://localhost/sac/reportes/reporte_atenciones/pdf/Wm1sc2RHVnlMbTV5YjE5aGRHVnVZMmx2Ymc9PQ==:/Wm1sc2RHVnlMbUYwWlc1amFXOXVkR2x3YjE5cFpBPT0=:TVE9PQ%3D%3D/Wm1sc2RHVnlMbUYwWlc1amFXOXVZMkYwWldkdmNtbGhYMmxr:TVE9PQ%3D%3D/Wm1sc2RHVnlMblZ6WlhKZmFXUT0=:T0E9PQ%3D%3D';
		App::import(
			'Vendor',
			'Html2Pdf',
			array('file' => 'html2pdf' . DS . 'html2pdf.class.php')
		);
		
		$ch = curl_init();

		// set url
		curl_setopt($ch, CURLOPT_URL, $url);

		//return the transfer as a string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// set the UA
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1944.0 Safari/537.36');

		// Alternatively, lie, and pretend to be a browser
		// curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)');

		// $output contains the output string
		$content = curl_exec($ch);
		// echo "<pre>"; $content;echo "</pre>";die;

		// close curl resource to free up system resources
		curl_close($ch);

		$html2pdf = new HTML2PDF('L','Letter','es');
		$content = str_replace("?", '', $content);
		$html2pdf->WriteHTML($content);
		$html2pdf->Output('reporte_mensual.pdf');
		exit(1);
	}
}