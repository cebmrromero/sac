<?php
App::uses('AppController', 'Controller');
/**
 * Atencionconsultas Controller
 *
 * @property Atencionconsulta $Atencionconsulta
 * @property PaginatorComponent $Paginator
 */
class AtencionconsultasController extends AppController {

/**
 * add method
 *
 * @return void
 */
	public function add($atencione_id = null) {
		$ciudadano = $this->Session->read('ciudadano');
		if (!$ciudadano || !$atencione_id) {
			if (!$ciudadano) {
				$this->Session->setFlash(__('Debe seleccionar un ciudadano.'), 'flash_warning');
			}
			if (!$atencione_id) {
				$this->Session->setFlash(__('Invalid request.'), 'flash_warning');
			}
			$this->redirect(array('controller' => 'atenciones', 'action' => 'main'));
		}
		if ($this->request->is('post')) {
			$this->Atencionconsulta->create();
			$this->request->data['Atencionconsulta']['ciudadano_id'] = $ciudadano['Ciudadano']['id_ciudadano'];
			$this->request->data['Atencionconsulta']['atencione_id'] = $atencione_id;
			//print_r($this->request->data);die;
			if ($this->Atencionconsulta->save($this->request->data)) {
				$this->Session->setFlash(__('The atencionconsulta has been saved.'), 'flash_success');
			} else {
				$this->Session->setFlash(__('The atencionconsulta could not be saved. Please, try again.'), 'flash_error');
			}
		} else {
			$this->Session->setFlash(__('Invalid request.'), 'flash_warning');
		}
		$this->redirect(array('controller' => 'atenciones', 'action' => 'view', $atencione_id));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Atencionconsulta->exists($id)) {
			throw new NotFoundException(__('Invalid atencionconsulta'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Atencionconsulta->save($this->request->data)) {
				$this->Session->setFlash(__('The atencionconsulta has been saved.'), 'flash_success');
				return $this->redirect(array('controller' => 'atenciones', 'action' => 'view', $this->request->data['Atencionconsulta']['atencione_id']));
			} else {
				$this->Session->setFlash(__('The atencionconsulta could not be saved. Please, try again.'), 'flash_error');
			}
		} else {
			$options = array('conditions' => array('Atencionconsulta.' . $this->Atencionconsulta->primaryKey => $id));
			$this->request->data = $this->Atencionconsulta->find('first', $options);
		}
		$ciudadano = $this->Session->read("ciudadano");
		$atencione = $this->Atencionconsulta->Atencione->findById($this->request->data['Atencionconsulta']['atencione_id']);
		$current_ciudadano = $this->Atencionconsulta->Ciudadano->findByIdCiudadano($this->request->data['Atencionconsulta']['ciudadano_id']);
		$this->set(compact('atencione', 'ciudadano', 'current_ciudadano'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Atencionconsulta->id = $id;
		if (!$this->Atencionconsulta->exists()) {
			throw new NotFoundException(__('Invalid atencionconsulta'));
		}
		$atencionconsulta = $this->Atencionconsulta->findById($id);
		$atencione_id = $atencionconsulta['Atencionconsulta']['atencione_id'];
		$this->request->onlyAllow('get');
		if ($this->Atencionconsulta->delete()) {
			$this->Session->setFlash(__('The atencionconsulta has been deleted.'), 'flash_success');
		} else {
			$this->Session->setFlash(__('The atencionconsulta could not be deleted. Please, try again.'), 'flash_error');
		}
		$this->redirect(array('controller' => 'atenciones', 'action' => 'view', $atencione_id));
	}
}
