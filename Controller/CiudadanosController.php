<?php
App::uses('AppController', 'Controller');
/**
 * Ciudadanos Controller
 *
 * @property Ciudadano $Ciudadano
 * @property PaginatorComponent $Paginator
 */
class CiudadanosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	public function getCedulasJSONP() {
		$sql = "SELECT DISTINCT Ciudadano.cedula FROM ciudadano as Ciudadano WHERE Ciudadano.cedula LIKE '%s' ORDER BY Ciudadano.cedula LIMIT 10";
		$sql = sprintf($sql, mysql_real_escape_string($this->request->query['text'] . '%'));
		$cedulas = Hash::extract($this->Ciudadano->query($sql), '{n}.Ciudadano.cedula');
		echo $this->request->query['callback'] . '(' . json_encode($cedulas) . ')';
		exit(1);
	}

/**
 * buscar method
 *
 * @return void
 */
	public function buscar() {
		if ($this->request->is('post')) {
			$this->Ciudadano->recursive = 0;
			$ciudadano = $this->Ciudadano->getCiudadanoForSession($this->request->data['Ciudadano']['cedula']);
			if (!$ciudadano) {
				$this->Session->setFlash(__('The ciudadano doest exists.'), 'flash_error');
				return $this->redirect(array('controller' => 'atenciones', 'action' => 'main'));
			}
			$this->Session->write("ciudadano", $ciudadano);
			$this->Session->write('inicio_atencion', date('Y-m-d H:i:s'));
		}
		$this->redirect($this->referer());
	}

/**
 * cambiar method
 *
 * @return void
 */
	public function cambiar() {
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Session->delete("ciudadano", "current_atencione_id", "inicio_atencion");
		}
		$this->redirect(array('controller' => 'atenciones', 'action' => 'main'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Ciudadano->exists($id)) {
			throw new NotFoundException(__('Invalid ciudadano'));
		}
		$ciudadano = $this->Session->read('ciudadano');
		if ($this->request->is('post') || $this->request->is('put')) {
			// print_r($this->request->data);die;
			if ($this->Ciudadano->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The ciudadano has been saved.'), 'flash_success');
				$ciudadano = $this->Ciudadano->getCiudadanoForSession($ciudadano['Ciudadano']['cedula']);
				$this->Session->write("ciudadano", $ciudadano);
				return $this->redirect(array('controller' => 'atenciones', 'action' => 'main'));
			} else {
				$this->Session->setFlash(__('The ciudadano could not be saved. Please, try again.'), 'flash_error');
				if (isset($this->request->data['Perfilciudadano']['parroquia_id']) && !empty($this->request->data['Perfilciudadano']['parroquia_id'])) {
					$fullpath = $this->Ciudadano->Perfilciudadano->Parroquia->getFullPath($this->request->data['Perfilciudadano']['parroquia_id']);
					$this->request->data = array_merge($this->request->data, $fullpath);
				}
			}
		} else {
			$this->Ciudadano->recursive = 2;
			$options = array('conditions' => array('Ciudadano.' . $this->Ciudadano->primaryKey => $id));
			$this->request->data = $this->Ciudadano->find('first', $options);
			if (isset($this->request->data['Perfilciudadano']['parroquia_id']) && !empty($this->request->data['Perfilciudadano']['parroquia_id'])) {
				$fullpath = $this->Ciudadano->Perfilciudadano->Parroquia->getFullPath($this->request->data['Perfilciudadano']['parroquia_id']);
				$this->request->data = array_merge($this->request->data, $fullpath);
			}
			$this->request->data['Perfilciudadano']['nivelinstruccione'] = '';
		}
		$paises = $this->Ciudadano->Perfilciudadano->Parroquia->Municipio->Entidade->Paise->find('list');
		$estadociviles = $this->Ciudadano->Perfilciudadano->Estadocivile->find('list');
		$nivelinstrucciones = $this->Ciudadano->Perfilciudadano->Nivelinstruccione->find('list', array('order' => 'Nivelinstruccione.order'));
		$this->set(compact('ciudadano', 'paises', 'estadociviles', 'nivelinstrucciones'));
	}
}
