<?php
App::uses('AppController', 'Controller');
/**
 * DenunciadosDenuncias Controller
 *
 * @property DenunciadosDenuncia $DenunciadosDenuncia
 * @property PaginatorComponent $Paginator
 */
class DenunciadosDenunciasController extends AppController {

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->DenunciadosDenuncia->id = $id;
		if (!$this->DenunciadosDenuncia->exists()) {
			throw new NotFoundException(__('Invalid denunciados denuncia'));
		}
		$this->request->onlyAllow('post', 'get', 'delete');
		if ($this->DenunciadosDenuncia->delete()) {
			$this->Session->setFlash(__('The denunciados denuncia has been deleted.'), 'flash_success');
		} else {
			$this->Session->setFlash(__('The denunciados denuncia could not be deleted. Please, try again.'), 'flash_error');
		}
		return $this->redirect($this->referer());
	}
}
