<?php
App::uses('AppController', 'Controller');
/**
 * Atenciones Controller
 *
 * @property Atencione $Atencione
 * @property PaginatorComponent $Paginator
 */
class AtencionesController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('planilla', 'planilla_pdf'); // We can remove this line after we're finished
	}

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	public function index() {
		$this->redirect(array('action' => 'main'));
	}

/**
 * main method
 *
 * @return void
 */
	public function main() {
		$ndias_showed = 365;
		$auth_user = $this->Session->read("Auth.User");
		$ciudadano = $this->Session->read("ciudadano");
		
		$this->Atencione->recursive = 0;
		if ($auth_user['group_id'] == 1) {
			$query = array(
				'conditions' => array(
					'Atencione.created >=' => date('Y-m-d H:i:s', time() - (60*60*24*$ndias_showed)),
					// 'NOT Atencione.atenciontipo_id = 9'
				),
				'order' => 'Atencione.id DESC',
			);
		} else {
			$query = array(
				'conditions' => array(
					// 'Atencione.user_id' => $auth_user['id'],
					'Atencione.created >=' => date('Y-m-d H:i:s', time() - (60*60*24*$ndias_showed)),
					// 'NOT Atencione.atenciontipo_id = 9'
				),
				'order' => 'Atencione.id DESC',
			);
		}
		// Si el ciudadano existe, agrego esa condicion al query
		if ($ciudadano) {
			$query['conditions'] = array_merge($query['conditions'], array('Atencione.ciudadano_id' => $ciudadano['Ciudadano']['id_ciudadano']));
		}
		//$this->Paginator->settings = $query;
		//$atenciones = $this->Paginator->paginate('Atencione');
		$atenciones = $this->Atencione->find("all", $query);
		
		// $this->loadModel("Denuncia");
		// $this->Denuncia->recursive = 0;
		// $query = array(
		// 	'conditions' => array(
		// 		'Denuncia.user_id' => $auth_user['id'],
		// 		'Denuncia.created >=' => date('Y-m-d H:i:s', time() - (60*60*24*$ndias_showed))
		// 	),
		// 	'order' => 'Denuncia.id DESC',
		// );
		// // Si el ciudadano existe, agrego esa condicion al query
		// if ($ciudadano) {
		// 	$query['conditions'] = array_merge($query['conditions'], array('Denuncia.ciudadano_id' => $ciudadano['Ciudadano']['id_ciudadano']));
		// }
		// $denuncias = $this->Denuncia->find("all", $query);
		$this->set(compact('atenciones'));
	}

/**
 * index method
 *
 * @return void
 */
	//public function index() {
	//	$this->Atencione->recursive = 0;
	//	$this->set('atenciones', $this->Paginator->paginate());
	//}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null, $print = false) {
		$print = in_array('print', $this->request['pass']);
		$ciudadano = $this->Session->read('ciudadano');
		$this->Atencione->recursive = 2;
		if (!$this->Atencione->exists($id)) {
			throw new NotFoundException(__('Invalid atencione'));
		}
		$options = array('conditions' => array('Atencione.' . $this->Atencione->primaryKey => $id));
		$atencione = $this->Atencione->find('first', $options);
		$this->set(compact('atencione', 'ciudadano', 'print'));
	}

/**
 * planilla method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function planilla($id = null) {
		App::import('Vendor', 'numero2letras');
		$num2text = new EnLetras();
		$this->loadModel('Parametro');
		$this->Atencione->recursive = 3;
		if (!$this->Atencione->exists($id)) {
			throw new NotFoundException(__('Invalid atencione'));
		}
		$options = array('conditions' => array('Atencione.' . $this->Atencione->primaryKey => $id));
		$atencione = $this->Atencione->find('first', $options);

		$fullpath = $this->Atencione->Ciudadano->Perfilciudadano->Parroquia->getFullPath($atencione['Ciudadano']['Perfilciudadano']['parroquia_id']);
		$atencione['Ciudadano']['Perfilciudadano'] = array_merge($atencione['Ciudadano']['Perfilciudadano'], $fullpath);

		$fullpath = $this->Atencione->Atencionprocedencia->Parroquia->getFullPath($atencione['Atencionprocedencia']['parroquia_id']);
		$atencione['Atencionprocedencia'] = array_merge($atencione['Atencionprocedencia'], $fullpath);

		$director_area = $this->Parametro->findByNombre('director_area');
		$pie_constancia = $this->Parametro->findByNombre('pie_constancia');

		$this->layout = false;
		$sexos = array('0' => 'Femenino', '1' => 'Masculino');
		$this->set(compact('atencione', 'sexos', 'director_area', 'pie_constancia', 'num2text'));
	}

	public function planilla_pdf($id = null) {
		App::import(
			'Vendor',
			'Html2Pdf',
			array('file' => 'html2pdf' . DS . 'html2pdf.class.php')
		);

		$url = Router::url(array('action' => 'planilla', $id), true);
		
		$ch = curl_init();

		// set url
		curl_setopt($ch, CURLOPT_URL, $url);

		//return the transfer as a string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// set the UA
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1944.0 Safari/537.36');

		// Alternatively, lie, and pretend to be a browser
		// curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)');

		// $output contains the output string
		$content = curl_exec($ch);

		// close curl resource to free up system resources
		curl_close($ch);

		$html2pdf = new HTML2PDF('P','Legal','es');
		$html2pdf->WriteHTML($content);
		$html2pdf->Output('planilla.pdf');
		exit(1);
	}

	public function no_concretada($atencione_id = null) {
		$this->loadModel('Paise');
		$this->loadModel("Ciudadano");
		$ciudadano = $this->Session->read('ciudadano');
		$ciudadano = $this->Ciudadano->getCiudadanoForSession($ciudadano['Ciudadano']['cedula']);
		$auth_user = $this->Session->read('Auth.User');
		$atenciontipo = $this->Atencione->Atenciontipo->findById(9);
		if (!$ciudadano) {
			$this->Session->setFlash(__('Debe seleccionar un ciudadano.'), 'flash_warning');
			$this->redirect(array('action' => 'main'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['Atencione']['created'] = $this->Session->read('inicio_atencion');
			$this->request->data['Atencione']['ended'] = date('Y-m-d H:i:s');
			if ($this->Atencione->save($this->request->data)) {
				$this->Session->setFlash(__('The atencione has been saved.'), 'flash_success');
				$this->redirect(array('action' => 'main'));
			}
		} else {
			if ($atencione_id) {
				$this->request->data = $this->Atencione->findById($atencione_id);
			}
		}
		$this->set(compact('ciudadano', 'atenciontipo', 'auth_user'));
	}
	

/**
 * add method
 *
 * @return void
 */
	public function add($atenciontipo_id = null, $paso = 1) {
		//$this->Session->delete('current_atencione_id');
		$this->loadModel('Paise');
		$this->loadModel("Ciudadano");
		$ciudadano = $this->Session->read('ciudadano');
		$ciudadano = $this->Ciudadano->getCiudadanoForSession($ciudadano['Ciudadano']['cedula']);
		$auth_user = $this->Session->read('Auth.User');
		$atenciontipo = $this->Atencione->Atenciontipo->findById($atenciontipo_id);
		if (!$ciudadano || !$atenciontipo_id) {
			if (!$ciudadano) {
				$this->Session->setFlash(__('Debe seleccionar un ciudadano.'), 'flash_warning');
			}
			if (!$atenciontipo_id) {
				$this->Session->setFlash(__('Invalid request.'), 'flash_warning');
			}
			$this->redirect(array('action' => 'main'));
		}
		
		if ($this->request->is('post') || $this->request->is('put')) {
			if (empty($ciudadano['Perfilciudadano'])) {
				$this->Ciudadano->Perfilciudadano->create();
			}
			// print_r($this->request->data);die;
			if ($this->Ciudadano->saveAll($this->request->data)) {
				$ciudadano = $this->Ciudadano->getCiudadanoForSession($ciudadano['Ciudadano']['cedula']);
				$this->Session->write("ciudadano", $ciudadano);
				$this->Atencione->create();
				$this->request->data['Atencione']['atenciontipo_id'] = $atenciontipo_id;
				$this->request->data['Atencione']['ciudadano_id'] = $ciudadano['Ciudadano']['id_ciudadano'];
				$this->request->data['Atencione']['user_id'] = $auth_user['id'];
				$this->request->data['Atencione']['tiene_acompanante'] = 0;
				$this->request->data['Atencione']['paso'] = 2;
				$this->request->data['Atencione']['created'] = $this->Session->read('inicio_atencion');
				if ($this->Atencione->save($this->request->data)) {
					$this->Session->setFlash(__('The atencione has been saved.'), 'flash_success');
					return $this->redirect(array('action' => 'continuar', $this->Atencione->getLastInsertID()));
				} else {
					$this->Session->setFlash(__('The atencione could not be saved, try again.'), 'flash_error');
				}
			} else {
				$this->Session->setFlash(__('The ciudadano could not be saved. Please, try again.'), 'flash_error');
				if (isset($this->request->data['Perfilciudadano']['parroquia_id']) && !empty($this->request->data['Perfilciudadano']['parroquia_id'])) {
					$fullpath = $this->Ciudadano->Perfilciudadano->Parroquia->getFullPath($this->request->data['Perfilciudadano']['parroquia_id']);
					$this->request->data = array_merge($this->request->data, $fullpath);
				}
			}
			
		} else {
			$this->Ciudadano->recursive = 2;
			$options = array('conditions' => array('Ciudadano.' . $this->Ciudadano->primaryKey => $ciudadano['Ciudadano']['id_ciudadano']));
			$this->request->data = $this->Ciudadano->find('first', $options);
			if (isset($this->request->data['Perfilciudadano']['parroquia_id']) && !empty($this->request->data['Perfilciudadano']['parroquia_id'])) {
				$fullpath = $this->Ciudadano->Perfilciudadano->Parroquia->getFullPath($this->request->data['Perfilciudadano']['parroquia_id']);
				$this->request->data = array_merge($this->request->data, $fullpath);
			}
			$this->request->data['Perfilciudadano']['nivelinstruccione'] = '';
		}
		$paises = $this->Paise->find('list', array('order' => 'Paise.nombre'));
		$estadociviles = $this->Atencione->Ciudadano->Perfilciudadano->Estadocivile->find("list");
		$nivelinstrucciones = $this->Atencione->Ciudadano->Perfilciudadano->Nivelinstruccione->find("list", array('order' => 'Nivelinstruccione.order'));
		$this->set(compact('auth_user', 'ciudadano', 'paso', 'atenciontipo_id', 'atenciontipo', 'estadociviles', 'paises', 'nivelinstrucciones'));
	}

/**
 * continuar method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function continuar($id = null, $paso = 2, $procedencia_selected = null) {
		$this->loadModel("Ciudadano");
		$this->loadModel('Paise');
		$paises = $this->Paise->find('list', array('order' => 'Paise.nombre'));
		$ciudadano = $this->Session->read('ciudadano');
		$auth_user = $this->Session->read('Auth.User');
		if (!$ciudadano || !$id) {
			if (!$ciudadano) {
				$this->Session->setFlash(__('Debe seleccionar un ciudadano.'), 'flash_warning');
			}
			if (!$id) {
				$this->Session->setFlash(__('Invalid request.'), 'flash_warning');
			}
			$this->redirect(array('action' => 'main'));
		}
		switch ($paso) {
			case 2:
				if ($this->request->is('put') || $this->request->is('post')) {
					if (!empty($this->request->data['Acompanante'])) {
						if (empty($this->request->data['Acompanante']['cedula'])) {
							$this->Session->setFlash(__('You must to select an acompanante.'), 'flash_warning');
							return $this->redirect(array('action' => 'continuar', $atenciontipo_id, 2));
						}
						$aux = $this->Ciudadano->findByCedula($this->request->data['Acompanante']['cedula']);
						$atencione = $this->Atencione->findById($id);
						$atencione['Acompanante']['atencione_id'] = $atencione['Atencione']['id'];
						$atencione['Acompanante']['ciudadano_id'] = $aux['Ciudadano']['id_ciudadano'];
						$acompanante['Acompanante'] = $atencione['Acompanante'];
						$this->Atencione->Acompanante->recursive = -1;
						$duplicated = $this->Atencione->Acompanante->find('first', array('conditions' => array('Acompanante.atencione_id' => $id, 'Acompanante.ciudadano_id' => $atencione['Acompanante']['ciudadano_id'])));
						if ($duplicated) {
							$this->Session->setFlash(__('Duplicated acompanante.'), 'flash_error');
							return $this->redirect(array('action' => 'continuar', $id, 2));
						}
						if ($this->Ciudadano->Acompanante->save($acompanante)) {
							$atencione['Atencione']['tiene_acompanante'] = 1;
							$this->Atencione->save($atencione);
							$this->Session->setFlash(__('The acompanante has been saved.'), 'flash_success');
							return $this->redirect(array('action' => 'continuar', $id, 2));
						}
					}
				} else {
					$options = array('conditions' => array('Atencione.' . $this->Atencione->primaryKey => $id));
					$this->Atencione->recursive = 2;
					$this->request->data = $this->Atencione->find('first', $options);
				}
				$atenciontipo = $this->Atencione->Atenciontipo->findById($this->request->data['Atencione']['atenciontipo_id']);
				$this->set(compact('auth_user', 'ciudadano', 'paso', 'id', 'atenciontipo'));
				break;
			case 3:
				if ($this->request->is('post') || $this->request->is('put')) {
					if ($this->request->data['Atencionprocedencia']['procedencia'] == 'I') {
						if (isset($this->request->data['Procedenciaorganismo']['organismo_id']) && !empty($this->request->data['Procedenciaorganismo']['organismo_id'])) {
							$organismo = $this->Atencione->Atencionprocedencia->Procedenciaorganismo->Organismo->findById($this->request->data['Procedenciaorganismo']['organismo_id']);
						} else if (!empty($this->request->data['Procedenciaorganismo']['organismo_ac'])) {
							$organismo = $this->Atencione->Atencionprocedencia->Procedenciaorganismo->Organismo->findByNombre($this->request->data['Procedenciaorganismo']['organismo_ac']);
						}
						$organismo['Organismo']['parroquia_id'] = $this->request->data['Atencionprocedencia']['parroquia_id'];
						$organismo['Organismo']['domicilio_fiscal'] = $this->request->data['Procedenciaorganismo']['domicilio_fiscal'];
						$organismo['Organismo']['nombre'] = $this->request->data['Procedenciaorganismo']['organismo_ac'];
						unset($this->request->data['Procedenciaorganismo']['domicilio_fiscal']);
						if (isset($organismo['Organismo']['id']) && !empty($organismo['Organismo']['id'])) {
							$this->Atencione->Atencionprocedencia->Procedenciaorganismo->Organismo->save($organismo);
						} else {
							$this->Atencione->Atencionprocedencia->Procedenciaorganismo->Organismo->create();
							$organismo = $this->Atencione->Atencionprocedencia->Procedenciaorganismo->Organismo->save($organismo);
							$this->request->data['Procedenciaorganismo']['organismo_id'] = $this->Atencione->Atencionprocedencia->Procedenciaorganismo->Organismo->getLastInsertID();
						}
					}
					if ($this->request->data['Atencionprocedencia']['procedencia'] == 'C') {
						if (isset($this->request->data['Procedenciacomunidade']['comunidade_id']) && !empty($this->request->data['Procedenciacomunidade']['comunidade_id'])) {
							$comunidade = $this->Atencione->Atencionprocedencia->Procedenciacomunidade->Comunidade->findById($this->request->data['Procedenciacomunidade']['comunidade_id']);
						} else if (isset($comunidade['Comunidade']['id']) && !empty($comunidade['Comunidade']['id'])) {
							$comunidade = $this->Atencione->Atencionprocedencia->Procedenciacomunidade->Comunidade->findByNombre($this->request->data['Procedenciacomunidade']['comunidade_ac']);
						}
						$comunidade['Comunidade']['parroquia_id'] = $this->request->data['Atencionprocedencia']['parroquia_id'];
						$comunidade['Comunidade']['ubicacion'] = $this->request->data['Procedenciacomunidade']['ubicacion'];
						$comunidade['Comunidade']['nombre'] = $this->request->data['Procedenciacomunidade']['comunidade_ac'];
						unset($this->request->data['Procedenciacomunidade']['ubicacion']);
						if (isset($comunidade['Comunidade']['id']) && !empty($comunidade['Comunidade']['id'])) {
							$this->Atencione->Atencionprocedencia->Procedenciacomunidade->Comunidade->save($comunidade);
						} else {
							$this->Atencione->Atencionprocedencia->Procedenciacomunidade->Comunidade->create();
							$this->Atencione->Atencionprocedencia->Procedenciacomunidade->Comunidade->save($comunidade);
							$this->request->data['Procedenciacomunidade']['comunidade_id'] = $this->Atencione->Atencionprocedencia->Procedenciacomunidade->Comunidade->getLastInsertID();
						}
					}
					if ($this->Atencione->Atencionprocedencia->saveAssociated($this->request->data)) {
						$this->Session->setFlash(__('The atencione has been saved.'), 'flash_success');
						return $this->redirect(array('action' => 'continuar', $id, 4));
					} else {
						$this->Session->setFlash(__('The atencione could not be saved. Please, try again.'), 'flash_error');
					}
				} else {
					$this->Atencione->Atencionprocedencia->recursive = 2;
					// if ($procedencia_selected) {
					// 	$options = array(
					// 		'conditions' => array(
					// 			'Atencionprocedencia.atencione_id' => $id,
					// 			'Atencionprocedencia.procedencia' => $procedencia_selected,
					// 		),
					// 		'order' => 'Atencionprocedencia.id DESC',
					// 	);
					// 	$this->request->data = $this->Atencione->Atencionprocedencia->find('first', $options);
					// } else {
						$options = array(
							'conditions' => array(
								'Atencionprocedencia.atencione_id' => $id,
							),
							'order' => 'Atencionprocedencia.id DESC',
						);
						$this->request->data = $this->Atencione->Atencionprocedencia->find('first', $options);
					// }
					if (!$this->request->data) {
						$this->request->data = array(
							'Atencionprocedencia' => array(
								'procedencia' => $procedencia_selected,
								'ciudadano_id' => $ciudadano['Ciudadano']['id_ciudadano'],
								'atencione_id' => $id,
							)
						);
					}
				}
				if (!$procedencia_selected) {
					$procedencia_selected = $this->request->data['Atencionprocedencia']['procedencia'];
				}
				$responsabilidades = null;
				if ($procedencia_selected == 'C') {
					$responsabilidades = $this->Atencione->Atencionprocedencia->Procedenciacomunidade->Responsabilidade->find('list');
				}
				$atencione = $this->Atencione->findById($id);
				$atencione['Atencione']['paso'] = 3;
				$this->Atencione->save($atencione);
				$atenciontipo = $this->Atencione->Atenciontipo->findById($atencione['Atencione']['atenciontipo_id']);
				$organismo = $comunidade = $particulare = array();
				if (!empty($this->request->data)) {
					if (!$procedencia_selected) {
						$procedencia_selected = $this->request->data['Atencionprocedencia']['procedencia'];
					}
					if (!empty($this->request->data['Procedenciaorganismo']['id'])) {
						$this->loadModel("Organismo");
						$organismo = $this->Organismo->findById($this->request->data['Procedenciaorganismo']['organismo_id']);
						$fullpath = $this->Organismo->Parroquia->getFullPath(($this->request->data['Atencionprocedencia']['parroquia_id']) ? $this->request->data['Atencionprocedencia']['parroquia_id'] : $organismo['Organismo']['parroquia_id']);
						$organismo = array_merge($organismo, $fullpath);
						
					}
					if (!empty($this->request->data['Procedenciacomunidade']['id'])) {
						$this->loadModel("Comunidade");
						$comunidade = $this->Comunidade->findById($this->request->data['Procedenciacomunidade']['comunidade_id']);
						$fullpath = $this->Comunidade->Parroquia->getFullPath(($this->request->data['Atencionprocedencia']['parroquia_id']) ? $this->request->data['Atencionprocedencia']['parroquia_id'] : $comunidade['Comunidade']['parroquia_id']);
						$comunidade = array_merge($comunidade, $fullpath);
					}
					if (!empty($this->request->data['Atencionprocedencia']['procedencia']) && $this->request->data['Atencionprocedencia']['procedencia'] == 'P') {
						$this->loadModel("Parroquia");
						$particulare = $this->Parroquia->getFullPath((isset($this->request->data['Atencionprocedencia']['parroquia_id'])) ? $this->request->data['Atencionprocedencia']['parroquia_id'] : $ciudadano['Perfilciudadano']['parroquia_id'] );
					}
				}
				$procedencias = array(
					'I' => __('Institucion'),
					'C' => __('Comunidad Organizada'),
					'P' => __('Particular'),
				);
				$organismos = $this->Atencione->Atencionprocedencia->Procedenciaorganismo->Organismo->find("list");
				$comunidades = $this->Atencione->Atencionprocedencia->Procedenciacomunidade->Comunidade->find("list");
				$this->set(compact('auth_user', 'ciudadano', 'paso', 'id', 'atenciontipo_id', 'atenciontipo', 'paises', 'procedencias', 'organismos', 'comunidades', 'organismo', 'comunidade', 'procedencia_selected', 'particulare', 'responsabilidades'));
				break;
			case 4:
				// Ajustando las validaciones, aqui es requerido ciertos campos, y aqui se hacen obligatorios
				if ($this->request->is('post') || $this->request->is('put')) {
					$this->Atencione->validate = $this->Atencione->getValidateUpdated();
					if ($this->Atencione->saveAll($this->request->data)) {
						//if (strlen($this->request->data['Atencione']['created']) == 10) { // Solo la fecha
							//$this->request->data['Atencione']['created'] = $this->request->data['Atencione']['created'] . ' ' . date('H:i:s');

						    //// Eliminar esto luego de que katerin ingrese todo
							//$this->request->data['Atencione']['ended'] = date('Y-m-d H:i:s', strtotime($this->data['Atencione']['created']) + (rand(15, 30) * 60));
							//$this->request->data['Atencione']['created'] . ' ' .  $this->data['Atencione']['ended'];
						//} else {
						if (!$this->request->data['Atencione']['ended']) {
								$this->request->data['Atencione']['ended'] = date('Y-m-d H:i:s');
						}
						//}

						$this->loadModel('Parametro');
						$this->request->data['Atencione']['completada'] = 1;
						$atenciontipo = $this->Atencione->Atenciontipo->findById($this->request->data['Atencione']['atenciontipo_id']);
						if (!$this->request->data['Atencione']['nro_atencion']) {
							$this->request->data['Atencione']['nro_atencion'] = '0'. $atenciontipo['Atenciontipo']['id'] . '-' . $this->Parametro->getUltimoAtencionControlByAtenciontipoId($this->request->data['Atencione']['atenciontipo_id']) . '-' . $this->Parametro->getEjercicioFiscal();
						}
						$this->Atencione->save($this->request->data);
						$this->Session->setFlash(__('The atencione has been saved.'), 'flash_success');
						return $this->redirect(array('action' => 'view', $id, 'print'));
					}
					$this->Session->setFlash(__('The atencione could not be saved. Please, try again.'), 'flash_error');
				} else {
					$atencione = $this->Atencione->findById($id);
					$atencione['Atencione']['paso'] = 4;
					$this->Atencione->save($atencione);
					$options = array('conditions' => array('Atencione.' . $this->Atencione->primaryKey => $id));
					$this->request->data = $this->Atencione->find('first', $options);
				}
				$atenciontipo = $this->Atencione->Atenciontipo->findById($this->request->data['Atencione']['atenciontipo_id']);
				$leyes = $this->Atencione->Leye->find('list');
				$atencioncategorias = $this->Atencione->Atencioncategoria->find('list');
				$user_options['joins'] = array(
					array('table' => 'sistemas_users',
						'alias' => 'SistemasUser',
						'conditions' => array(
							'SistemasUser.user_id = User.id',
							'SistemasUser.sistema_id = ' . Configure::read('sistema')
						)
					)
				);
				$user_options['conditions'] = array('User.bloqueado NOT' => 1, 'User.group_id NOT' => 99);
				$users = $this->Atencione->User->find('list', $user_options);
				$this->set(compact('auth_user', 'ciudadano', 'paso', 'id', 'atenciontipo_id', 'atenciontipo', 'leyes', 'atencioncategorias', 'users'));
				break;
			
			default:
				return $this->redirect(array('action' => 'main'));
				break;
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Atencione->id = $id;
		if (!$this->Atencione->exists()) {
			throw new NotFoundException(__('Invalid atencione'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Atencione->delete()) {
			$this->Session->setFlash(__('The atencione has been deleted.'), 'flash_success');
		} else {
			$this->Session->setFlash(__('The atencione could not be deleted. Please, try again.'), 'flash_error');
		}
		return $this->redirect(array('action' => 'main'));
	}

	public function getAreasJSONP() {
		$sql = "SELECT DISTINCT Atencione.area FROM atenciones as Atencione WHERE Atencione.area LIKE '%s' ORDER BY Atencione.area LIMIT 10";
		$sql = sprintf($sql, '%' . mysql_real_escape_string($this->request->query['text'] . '%'));
		$areas = Hash::extract($this->Atencione->query($sql), '{n}.Atencione.area');
		echo $this->request->query['callback'] . '(' . json_encode($areas) . ')';
		exit(1);
	}
	
	public function getSubareasJSONP() {
		$sql = "SELECT DISTINCT Atencione.subarea FROM atenciones as Atencione WHERE Atencione.subarea LIKE '%s' ORDER BY Atencione.subarea LIMIT 10";
		$sql = sprintf($sql, '%' . mysql_real_escape_string($this->request->query['text'] . '%'));
		$subareas = Hash::extract($this->Atencione->query($sql), '{n}.Atencione.subarea');
		echo $this->request->query['callback'] . '(' . json_encode($subareas) . ')';
		exit(1);
	}
}
