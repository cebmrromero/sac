<?php
App::uses('AppController', 'Controller');
/**
 * Sistemas Controller
 *
 * @property Sistema $Sistema
 * @property PaginatorComponent $Paginator
 */
class SistemasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
}
