<?php
App::uses('AppController', 'Controller');
/**
 * Responsabilidades Controller
 *
 * @property Responsabilidade $Responsabilidade
 * @property PaginatorComponent $Paginator
 */
class ResponsabilidadesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Responsabilidade->recursive = 0;
		$this->set('responsabilidades', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Responsabilidade->recursive = 4;
		if (!$this->Responsabilidade->exists($id)) {
			throw new NotFoundException(__('Invalid responsabilidade'));
		}
		$options = array('conditions' => array('Responsabilidade.' . $this->Responsabilidade->primaryKey => $id));
		$this->set('responsabilidade', $this->Responsabilidade->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Responsabilidade->create();
			if ($this->Responsabilidade->save($this->request->data)) {
				$this->Session->setFlash(__('The responsabilidade has been saved.'), 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The responsabilidade could not be saved. Please, try again.'), 'flash_error');
			}
		}
		$ciudadano = $this->Session->read('ciudadano');
		$this->set(compact('ciudadano'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Responsabilidade->exists($id)) {
			throw new NotFoundException(__('Invalid responsabilidade'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Responsabilidade->save($this->request->data)) {
				$this->Session->setFlash(__('The responsabilidade has been saved.'), 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The responsabilidade could not be saved. Please, try again.'), 'flash_error');
			}
		} else {
			$options = array('conditions' => array('Responsabilidade.' . $this->Responsabilidade->primaryKey => $id));
			$this->request->data = $this->Responsabilidade->find('first', $options);
		}
		$ciudadano = $this->Session->read('ciudadano');
		$this->set(compact('ciudadano'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Responsabilidade->id = $id;
		if (!$this->Responsabilidade->exists()) {
			throw new NotFoundException(__('Invalid responsabilidade'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Responsabilidade->delete()) {
			$this->Session->setFlash(__('The responsabilidade has been deleted.'), 'flash_success');
		} else {
			$this->Session->setFlash(__('The responsabilidade could not be deleted. Please, try again.'), 'flash_error');
		}
		return $this->redirect(array('action' => 'index'));
	}}
