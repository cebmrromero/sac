<?php
App::uses('AppController', 'Controller');
/**
 * Parroquias Controller
 *
 * @property Parroquia $Parroquia
 * @property PaginatorComponent $Paginator
 */
class ParroquiasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
        
        public function getAllByMunicipioIdJson($pais_id) {
            $options = array(
                'conditions' => array('Parroquia.municipio_id' => $pais_id),
                'order' => 'Parroquia.nombre'
            );
            echo json_encode($this->Parroquia->find("list", $options));
            exit(1);
        }

}
