<?php
App::uses('AppController', 'Controller');
/**
 * Municipios Controller
 *
 * @property Municipio $Municipio
 * @property PaginatorComponent $Paginator
 */
class MunicipiosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
        
        public function getAllByEntidadIdJson($pais_id) {
            $options = array(
                'conditions' => array('Municipio.entidad_id' => $pais_id),
                'order' => 'Municipio.nombre'
            );
            echo json_encode($this->Municipio->find("list", $options));
            exit(1);
        }

}
