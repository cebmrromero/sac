<?php
App::uses('AppController', 'Controller');
/**
 * Organismos Controller
 *
 * @property Organismo $Organismo
 * @property PaginatorComponent $Paginator
 */
class OrganismosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
        
        public function getByIdJson($id) {
            $options = array(
                'conditions' => array('Organismo.id' => $id),
                'order' => 'Organismo.nombre ASC'
            );
            $this->Organismo->recursive = 0;
            $organismo = $this->Organismo->find("first", $options);
            if (isset($organismo['Parroquia']['id']) && !empty($organismo['Parroquia']['id'])) {
                $fullpath = $this->Organismo->Parroquia->getFullPath($organismo['Parroquia']['id']);
                $organismo = array_merge($organismo, $fullpath);
            }
            echo json_encode($organismo, JSON_FORCE_OBJECT);
            exit(1);
        }
        
        public function getByNombreJSONP() {
            $nombre = mysql_real_escape_string(Inflector::slug($this->request->query['text']));
            $options = array(
                'conditions' => array(
                    'OR' => array(
                        'Organismo.nombre LIKE' => '%' . $nombre . '%',
                        'Organismo.siglas LIKE' => '%' . $nombre . '%',
                    ),
                ),
                'order' => 'Organismo.nombre ASC',
                'limit' => '10'
            );
            $this->Organismo->recursive = -1;
            $organismos = $this->Organismo->find("list", $options);
            
            echo $this->request->query['callback'] . '(' . json_encode($organismos) . ')';
            exit(1);
        }

}
