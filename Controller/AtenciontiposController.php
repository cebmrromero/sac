<?php
App::uses('AppController', 'Controller');
/**
 * Atenciontipos Controller
 *
 * @property Atenciontipo $Atenciontipo
 * @property PaginatorComponent $Paginator
 */
class AtenciontiposController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Atenciontipo->recursive = 0;
		$this->set('atenciontipos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Atenciontipo->recursive = 2;
		if (!$this->Atenciontipo->exists($id)) {
			throw new NotFoundException(__('Invalid atenciontipo'));
		}
		$options = array('conditions' => array('Atenciontipo.' . $this->Atenciontipo->primaryKey => $id));
		$this->set('atenciontipo', $this->Atenciontipo->find('first', $options));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$ciudadano = $this->Session->read("ciudadano");
		if (!$this->Atenciontipo->exists($id)) {
			throw new NotFoundException(__('Invalid atenciontipo'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Atenciontipo->save($this->request->data)) {
				$this->Session->setFlash(__('The atenciontipo has been saved.'), 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The atenciontipo could not be saved. Please, try again.'), 'flash_error');
			}
		} else {
			$options = array('conditions' => array('Atenciontipo.' . $this->Atenciontipo->primaryKey => $id));
			$this->request->data = $this->Atenciontipo->find('first', $options);
		}
		$this->set(compact('ciudadano'));
	}
}
