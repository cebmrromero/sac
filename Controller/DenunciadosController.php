<?php
App::uses('AppController', 'Controller');
/**
 * Denunciados Controller
 *
 * @property Denunciado $Denunciado
 * @property PaginatorComponent $Paginator
 */
class DenunciadosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function updateMany() {
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Denunciado->saveMany($this->request->data['Denunciado'])) {
				$this->Session->setFlash(__('The denunciado has been saved.'), 'flash_success');
			} else {
				$this->Session->setFlash(__('The denunciado could not be saved. Please, try again.'), 'flash_error');
			}
		}
		$this->redirect($this->referer());
	}

	public function getByCedula($cedula) {
		$denunciado = array();
		$denunciado = $this->Denunciado->findByCedula($cedula);
		if (isset($denunciado['Denunciado']['parroquia_id']) && !empty($denunciado['Denunciado']['parroquia_id'])) {
			$denunciado = array_merge($denunciado, $this->Denunciado->Parroquia->getFullPath($denunciado['Denunciado']['parroquia_id']));
		}
		return $denunciado;
	}

	public function getByCedulaJSON($cedula) {
		echo $this->request->query['callback'] . '(' . json_encode($this->getByCedula($cedula)) . ')';
		exit(1);
	}

}
