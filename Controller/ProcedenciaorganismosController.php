<?php
App::uses('AppController', 'Controller');
/**
 * Procedenciaorganismos Controller
 *
 * @property Procedenciaorganismo $Procedenciaorganismo
 * @property PaginatorComponent $Paginator
 */
class ProcedenciaorganismosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	public function getDependenciasJSONP() {
        $sql = "SELECT DISTINCT Procedenciaorganismo.dependencia FROM procedenciaorganismos as Procedenciaorganismo WHERE Procedenciaorganismo.dependencia LIKE '%s' ORDER BY Procedenciaorganismo.dependencia LIMIT 10";
        $sql = sprintf($sql, '%' . mysql_real_escape_string($this->request->query['text'] . '%'));
        $dependencias = Hash::extract($this->Procedenciaorganismo->query($sql), '{n}.Procedenciaorganismo.dependencia');
        echo $this->request->query['callback'] . '(' . json_encode($dependencias) . ')';
        exit(1);
	}
	
	public function getCargosJSONP() {
        $sql = "SELECT DISTINCT Procedenciaorganismo.cargo FROM procedenciaorganismos as Procedenciaorganismo WHERE Procedenciaorganismo.cargo LIKE '%s' ORDER BY Procedenciaorganismo.cargo LIMIT 10";
        $sql = sprintf($sql, '%' . mysql_real_escape_string($this->request->query['text'] . '%'));
        $cargos = Hash::extract($this->Procedenciaorganismo->query($sql), '{n}.Procedenciaorganismo.cargo');
        echo $this->request->query['callback'] . '(' . json_encode($cargos) . ')';
        exit(1);
	}

}
