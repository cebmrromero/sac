<?php
App::uses('AppController', 'Controller');
/**
 * Security Controller
 *
 * @property Security $Security
 * @property PaginatorComponent $Paginator
 */
class SecurityController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * Models
 *
 * @var array
 */
	public $uses = array();

/**
 * backup method
 *
 * @return void
 */
	public function backup() {
		if ($this->request->is('post')) {
			// print_r($this->request->data); die;
			App::uses('ConnectionManager', 'Model');
			$dataSource = ConnectionManager::getDataSource($this->request->data['Backup']['datasource']);
			
			$filename = strtoupper($dataSource->config['database']) . "_" . date("d-m-Y_h:i:s_A") . ".sql";
			$path = 'backup';
			$command = "mysqldump -u %s -p%s %s > %s%s%s";
			$command = sprintf($command,
					   $dataSource->config['login'],
					   $dataSource->config['password'],
					   $dataSource->config['database'],
					   $path,
					   DS,
					   $filename);
			exec($command, $output, $returnval);
			// if (file_exists($path . $file_exists) && filesize($path . $filename) > 0) {
			if ($returnval > 0) {
				$this->Session->setFlash(__('No se pudo generar el respaldo. Contacte a su administrador de sistemas.'), 'flash_error');
				$command = "rm -rf " . $path . DS . $filename;
				system($command);
			}
			if (file_exists($path . DS . $filename) && filesize($path . DS . $filename) > 0) {
				$this->response->file($path . DS . $filename);
				return $this->response;
			}
		}
		$ciudadano = $this->Session->read('ciudadano');
		$this->set(compact('ciudadano'));
	}

/**
 * backup method
 *
 * @return void
 */
	public function restore() {
		if ($this->request->is('post')) {
			App::uses('ConnectionManager', 'Model');
			$dataSource = ConnectionManager::getDataSource($this->request->data['Restore']['datasource']);
			
			$filename = $this->request->data['Restore']['file']['tmp_name'];
			if (file_exists($filename) && filesize($filename) > 0) {
				$command = "mysql -u %s -p%s %s < %s";
				$command = sprintf($command,
						   $dataSource->config['login'],
						   $dataSource->config['password'],
						   $dataSource->config['database'],
						   $filename);
				exec($command, $output, $returnval);
				if ($returnval > 0) {
					$this->Session->setFlash(__('No se pudo recuperar la base de datos. Contacte a su administrador de servicios.'), 'flash_error');
				} else {
					$this->Session->setFlash(__('Datos recuperados de manera existosa.'), 'flash_success');
					$this->redirect(array("action" => "restore"));
				}
			} else {
				$this->Session->setFlash(__('No se pudo recuperar la base de datos. El archivo es invalido o no existe.'), 'flash_error');
			}
		}
		$ciudadano = $this->Session->read('ciudadano');
		$this->set(compact('ciudadano'));
	}
}