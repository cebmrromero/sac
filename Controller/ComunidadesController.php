<?php
App::uses('AppController', 'Controller');
/**
 * Comunidades Controller
 *
 * @property Comunidade $Comunidade
 * @property PaginatorComponent $Paginator
 */
class ComunidadesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
        
        public function getByIdJson($id) {
            $options = array(
                'conditions' => array('Comunidade.id' => $id),
                'order' => 'Comunidade.nombre ASC'
            );
            $this->Comunidade->recursive = 0;
            $comunidade = $this->Comunidade->find("first", $options);
            if (isset($comunidade['Parroquia']['id']) && !empty($comunidade['Parroquia']['id'])) {
                $fullpath = $this->Comunidade->Parroquia->getFullPath($comunidade['Parroquia']['id']);
                $comunidade = array_merge($comunidade, $fullpath);
            }
            echo json_encode($comunidade, JSON_FORCE_OBJECT);
            exit(1);
        }
        
        public function getByNombreJSONP() {
            $nombre = mysql_real_escape_string(Inflector::slug($this->request->query['text']));
            $options = array(
                'conditions' => array(
                    'Comunidade.nombre LIKE ' => '%' . $nombre . '%',
                ),
                'order' => 'Comunidade.nombre ASC',
                'limit' => '10',
            );
            $this->Comunidade->recursive = -1;
            $comunidades = $this->Comunidade->find("list", $options);
            
            echo $this->request->query['callback'] . '(' . json_encode($comunidades) . ')';
            exit(1);
        }

}
