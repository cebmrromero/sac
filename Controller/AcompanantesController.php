<?php
App::uses('AppController', 'Controller');
/**
 * Acompanantes Controller
 *
 * @property Acompanante $Acompanante
 * @property PaginatorComponent $Paginator
 */
class AcompanantesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Acompanante->id = $id;
		if (!$this->Acompanante->exists()) {
			throw new NotFoundException(__('Invalid acompanante'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Acompanante->delete()) {
			$this->Session->setFlash(__('The acompanante has been deleted.'), 'flash_success');
		} else {
			$this->Session->setFlash(__('The acompanante could not be deleted. Please, try again.'), 'flash_error');
		}
		return $this->redirect($this->referer());
	}}
