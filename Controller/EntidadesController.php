<?php
App::uses('AppController', 'Controller');
/**
 * Entidades Controller
 *
 * @property Entidade $Entidade
 * @property PaginatorComponent $Paginator
 */
class EntidadesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
        
        public function getAllByPaisIdJson($pais_id) {
            $options = array(
                'conditions' => array('Entidade.pais_id' => $pais_id),
                'order' => 'Entidade.nombre ASC'
            );
            $entidades = $this->Entidade->find("list", $options);
            //$data = array();
            //foreach ($entidades as $k => $v) {
            //    $data["$k"] = $v;
            //}
            echo json_encode($entidades, JSON_FORCE_OBJECT);
            //echo json_encode($data);
            exit(1);
        }
}
