<?php
App::uses('AppController', 'Controller');
/**
 * Denuncias Controller
 *
 * @property Denuncia $Denuncia
 * @property PaginatorComponent $Paginator
 */
class DenunciasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->redirect(array('controller' => 'atenciones', 'action' => 'main'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null, $print = false) {
		$print = in_array('print', $this->request['pass']);
		$ciudadano = $this->Session->read('ciudadano');
		$this->Denuncia->recursive = 2;
		if (!$this->Denuncia->exists($id)) {
			throw new NotFoundException(__('Invalid denuncia'));
		}
		$options = array('conditions' => array('Denuncia.' . $this->Denuncia->primaryKey => $id));
		$denuncia = $this->Denuncia->find('first', $options);
		$this->set(compact('denuncia', 'ciudadano', 'print'));
	}

/**
 * planilla method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function planilla($id = null) {
		$this->Denuncia->recursive = 2;
		if (!$this->Denuncia->exists($id)) {
			throw new NotFoundException(__('Invalid denuncia'));
		}
		$options = array('conditions' => array('Denuncia.' . $this->Denuncia->primaryKey => $id));
		$denuncia = $this->Denuncia->find('first', $options);

		$fullpath = $this->Denuncia->Ciudadano->Perfilciudadano->Parroquia->getFullPath($denuncia['Ciudadano']['Perfilciudadano']['parroquia_id']);
		$denuncia['Ciudadano']['Perfilciudadano'] = array_merge($denuncia['Ciudadano']['Perfilciudadano'], $fullpath);

		foreach ($denuncia['Denunciado'] as &$denunciado) {
			$fullpath = $this->Denuncia->Denunciado->Parroquia->getFullPath($denunciado['parroquia_id']);
			$denunciado = array_merge($denunciado, $fullpath);
		}
		$this->layout = false;
		$this->set(compact('denuncia'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($paso = 1) {
		// TODO AQUI DEBO ACOMODAR EL REQUEST en el controller o en la vista, pero tengo ahi un problema que no esta actualizado el request cuando se da submit
		$this->loadModel('Paise');
		$this->loadModel("Ciudadano");
		$ciudadano = $this->Session->read('ciudadano');
		$auth_user = $this->Session->read('Auth.User');
		if (!$ciudadano) {
			if (!$ciudadano) {
				$this->Session->setFlash(__('Debe seleccionar un ciudadano.'), 'flash_warning');
			}
			$this->redirect(array('controller' => 'atenciones', 'action' => 'main'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			// unset($this->request->data['Paise'], $this->request->data['Entidade'], $this->request->data['Municipio']);
			if (empty($ciudadano['Perfilciudadano'])) {
				$this->Ciudadano->Perfilciudadano->create();
			}
			if ($this->Ciudadano->Perfilciudadano->saveAll($this->request->data)) {
				$ciudadano = $this->Ciudadano->getCiudadanoForSession($ciudadano['Ciudadano']['cedula']);
				$this->Session->write("ciudadano", $ciudadano);
				$this->Denuncia->create();
				$this->request->data['Denuncia']['ciudadano_id'] = $ciudadano['Ciudadano']['id_ciudadano'];
				$this->request->data['Denuncia']['user_id'] = $auth_user['id'];
				$this->request->data['Denuncia']['paso'] = 2;
				$this->request->data['Denuncia']['created'] = $this->Session->read('inicio_atencion');
				if ($this->Denuncia->save($this->request->data)) {
					$this->Session->setFlash(__('The denuncia has been saved.'), 'flash_success');
					return $this->redirect(array('action' => 'continuar', $this->Denuncia->getLastInsertID()));
				} else {
					$this->Session->setFlash(__('The denuncia could not be saved, try again.'), 'flash_error');
				}
			} else {
				$this->Session->setFlash(__('The ciudadano could not be saved. Please, try again.'), 'flash_error');
			}
		} else {
			$this->Ciudadano->recursive = 2;
			$options = array('conditions' => array('Ciudadano.' . $this->Ciudadano->primaryKey => $ciudadano['Ciudadano']['id_ciudadano']));
			$this->request->data = $this->Ciudadano->find('first', $options);
			if (isset($this->request->data['Perfilciudadano']['parroquia_id']) && !empty($this->request->data['Perfilciudadano']['parroquia_id'])) {
				$fullpath = $this->Ciudadano->Perfilciudadano->Parroquia->getFullPath($this->request->data['Perfilciudadano']['parroquia_id']);
				$this->request->data = array_merge($this->request->data, $fullpath);
			}
			$this->request->data['Perfilciudadano']['nivelinstruccione'] = '';
		}
		$paises = $this->Paise->find('list', array('order' => 'Paise.nombre'));
		$estadociviles = $this->Denuncia->Ciudadano->Perfilciudadano->Estadocivile->find("list");
		$nivelinstrucciones = $this->Denuncia->Ciudadano->Perfilciudadano->Nivelinstruccione->find("list");
		$this->set(compact('auth_user', 'ciudadano', 'paso', 'atenciontipo_id', 'atenciontipo', 'estadociviles', 'paises', 'nivelinstrucciones'));
	}

/**
 * continuar method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function continuar($id = null, $paso = 2, $procedencia_selected = null) {
		$this->loadModel("Ciudadano");
		$this->loadModel('Paise');
		$paises = $this->Paise->find('list', array('order' => 'Paise.nombre'));
		$ciudadano = $this->Session->read('ciudadano');
		$auth_user = $this->Session->read('Auth.User');
		$denuncia = $this->Denuncia->findById($id);
		// if ($denuncia['Denuncia']['paso'] != $paso) {
		// 	$paso = $denuncia['Denuncia']['paso'];
		// }
		unset($denuncia);
		if (!$ciudadano || !$id) {
			if (!$ciudadano) {
				$this->Session->setFlash(__('Debe seleccionar un ciudadano.'), 'flash_warning');
			}
			if (!$id) {
				$this->Session->setFlash(__('Invalid request.'), 'flash_warning');
			}
			$this->redirect(array('controller' => 'atenciones', 'action' => 'main'));
		}
		switch ($paso) {
			case 2:
				// Esto es Denunciaorganismo o Denunciacomunidade, ajustarlo de acuerdo a procedencia_selected, debe ser el formulario de cada quien, para poder autohidratar los input con el request->data que corresponda
				$this->Denuncia->validate = $this->Denuncia->getValidateUpdated();
				if ($this->request->is('post') || $this->request->is('put')) {
					$this->request->data['Denuncia']['paso'] = 3;
					if ($this->Denuncia->saveAll($this->request->data)) {
						$this->Session->setFlash(__('The denuncia has been saved.'), 'flash_success');
						return $this->redirect(array('action' => 'continuar', $id, 3));
					} else {
						$this->Session->setFlash(__('The denuncia could not be saved. Please, try again.'), 'flash_error');
						// FIx para cuando no se envia el form, se establece manualmente el nombre de la comunidad/organismo
						$this->request->data['Denunciacomunidade']['Comunidade']['nombre'] = (isset($this->request->data['Denunciacomunidade']['comunidade_ac'])) ? $this->request->data['Denunciacomunidade']['comunidade_ac'] : '';
						$this->request->data['Denunciaorganismo']['Organismo']['nombre'] = (isset($this->request->data['Denunciaorganismo']['organismo_ac'])) ? $this->request->data['Denunciaorganismo']['organismo_ac'] : '';
					}
				} else {
					$this->Denuncia->recursive = 2;
					$options = array('conditions' => array('Denuncia.' . $this->Denuncia->primaryKey => $id));
					$this->request->data = $this->Denuncia->find('first', $options);
				}
				if (!$procedencia_selected) {
					$procedencia_selected = $this->request->data['Denuncia']['organismo_denunciado'];
				}
				if (!empty($this->request->data['Denunciaorganismo']['id'])) {
					$this->loadModel("Organismo");
					$organismo = $this->Organismo->findById($this->request->data['Denunciaorganismo']['organismo_id']);
					$fullpath = ($organismo['Organismo']['parroquia_id']) ? $this->Organismo->Parroquia->getFullPath($organismo['Organismo']['parroquia_id']) : array();
					$organismo = array_merge($organismo, $fullpath);
				}
				if (!empty($this->request->data['Denunciacomunidade']['id'])) {
					$this->loadModel("Comunidade");
					$comunidade = $this->Comunidade->findById($this->request->data['Denunciacomunidade']['comunidade_id']);
					$fullpath = ($comunidade['Comunidade']['parroquia_id']) ? $this->Comunidade->Parroquia->getFullPath($comunidade['Comunidade']['parroquia_id']): array();
					$comunidade = array_merge($comunidade, $fullpath);
				}
				$procedencias = array(
					'I' => __('Institucion'),
					'C' => __('Comunidad Organizada'),
				);
				$organismos = $this->Denuncia->Denunciaorganismo->Organismo->find("list");
				$comunidades = $this->Denuncia->Denunciacomunidade->Comunidade->find("list");
				$this->set(compact('auth_user', 'ciudadano', 'paso', 'id', 'paises', 'procedencias', 'organismos', 'comunidades', 'organismo', 'comunidade', 'procedencia_selected'));
				break;
			case 3:
				if ($this->request->is('post') || $this->request->is('put')) {
					// print_r($this->request->data);die;
					// $this->request->data = Hash::filter($this->request->data);
					// foreach ($this->request->data['Denunciado'] as &$denunciado) {
						// unset($denunciado['Paise'], $denunciado['Entidade'], $denunciado['Municipio']);
						$this->request->data['Denunciado']['Denuncia'] = array('Denuncia' => $id);
					// }
					// print_r($this->request->data);die;
					// print_r(Hash::filter($this->request->data));die;
					// if ($this->request->data['Denunciado']['cedula']) {
					// 	$this->request->data['Denuncia']['Denuncia'] = $id;
					// }
					// $this->request->data = Hash::filter($this->request->data);
					// print_r($this->request->data);die;
					if (empty($this->request->data['Denunciado']['id'])) {
						unset($this->request->data['Denunciado']['id']);
					}
					// print_r($this->request->data);die;
					if ($this->Denuncia->Denunciado->save($this->request->data['Denunciado'])) {
						$this->Session->setFlash(__('The denunciado has been saved.'), 'flash_success');
						return $this->redirect($this->referer());
					} else {
						$this->Session->setFlash(__('The denunciado could not be saved. Please, try again.'), 'flash_error');
					}
				} else {
					$this->Denuncia->recursive = 2;
					$options = array('conditions' => array('Denuncia.' . $this->Denuncia->primaryKey => $id));
					$this->request->data = $this->Denuncia->find('first', $options);
					if ($this->request->data['Denuncia']['paso'] != 3) {
						$this->request->data['Denuncia']['paso'] = 3;
						$this->Denuncia->save($this->request->data);
					}
				}
				$denuncia = $this->Denuncia->findById($id);
				$denunciados = $denuncia['Denunciado'];

				foreach ($denunciados as &$denunciado) {
					$fullpath = $this->Denuncia->Denunciado->Parroquia->getFullPath($denunciado['parroquia_id']);
					$denunciado = array_merge($denunciado, $fullpath);
				}
				$this->set(compact('auth_user', 'ciudadano', 'paises', 'paso', 'id', 'denunciados'));
				break;
			default:
				return $this->redirect(array('controller' => 'atenciones', 'action' => 'main'));
				break;
		}
	}

	public function finalizar($id) {
		if ($this->request->is('get')) {
			$this->loadModel('Parametro');
			$denuncia = $this->Denuncia->findById($id);
			$denuncia['Denuncia']['completada'] = 1;
			$denuncia['Denuncia']['paso'] = 3;
			$denuncia['Denuncia']['ended'] = date('Y-m-d H:i:s');
			if (!$denuncia['Denuncia']['nro_expediente']) {
				$denuncia['Denuncia']['nro_expediente'] = 'OAC-01-02-' . $this->Parametro->getEjercicioFiscal(true) . '-' . $this->Parametro->getUltimoDenunciaControl();
			}
			$this->Denuncia->save($denuncia);
			return $this->redirect(array('action' => 'view', $id, 'print'));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Denuncia->id = $id;
		if (!$this->Denuncia->exists()) {
			throw new NotFoundException(__('Invalid denuncia'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Denuncia->delete()) {
			$this->Session->setFlash(__('The denuncia has been deleted.'));
		} else {
			$this->Session->setFlash(__('The denuncia could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
