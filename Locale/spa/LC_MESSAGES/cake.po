#: Utility/CakeTime.php:399
msgid "Today, %s"
msgstr "Hoy, %s"

#: Utility/CakeTime.php:402
msgid "Yesterday, %s"
msgstr "Ayer, %s"

#: Utility/CakeTime.php:405
msgid "Tomorrow, %s"
msgstr "Mañana, %s"

#: Utility/CakeTime.php:410
msgid "Sunday"
msgstr "Domingo"

#: Utility/CakeTime.php:411
msgid "Monday"
msgstr "Lunes"

#: Utility/CakeTime.php:412
msgid "Tuesday"
msgstr "Martes"

#: Utility/CakeTime.php:413
msgid "Wednesday"
msgstr "Miércoles"

#: Utility/CakeTime.php:414
msgid "Thursday"
msgstr "Jueves"

#: Utility/CakeTime.php:415
msgid "Friday"
msgstr "Viernes"

#: Utility/CakeTime.php:416
msgid "Saturday"
msgstr "Sábado"

#: Utility/CakeTime.php:422
msgid "On %s %s"
msgstr "El %s %s"

#: Utility/CakeTime.php:821
msgid "just now"
msgstr "justo ahora"

#: Utility/CakeTime.php:825
msgid "on %s"
msgstr "el %s"

#: Utility/CakeTime.php:869
msgid "about a second ago"
msgstr "hace un segundo"

#: Utility/CakeTime.php:870
msgid "about a minute ago"
msgstr "hace un minuto"

#: Utility/CakeTime.php:871
msgid "about an hour ago"
msgstr "hace una hora"

#: Utility/CakeTime.php:872
msgid "about a day ago"
msgstr "hace un día"

#: Utility/CakeTime.php:873
msgid "about a week ago"
msgstr "hace una semana"

#: Utility/CakeTime.php:874
msgid "about a year ago"
msgstr "hace un año"

#: Utility/CakeTime.php:878
msgid "in about a second"
msgstr "en un segundo"

#: Utility/CakeTime.php:879
msgid "in about a minute"
msgstr "en un minuto"

#: Utility/CakeTime.php:880
msgid "in about an hour"
msgstr "un una hora"

#: Utility/CakeTime.php:881
msgid "in about a day"
msgstr "en un día"

#: Utility/CakeTime.php:882
msgid "in about a week"
msgstr "en una semana"

#: Utility/CakeTime.php:883
msgid "in about a year"
msgstr "en un año"

#: Utility/CakeTime.php:889
msgid "%s ago"
msgstr "hace %s"

#: Utility/CakeTime.php:916;939
msgid "days"
msgstr "días"

#: Utility/CakeTime.php:847
msgid "%d year"
msgid_plural "%d years"
msgstr[0] "%d año"
msgstr[1] "$d años"

#: Utility/CakeTime.php:850
msgid "%d month"
msgid_plural "%d months"
msgstr[0] "%d mes"
msgstr[1] "%d meses"

#: Utility/CakeTime.php:853
msgid "%d week"
msgid_plural "%d weeks"
msgstr[0] "%d semana"
msgstr[1] "%s semanas"

#: Utility/CakeTime.php:856
msgid "%d day"
msgid_plural "%d days"
msgstr[0] "%d día"
msgstr[1] "%d días"

#: Utility/CakeTime.php:859
msgid "%d hour"
msgid_plural "%d hours"
msgstr[0] "%d hora"
msgstr[1] "%s horas"

#: Utility/CakeTime.php:862
msgid "%d minute"
msgid_plural "%d minutes"
msgstr[0] "%d minuto"
msgstr[1] "%d minutos"

#: Utility/CakeTime.php:865
msgid "%d second"
msgid_plural "%d seconds"
msgstr[0] "%d segundo"
msgstr[1] "%d segundos"