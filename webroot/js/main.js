$(function() {
    function render_select(select, items) {
        // $("<option>").val('').text('').appendTo(select)
        $.each(items, function (id, value) {
            $("<option>").val(id).text(value).appendTo(select)
        })
        $(select + ' option').sort(NASort).appendTo(select);
    }
        
    function getValues(url) {
        var values;
        $.ajax({
            url: url,
            dataType: 'json',
            async: false,
            success: function(items) {
                //render_select(select, items);
                values = items
            }
        })
        return values;
    }
    
    // Date pickers
    $(".datepicker").datepicker({
        format: "dd-mm-yyyy", // set output format
        position: "bottom", // top or bottom,
        locale: 'es', // 'ru' or 'en', default is $.Metro.currentLocale
        startMode: 'year'
    });
    
    $( "#ProcedenciaorganismoOrganismoId, #ProcedenciacomunidadeComunidadeId" ).change(function () {
        update_url = $(this).data('url');
        url = BASE_URL + update_url + $(this).val();
        values = getValues(url);
        pais_select = $(this).data('child');
        entidade_select = $(pais_select).data('child');
        municipio_select = $(entidade_select).data('child');
        parroquia_select = $(municipio_select).data('child');
        domicilio_fiscal = $('#ProcedenciaorganismoDomicilioFiscal');
        ubicacion = $('#ProcedenciacomunidadeUbicacion');
        $(domicilio_fiscal).val("").empty();
        $(ubicacion).val("").empty();
        $(pais_select).val("23").trigger("change");
        $(entidade_select).val("14").trigger("change");
        
        if (values.Parroquia.id) {
            $(pais_select).val(values.Paise.id).trigger("change")
            $(entidade_select).val(values.Entidade.id).trigger("change")
            $(municipio_select).val(values.Municipio.id).trigger("change")
            $(parroquia_select).val(values.Parroquia.id).trigger("change")
            if (values.Organismo) {
                $(domicilio_fiscal).val(values.Organismo.domicilio_fiscal).trigger("change")
            }
            if (values.Comunidade) {
                $(ubicacion).val(values.Comunidade.ubicacion).trigger("change")
            }
        }
    })
    
    // Selects Dependientes
    parroquia = $( ".parroquia-select");
    if ($(parroquia).length) {
        if (!$( ".entidade-select").attr('alt')) {
            $( ".entidade-select").attr('alt', '14');
        }
        if (!$( ".pais-select").val()) {
            $( ".pais-select").val('23').trigger('change');
        }
        $( ".pais-select, .entidade-select, .municipio-select, .parroquia-select" ).change(function () {
            target_select = $(this).data('child');
            update_url = $(this).data('url');
            selected_val = null;
            
            if ($(this).attr('alt') || $(this).val()) {
                selected_val = ($(this).attr('alt')) ? $(this).attr('alt') : $(this).val();
                $(this).removeAttr('alt');
            }
            
            $(this).val(selected_val);
            
            $(target_select).empty().append($("<option>").val(""));
            if (update_url && $(this).val()) {
                url = BASE_URL + update_url + $(this).val();
                render_select(target_select, getValues(url));
            }
            $(target_select).val('').trigger("change");
        })
        if ($( "#PaiseId" ).val()) {
            $( this ).trigger("change");
        }

        $( ".pais-select" ).each(function () {
            if ($( this ).val()) {
                $( this ).trigger("change");
            }
        })
    }
    CKEDITOR.config.extraPlugins = 'wordcount';
    CKEDITOR.config.entities_latin = false;
    // CKEDITOR.config.wordcount = {

    //     // Whether or not you want to show the Word Count
    //     showWordCount: false,

    //     // Whether or not you want to show the Char Count
    //     showCharCount: true,

    //     // Whether or not to include Html chars in the Char Count
    //     countHTML: false,

    //     charLimit: 600
    // };
    CKEDITOR.config.toolbar = [[ 'Bold', 'Italic', 'Underline', '-', 'RemoveFormat' ], ['spellchecker', 'Scayt', '-', 'NumberedList', 'BulletedList'], ['Undo', 'Redo']];
    $('.ckeditor.motivo').ckeditor({
        height: 120,
        wordcount: {

            // Whether or not you want to show the Word Count
            showWordCount: false,

            // Whether or not you want to show the Char Count
            showCharCount: true,

            // Whether or not to include Html chars in the Char Count
            countHTML: false,

            charLimit: 600
        }
    });

    CKEDITOR.config.extraPlugins = 'wordcount';
    CKEDITOR.config.entities_latin = false;
    // CKEDITOR.config.wordcount = {

    //     // Whether or not you want to show the Word Count
    //     showWordCount: false,

    //     // Whether or not you want to show the Char Count
    //     showCharCount: true,

    //     // Whether or not to include Html chars in the Char Count
    //     countHTML: false,

    //     charLimit: 1000
    // };
    CKEDITOR.config.toolbar = [[ 'Bold', 'Italic', 'Underline', '-', 'RemoveFormat' ], ['spellchecker', 'Scayt', '-', 'NumberedList', 'BulletedList'], ['Undo', 'Redo']];
    $('.ckeditor.atencion').ckeditor({
        height: 120,
        wordcount: {

            // Whether or not you want to show the Word Count
            showWordCount: false,

            // Whether or not you want to show the Char Count
            showCharCount: true,

            // Whether or not to include Html chars in the Char Count
            countHTML: false,

            charLimit: 1500
        }
    });


    CKEDITOR.config.extraPlugins = 'wordcount';
    CKEDITOR.config.entities_latin = false;
    // CKEDITOR.config.wordcount = {

    //     // Whether or not you want to show the Word Count
    //     showWordCount: false,

    //     // Whether or not you want to show the Char Count
    //     showCharCount: true,

    //     // Whether or not to include Html chars in the Char Count
    //     countHTML: false,

    //     charLimit: 400
    // };
    CKEDITOR.config.toolbar = [[ 'Bold', 'Italic', 'Underline', '-', 'RemoveFormat' ], ['spellchecker', 'Scayt', '-', 'NumberedList', 'BulletedList'], ['Undo', 'Redo']];
    $('.ckeditor.observacion').ckeditor({
        height: 120,
        wordcount: {

            // Whether or not you want to show the Word Count
            showWordCount: false,

            // Whether or not you want to show the Char Count
            showCharCount: true,

            // Whether or not to include Html chars in the Char Count
            countHTML: false,

            charLimit: 400
        }
    });

    $( ".add-textofundamentacione" ).click(function (e) {
        e.preventDefault();
        CKEDITOR.instances.AtencioneFundamentacionLegal.insertText($(this).attr('alt'));
    }) 
    
    $("a.show-consulta-details").on('click', function(){
        var text = $(this).find('span.list-subtitle').attr('alt');
        var url_edit = $(this).find('span.list-subtitle').data('url-edit');
        var url_delete = $(this).find('span.list-subtitle').data('url-delete');
        var link_edit = $("<a>").attr('href', url_edit).addClass('button info').text("Editar");
        var link_delete = $("<a>").attr('href', url_delete).addClass('button danger').text("Eliminar").attr('onclick', 'return confirm("¿Seguro(a) que desea eliminar el registro?")');
        var ciudadano = $(this).find('span.list-title').text();
        var title = "Detalle de la consulta"
        //console.log(text);
        $.Dialog({
            overlay: true,
            shadow: true,
            flat: true,
            icon: '<i class="icon-list"></i>',
            title: title,
            content: '',
            width: 600,
            padding: 20,
            onShow: function(_dialog) {
                var content = _dialog.children('.content');
                strong = $("<strong>").text(ciudadano).append(":");
                par = $("<p>").text(text);
                // '<strong>' + ciudadano + '</strong>:<br />' + text;
                // $(link).appendTo(text)
                content.empty().append(strong).append(par, "<hr />", link_edit, ' ', link_delete);
            }
        });
    })
    
    
    // Source: http://stackoverflow.com/questions/6670041/sort-options-in-a-select-list-with-javascript-jquery-but-not-alphabetically
    function NASort(a, b) {
        if (a.innerHTML == 'NA') {
            return 1;
        }
        else if (b.innerHTML == 'NA') {
            return -1;
        }
        return (a.innerHTML > b.innerHTML) ? 1 : -1;
    };
    $( ".autocomplete" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: BASE_URL + this.element.data('url'),
                dataType: "jsonp",
                data: {
                    maxRows: 12,
                    text: request.term
                },
                success: function( data ) {
                    response( $.map( data, function( item, id ) {
                        return {
                            label: item,
                            value: item,
                            id: id
                        }
                    }));
                }
            });
        },
        minLength: $(this).data('minkeys'),
        select: function( event, ui ) {
            if ($(this).data('target-update')) {
                $($(this).data('target-update')).val(ui.item.id).trigger("change")
            }
        },
        open: function() {
            $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
            $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
    
    $(".switch input").change(function () {
        if ($(this).is(':checked')) {
            $(this).parent().find('.caption').text('Sí');
        } else {
            $(this).parent().find('.caption').text('No');
        }
    })
    
    $( "#AtencioneTieneDocumentos, #DenunciaAnexaDocumentos, #PerfilciudadanoTieneTelefonos" ).change(function () {
        if ($(this).is(':checked')) {
            $("div.documentos, div.telefonos").removeClass('hidden');
            $("div.documentos input, div.telefonos input").removeAttr("disabled");
        } else {
            $("div.documentos, div.telefonos").addClass('hidden');
            $("div.documentos input, div.telefonos input").attr("disabled", "disabled");
        }
    });
    
    $( "#AtencioneTieneDocumentos, #DenunciaAnexaDocumentos, #PerfilciudadanoTieneTelefonos" ).trigger( "change" );

    // Accordion arrow indicator
    $("a.heading").click(function () {
        if ( $( this ).hasClass('collapsed') ) {
            $( ".icon-toggle-accordion" ).removeClass("icon-arrow-up-4").addClass("icon-arrow-down-4");
        } else {
            $( ".icon-toggle-accordion" ).removeClass("icon-arrow-down-4").addClass("icon-arrow-up-4");
        }
    })
    
    cedula_help = $("nav.navigation-bar").find("div.cedula-help")
    if (cedula_help.length) {
        $(cedula_help).click(function () { $(this).fadeOut() })
    }

    denunciado_cedula = $(".denunciado_cedula");
    if (denunciado_cedula.length) {
        $(denunciado_cedula).change(function () {
            $(".denunciado_id").val('');
            $(".denunciado_nombres").val('');
            $(".denunciado_apellidos").val('');
            $(".denunciado_paise_id").val('23').trigger("change");
            $(".denunciado_entidade_id").val('14').trigger("change");
            $(".denunciado_municipio_id").val('').trigger("change");
            $(".denunciado_parroquia_id").val('').trigger("change");
            $(".denunciado_direccion_habitacion").val('');
            $(".denunciado_telefono_celular").val('');
            $(".denunciado_telefono_habitacion").val('');
            $(".denunciado_telefono_oficina").val('');
            $.ajax({
                url: $(denunciado_cedula).data('url') + '/' + this.value,
                dataType: 'jsonp',
                success: function (data) {
                    if ($(data).length) {
                        $(".denunciado_id").val(data.Denunciado.id);
                        $(".denunciado_nombres").val(data.Denunciado.nombres);
                        $(".denunciado_apellidos").val(data.Denunciado.apellidos);
                        if (data.Parroquia.id) {
                            $(".denunciado_paise_id").val(data.Paise.id).trigger("change");
                            $(".denunciado_entidade_id").val(data.Entidade.id).trigger("change");
                            $(".denunciado_municipio_id").val(data.Municipio.id).trigger("change");
                            $(".denunciado_parroquia_id").val(data.Parroquia.id).trigger("change");
                        }
                        $(".denunciado_direccion_habitacion").val(data.Denunciado.direccion_habitacion);
                        $(".denunciado_telefono_celular").val(data.Denunciado.telefono_celular);
                        $(".denunciado_telefono_habitacion").val(data.Denunciado.telefono_habitacion);
                        $(".denunciado_telefono_oficina").val(data.Denunciado.telefono_oficina);
                    }
                }
            })
        })
    }
})


function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}